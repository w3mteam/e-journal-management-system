<html>
	<head>
		<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/bootstrap.min.css">
		<script src="<?php echo base_url();?>files/public/js/jquery-1.8.3.min.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>
		
		<script type="text/javascript">
			function displayRes1(item, val, text) {
			    $('#selecteditems1').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" class="pub_activeinput" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo1').val('');
			}
			function displayRes2(item, val, text) {
			    $('#selecteditems2').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" class="pro_activeinput" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo2').val('');
			}
			function displayRes3(item, val, text) {
			    $('#selecteditems3').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" class="course_activeinput" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo3').val('');
			}
			function removeparent(e){
				e.parentNode.remove();
				var p = parseInt(e.parentNode.lastChild.textContent);
			}
			$(document).ready(function () {
				var jsonData = <?php echo $concept;?>;
				src = []; 
				all_publication = [];
				all_projects = [];
				all_courses = [];
				for (var i = 0; i < jsonData.length; i++) {
					src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
				}
			    $('#demo1').typeahead({
			        source:src,
			        itemSelected: displayRes1
			    });
			   $('#demo2').typeahead({
			        source:src,
			        itemSelected: displayRes2
			    });
			    $('#demo3').typeahead({
			        source:src,
			        itemSelected: displayRes3
			    });
			    /*********************************************************************/
			    /********************************Publication**************************/
			    /*********************************************************************/
			    $('#add_new_publications').click(function(){
			    	var pub_data = {};
			    	var publications = {}; 
					$(".pub_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pub_data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = JSON.stringify(pub_data);
					all_publication.push(publications);
					$('#selecteditems1').html('');
					$('.pub_activeinput').attr('value','');
					console.log(all_publication);
			    });
			    /*********************************************************************/
			    /********************************Project******************************/
			    /*********************************************************************/
			    $('#add_new_projects').click(function(){
			    	var pro_data = {};
			    	var projects = {}; 
					$(".pro_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pro_data[name] = value;}
						else{
							projects[name]=value;
						}
					});
					projects['pro_domain'] = JSON.stringify(pro_data);
					all_projects.push(projects);
					$('#selecteditems2').html('');
					$('.pro_activeinput').attr('value','');
					console.log(all_projects);
			    });
			    /*********************************************************************/
			    /********************************Course*******************************/
			    /*********************************************************************/
			    $('#add_new_courses').click(function(){
			    	var course_data = {};
			    	var courses = {}; 
					$(".course_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {course_data[name] = value;}
						else{
							courses[name]=value;
						}
					});
					courses['course_domain'] = JSON.stringify(course_data);
					all_courses.push(courses);
					$('#selecteditems3').html('');
					$('.course_activeinput').attr('value','');
					console.log(all_courses);
			    });
				$('form#add').on('submit', function(){
					/*********************************************************************/
				    /********************************Publication**************************/
				    /*********************************************************************/
					var pub_data = {};
			    	var publications = {}; 
					$(".pub_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pub_data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = JSON.stringify(pub_data);
					all_publication.push(publications);
					$('#selecteditems1').html('');
					/*********************************************************************/
				    /********************************Project******************************/
				    /*********************************************************************/
				    var pro_data = {};
			    	var projects = {}; 
					$(".pro_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pro_data[name] = value;}
						else{
							projects[name]=value;
						}
					});
					projects['pro_domain'] = JSON.stringify(pro_data);
					all_projects.push(projects);
					$('#selecteditems2').html('');
					/*********************************************************************/
			    	/********************************Course*******************************/
			    	/*********************************************************************/
			   		var course_data = {};
			    	var courses = {}; 
					$(".course_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {course_data[name] = value;}
						else{
							courses[name]=value;
						}
					});
					courses['course_domain'] = JSON.stringify(course_data);
					all_courses.push(courses);
					$('#selecteditems3').html('');
					/*********************************************************************/
				    /********************************Ajax*********************************/
				    /*********************************************************************/
				    $.ajax({ 
					      type: "POST",
					      url: '<?php echo base_url();?>index.php/profile/add_jobs',
					      datatype: "json",
					      traditional: true,
					      data: {pub_json:JSON.stringify(all_publication),pro_json:JSON.stringify(all_projects),course_json:JSON.stringify(all_courses)}, 
					      success: function (response) {
					      	console.log(response);
					      	$('.all').html('');
					      }          
					 });
					 return false; //disable refresh
				});
			    
			});
		</script>
	</head>
	<body>
		<div class="all">
			<?php echo form_open('profile/add_jobs','id= "add"'); ?>
			<!------------------------------------------------------------>
			<!--------------------------Publications---------------------->
			<!------------------------------------------------------------>
			<h4>Publications</h4>
			<hr />
			<div class="input_fields_publication">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Publication Name</label>
							<?php echo form_input('pub_name',set_value('pub_name'),'class="pub_activeinput"');?><br />
							<label>Publication Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pub_domain',set_value('pub_domain'),'id="demo1" class="pub_activeinput"'); ?><br />
							</div>
							
							<label>Publication Details</label><br />
							<textarea name="pub_details" class="pub_activeinput" rows="4"></textarea><br />
							<label>Publication Date</label>
							<input type="date" name="pub_date" class="pub_activeinput" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems1">
								
								</tbody>
							</table>
						</div>
						
					</div>
					<a href="#" id="add_new_publications">Add New Publications</a>
				</div>
				
			</div>
			<!------------------------------------------------------------>
			<!--------------------------Projects-------------------------->
			<!------------------------------------------------------------>
			<hr />
			<h4>Project</h4>
			<hr />
			<div class="input_fields_project">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Project Name</label>
							<?php echo form_input('pro_name',set_value('pro_name'),'class="pro_activeinput"');?><br />
							<label>Project Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pro_domain',set_value('pro_domain'),'id="demo2" class="pro_activeinput"'); ?><br />
							</div>
							
							<label>Project Details</label><br />
							<textarea name="pro_details" class="pro_activeinput" rows="4"></textarea><br />
							<label>Project Date</label>
							<input type="date" name="pro_date" class="pro_activeinput" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems2">
								
								</tbody>
							</table>
						</div>
						
					</div>
					<a href="#" id="add_new_projects">Add New Projects</a>
				</div>
				
			</div>
			<!------------------------------------------------------------>
			<!--------------------------Courses--------------------------->
			<!------------------------------------------------------------>
			<hr />
			<h4>Course</h4>
			<hr />
			<div class="input_fields_course">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Course Name</label>
							<?php echo form_input('course_name',set_value('course_name'),'class="course_activeinput"');?><br />
							<label>Course Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('course_domain',set_value('course_domain'),'id="demo3" class="course_activeinput"'); ?><br />
							</div>
							
							<label>Course Details</label><br />
							<textarea name="course_details" class="course_activeinput" rows="4"></textarea><br />
							<label>Course Date</label>
							<input type="date" name="course_date" class="course_activeinput" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems3">
								
								</tbody>
							</table>
						</div>
						
					</div>
					<a href="#" id="add_new_courses">Add New Courses</a>
				</div>
				
			</div>
			<?php echo form_submit('Save','Add','');?>
		</div>
	</body> 
</html>
