-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2015 at 10:41 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `e-journal-management-system`
--
CREATE DATABASE IF NOT EXISTS `e-journal-management-system` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `e-journal-management-system`;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_paper`
--

CREATE TABLE IF NOT EXISTS `assigned_paper` (
  `ap_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ap_paper_id` bigint(20) unsigned NOT NULL,
  `ap_judge_id` int(11) unsigned NOT NULL,
  `ap_linkmatrix_rank` float NOT NULL,
  `ap_assign_start_date` date NOT NULL,
  `ap_assign_end_date` date NOT NULL,
  `ap_is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ap_id`),
  KEY `ap_judge_id` (`ap_judge_id`),
  KEY `ap_paper_id` (`ap_paper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=236 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `com_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `com_user_id` int(11) unsigned NOT NULL,
  `com_text` text COLLATE utf8_unicode_ci NOT NULL,
  `com_creation_date` date NOT NULL,
  `com_status` int(10) unsigned NOT NULL,
  `com_paper_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`com_id`),
  KEY `IXFK_comment_Paper` (`com_paper_id`),
  KEY `IXFK_comment_users` (`com_user_id`),
  KEY `com_status` (`com_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `concept`
--

CREATE TABLE IF NOT EXISTS `concept` (
  `concept_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `prog_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`concept_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=98 ;

--
-- Dumping data for table `concept`
--

INSERT INTO `concept` (`concept_id`, `name`, `prog_name`, `parent_id`, `valid`) VALUES
(1, 'Ite', '', NULL, 0),
(2, 'Data Base', 'data_base', 1, 1),
(3, 'DBMS', 'dbms', 2, 1),
(4, 'ORDB', 'ordb', 2, 1),
(5, 'Transaction Management', 'transaction_management', 2, 1),
(6, 'Data Mining', 'data_mining', 2, 1),
(7, 'Query Processing', 'query_processing', 2, 1),
(8, 'Object Oriented Data Base', 'object_oriented_dat_base', 2, 1),
(9, 'Data Warehousing', 'data_warehousing', 2, 1),
(10, 'Web', 'web', 1, 1),
(11, 'Business intelligent', 'business_intelligent', 10, 0),
(12, 'Cloud Computing', 'cloud_computing', 10, 1),
(13, 'Web Technology', 'web_technology', 10, 1),
(14, 'JQuery', 'Jquery', 13, 1),
(15, 'Java Script', 'javascript', 13, 1),
(16, 'Css', 'css', 13, 1),
(17, 'XML', 'xml', 13, 1),
(18, 'HTML', 'html', 13, 1),
(19, 'RDF', 'rdf', 13, 1),
(20, 'Ontology', 'ontology', 10, 1),
(21, 'Data Set', 'data_set', 10, 1),
(22, 'Semantic Web', 'semantic_web', 10, 1),
(23, 'Web Agent', 'web_agent', 10, 1),
(24, 'Big Data', 'big_data', 10, 1),
(25, 'Linked Data', 'linked_data', 10, 1),
(26, 'Formal Model', 'formal_model', 10, 1),
(27, 'Boolean Model', 'boolean_model', 26, 1),
(28, 'Inference Model', 'inference_model', 26, 1),
(29, 'Vector Model', 'vector_model', 26, 1),
(30, 'Latent Semantic Analysis Model', 'latent_semantic_analysis_model', 26, 1),
(31, 'Logical Model', 'logical_model', 26, 1),
(32, 'Electronic Information System', 'electronic_information_system', 10, 0),
(33, 'Electronic Government', 'electronic_government', 32, 1),
(35, 'Electronic Journal', 'electronic_journal', 32, 1),
(36, 'Electronic Health', 'electronic_health', 32, 1),
(37, 'Information System Engineering', 'information_system_engineering', 1, 1),
(38, 'Work Flow Diagram', 'work_flow_diagram', 37, 1),
(39, 'Request Engineering', 'request_engineering', 37, 1),
(40, 'Software requirements specificatio', 'software_requirements_specificatio', 37, 1),
(41, 'Processing Model', 'processing_model', 37, 1),
(42, 'Use Case Diagram', 'use_case_diagram', 37, 1),
(43, 'Business Logic', 'business_logic', 37, 1),
(44, 'Software Engineering', 'software_engineering', 1, 1),
(45, 'Quality Management', 'quality_management', 44, 1),
(46, 'Risk Management', 'risk_management', 44, 1),
(47, 'Models', 'models', 44, 1),
(48, 'Agile Methode', 'agile_methode', 47, 1),
(49, 'Reused Model', 'reused_model', 47, 1),
(50, 'Spiral Model', 'spiral_model', 47, 1),
(51, 'Incremental Model', 'incremental_model', 47, 1),
(52, 'Waterfall Model', 'waterfall_model', 47, 1),
(53, 'Unified Process', 'unified_process', 47, 1),
(54, 'XP', 'xp', 47, 0),
(55, 'UML', 'uml', 44, 1),
(56, 'Software Measures', 'software_measures', 44, 1),
(57, 'Time', 'time', 56, 1),
(58, 'Cost', 'cost', 56, 1),
(59, 'Quality', 'quality', 56, 1),
(60, 'Pitre Net', 'pitre_net', 44, 1),
(61, 'Meta Model', 'meta_model', 44, 1),
(62, 'Design Pattern', 'design_pattern', 44, 1),
(63, 'Model Driven Architecture', 'model_driven_architecture', 44, 1),
(64, 'MDE', 'mde', 44, 1),
(65, 'Compiler', 'compiler', 1, 1),
(66, 'Lexical Analyzer', 'lexical_analyzer', 65, 1),
(67, 'Syntactical Analyzer', 'syntactical_analyzer', 65, 1),
(68, 'Code Generation', 'code_generation', 65, 1),
(69, 'Semantic Chicker', 'semantic_chicker', 65, 1),
(70, 'Type Checking', 'type_checking', 65, 1),
(71, 'Database Servers', 'database_servers', 2, 1),
(72, 'MySQL', 'mysql', 71, 1),
(73, 'MSSQL', 'mssql', 71, 1),
(74, 'Oracle', 'oracle', 71, 1),
(75, 'Distributed Database', 'distributed_database', 2, 1),
(76, 'Computing for data Analysis', 'computing_for_data_analysis', 2, 1),
(77, 'Web Development', 'web_development', 10, 0),
(78, 'Web Application Security', 'web_application_security', 10, 1),
(79, 'Information Retrieval', 'information_retrieval', 10, 1),
(80, 'Web Administrator', 'web_administrator', 10, 1),
(81, 'Web Intelligence', 'web_intelligence', 10, 1),
(82, 'Educational Technology', 'educational_technology', 10, 1),
(83, 'Learning Management System', 'learning_management_system', 82, 1),
(84, 'Learning Content Management System', 'learning_content_management_system', 82, 1),
(85, 'E-Learning', 'elearning', 82, 1),
(86, 'PHP', 'php', 13, 1),
(87, 'JSF', 'jsf', 13, 1),
(88, 'ASP.Net', 'asp', 13, 1),
(89, 'Bootstrap', 'bootstrap', 13, 1),
(90, 'AJAX', 'ajax', 13, 1),
(91, 'Angular Js', 'angular_js', 13, 1),
(92, 'Ember Js', 'ember_js', 13, 1),
(93, 'MVC', 'mvc', 13, 1),
(94, 'Web Services', 'web_services', 13, 1),
(95, 'Testing', 'testing', 44, 1),
(96, 'Software Documentation', 'software_documentation', 44, 1),
(97, 'Aspect', 'aspect', 44, 1);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `date_time` date NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=141 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `profile_id`, `name`, `details`, `date_time`) VALUES
(19, 71, '•	Computing for Data Analysis', 'In this course you will learn how to program in R and how to use R for effective data analysis. You will learn how to install and configure software necessary for a statistical programming environment, discuss generic programming language concepts as they are implemented in a high-level statistical language. The course covers practical issues in statistical computing which includes programming in R, reading data into R, creating informative data graphics, accessing R packages, creating R packages with documentation, writing R functions, debugging, and organizing and commenting R code. Topics in statistical data analysis and optimization will provide working examples.', '2010-01-01'),
(20, 71, 'Big Data', 'Big data is a buzzword, or catch-phrase, used to describe a massive volume of both structured and unstructured data that is so large it is difficult to process using traditional database andsoftware techniques. In most enterprise scenarios the volume of data is too big or it moves too fast or it exceeds current processing capacity. Despite these problems, big data has the potential to help companies improve operations and make faster, more intelligent decisions.', '2015-03-04'),
(21, 72, 'Linked Data', 'Linked Data is about using the Web to connect related data that wasn''t previously linked, or using the Web to lower the barriers to linking data currently linked using other methods. More specifically, Wikipedia defines Linked Data as "a term used to describe a recommended best practice for exposing, sharing, and connecting pieces of data, information, and knowledge on the Semantic Web using URIs and RDF.', '2015-01-01'),
(22, 72, 'Semantic web', 'The Semantic Web is still a web, a collection of linked nodes.\nNavigation of links is currently, and will remain for humans if\nnot machines, a key mechanism for exploring the space. The\nSemantic Web is viewed by many as a knowledge base, a\ndatabase or an indexed and searchable document collection; in\nthe work discussed here we view it as a hypertext.\nThe aim of the COHSE project is to research into methods to\nimprove significantly the quality, consistency and breadth of\nlinking of Web documents at retrieval time (as readers browse\nthe documents) and authoring time (as authors create the\ndocuments). The objective is link creation rather than\nresource discovery; in contrast, many existing projects are\nconcerned primarily with the discovery of resources (reading),\nrather than the construction of hypertexts (authoring). The\nproject plans to produce a COHSE (Conceptual Open\nHypermedia ServicE) by integrating an ontological reasoning\nservice with a Web-based Open Hypermedia link service. This\nwill form a Conceptual Hypermedia system enabling\ndocuments to be linked via metadata describing their contents.\nThe bringing together of Open Hypermedia and Ontology\nservices can be seen as one particular implementation of the\nSemantic Web. Here we briefly present open and conceptual\nhypermedia, and introduce the architecture being employed\nwithin the COHSE project, and the prototype COHSE\nplatform we have developed. We present the questions that we\nnow plan to address that surround the Semantic Web when\nviewed from the perspective of a hypertext for people..\n', '2015-03-04'),
(23, 73, 'MDA', 'Model-driven architecture (MDA) is a software design approach for the development of software systems. It provides a set of guidelines for the structuring of specifications, which are expressed as models. Model-driven architecture is a kind of domain engineering, and supports model-driven engineering of software systems. It was launched by theObject Management Group (OMG) in 2001.[1]', '2001-01-01'),
(24, 73, 'petri Net', 'A Petri net (also known as a place/transition net or P/T net) is one of several mathematical modeling languages for the description of distributed systems. A Petri net is a directed bipartite graph, in which the nodes represent transitions (i.e. events that may occur, signified by bars) and places (i.e. conditions, signified by circles). The directed arcs describe which places are pre- and/or postconditions for which transitions (signified by arrows). Some sources[1] state that Petri nets were invented in August 1939 by Carl Adam Petri — at the age of 13 — for the purpose of describing chemical processes.', '2014-03-04'),
(25, 73, 'Aspect', 'Aspects redirects here. For the musical group, see Aspects (band)\nAspect may be:\n\nAspect (computer programming), a feature that is linked to many parts of a program, but which is not necessarily the primary function of the program.\nGrammatical aspect, in linguistics, a component of the conjugation of a verb, having to do with the internal temporal flow of an event\nLexical aspect, in linguistics, a distinction among different kinds of verb according to their relation to time\nAstrological aspect, the relative angle between two heavenly bodies\nAspect (geography), the direction in which a slope faces\nAspect (trade union), a trade union in the United Kingdom\nAn anatomic term, see Anatomical terms of location\nAspect (Dungeons & Dragons), aspect refers to a figure which is the representation of a god\nASPECT (Program), is a scientific computer software for mantle convection models\nMap projection#Aspect, the orientation of a map projections', '2015-04-04'),
(26, 74, 'OODB', 'An object database (also object-oriented database management system) is a database management system in which information is represented in the form of objects as used in object-oriented programming. Object databases are different from relational databases which are table-oriented. Object-relational databases are a hybrid of both approaches.\nObject databases have been considered since the early 1980s.[2]\n', '2015-01-01'),
(27, 74, 'Data Warehousing', 'In computing, a data warehouse (DW or DWH), also known as an enterprise data warehouse (EDW), is a system used for reporting anddata analysis. DWs are central repositories of integrated data from one or more disparate sources. They store current and historical data and are used for creating analytical reports for knowledge workers throughout the enterprise. Examples of reports could range from annual and quarterly comparisons and trends to detailed daily sales analyses.\nThe data stored in the warehouse is uploaded from the operational systems (such as marketing, sales, etc., shown in the figure to the right). The data may pass through an operational data store for additional operations before it is used in the DW for reporting.', '2014-03-04'),
(28, 74, 'Data Mining', 'Data mining (the analysis step of the "Knowledge Discovery in Databases" process, or KDD),[1] an interdisciplinary subfield ofcomputer science,[2][3][4] is the computational process of discovering patterns in large data sets involving methods at the intersection of artificial intelligence, machine learning, statistics, and database systems.[2] The overall goal of the data mining process is to extract information from a data set and transform it into an understandable structure for further use.[2] Aside from the raw analysis step, it involves database and data management aspects, data pre-processing, model and inference considerations, interestingness metrics, complexity considerations, post-processing of discovered structures, visualization, and online updating.[2]', '2015-04-02'),
(29, 75, 'Workflow Diagram', 'A workflow diagram depicts a series of actions that define a job or how work should be done. A workflow diagram visualizes how tasks will flow between resources, whether they''re machines or people and what conditions allow the sequence to move forward. This workflow can be illustrated or described with aflowchart using abstract boxes and diamonds or it can be created with depictions of real-life objects using graphics and pictures that represent customers, forms, finance, products, shipping, payments, and more. For software development, a workflow diagram defines a series of steps a process must execute consistently.', '2015-01-05'),
(30, 75, 'Requrements engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\nIn the waterfall model,[6] requirements engineering is presented as the first phase of the development process. Later software development methods, including the Rational Unified Process (RUP), extreme programming (XP) and Scrum assume that requirements engineering continues through the lifetime of a system.\nAlan M. Davis maintains an extensive bibliography of requirements engineering.[7]\n', '2012-03-04'),
(31, 76, 'E-Learning', 'e-learning is electronic learning, and typically this means using a computer to deliver part, or all of a course whether it''s in a school, part of your mandatory business training or a full distance learning course.\nIn the early days it received a bad press, as many people thought bringing computers into the classroom would remove that human element that some learners need, but as time has progressed technology has developed, and now we embrace smartphones and tablets in the classroom and office, as well as using a wealth of interactive designs that makes distance learning not only engaging for the users, but valuable as a lesson delivery medium.\nBuilding partnerships with quality training providers, and combining this with a dedicated experienced technical team and support staff, Virtual College provides the perfect blended learning environment, offering anyone the chance to take their online training to the next level.\n', '2000-01-06'),
(32, 76, 'Educational technology', 'Educational technology is the effective use of technological tools in learning. As a concept, it concerns an array of tools, such as media, machines and networking hardware, as well as considering underlying theoretical perspectives for their effective application.[1][2] ', '1998-03-04'),
(33, 77, 'Software Engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications.', '2003-01-06'),
(34, 77, 'Data base', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.', '2003-03-04'),
(35, 77, 'DBMS', 'Database management systems (DBMS) are computer software applications that interact with the user, other applications, and the database itself to capture and analyze data. A general-purpose DBMS is designed to allow the definition, creation, querying, update, and administration of databases. Well-known DBMSs include MySQL, PostgreSQL,Microsoft SQL Server, Oracle, Sybase and IBM DB2. A database is not generally portable across different DBMSs, but different DBMS can interoperate by using standards such as SQL and ODBC or JDBC to allow a single application to work with more than one DBMS. Database management systems are often classified according to the database modelthat they support; the most popular database systems since the 1980s have all supported the relational model as represented by the SQL language. Sometimes a DBMS is loosely referred to as a ''database''.', '2004-03-03'),
(36, 78, 'Quality management', 'Quality management ensures that an organization, product or service is consistent. It has four main components: quality planning, quality control, quality assurance and quality improvement.[1] Quality management is focused not only on product and service quality, but also on the means to achieve it. Quality management, therefore, uses quality assurance and control of processes as well as products to achieve more consistent quality.', '2013-01-01'),
(37, 78, 'ISE', 'An Information System (IS) is a system composed of people and computers that processes or interprets information. [1][2][3] The term is also sometimes used in more restricted senses to refer to only the software used to run a computerized database or to refer to only a computer system.', '2013-03-04'),
(38, 78, 'Requirement engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.', '2000-03-03'),
(39, 78, 'Business Process', 'A business process or business method is a collection of related, structured activities or tasks that produce a specific service or product (serve a particular goal) for a particular customer or customers. It may often be visualized as a flowchart of a sequence of activities with interleaving decision points or as a Process Matrix of a sequence of activities with relevance rules based on data in the process.', '2013-12-05'),
(40, 78, 'Database', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies', '2013-12-06'),
(41, 79, 'Web technology', 'Web technology for developers\nThe open Web presents incredible opportunities for developers. To take full advantage of these technologies, you need to know how to use them. Below you''ll find the links to MDN''s documentation on Web technologies.\n', '2000-11-01'),
(42, 79, 'Web services', 'A Web service is a method of communication between two electronic devices over a network. It is a software function provided at a network address over the Web with the service always on as in the concept of utility computing. The W3C defines a Web service generally as:-\na software system designed to support interoperable machine-to-machine interaction over a network.[1]\n', '2000-03-04'),
(43, 80, 'ISE', 'An Information System (IS) is a system composed of people and computers that processes or interprets information. [1][2][3] The term is also sometimes used in more restricted senses to refer to only the software used to run a computerized database or to refer to only a computer system.documentation on Web technologies.', '2000-01-01'),
(44, 80, 'S-E', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications.', '2000-03-04'),
(45, 80, 'Quality Management', 'Quality management ensures that an organization, product or service is consistent. It has four main components: quality planning, quality control, quality assurance and quality improvement.[1] Quality management is focused not only on product and service quality, but also on the means to achieve it. Quality management, therefore, uses quality assurance and control of processes as well as products to achieve more consistent quality.', '2001-03-04'),
(46, 80, 'Risk managemet', 'Risk management is the identification, assessment, and prioritization of risks (defined in ISO 31000 as the effect of uncertainty on objectives) followed by coordinated and economical application of resources to minimize, monitor, and control the probability and/or impact of unfortunate events[1] or to maximize the realization of opportunities. Risk management’s objective is to assure uncertainty does not deviate the endeavor from the business goals.[2]', '2001-01-07'),
(47, 81, 'Software Decumentation', 'Software documentation is written text that accompanies computer software. It either explains how it operates or how to use it, and may mean different things to people in different roles.\nDocumentation is an important part of software engineering. Types of documentation include:\n1.	Requirements - Statements that identify attributes, capabilities, characteristics, or qualities of a system. This is the foundation for what shall be or has been implemented.\n2.	Architecture/Design - Overview of software. Includes relations to an environment and construction principles to be used in design of software components.\n3.	Technical - Documentation of code, algorithms, interfaces, and APIs.\n4.	End user - Manuals for the end-user, system administrators and support staff.\n5.	Marketing - How to market the product and analysis of the market demand.\n \n', '2010-01-02'),
(48, 81, 'Processing model', 'Process models are processes of the same nature that are classified together into a model. Thus, a process model is a description of a process at the type level. Since the process model is at the type level, a process is an instantiation of it. The same process model is used repeatedly for the development of many applications and thus, has many instantiations. One possible use of a process model is to prescribe how things must/should/could be done in contrast to the process itself which is really what happens. A process model is roughly an anticipation of what the process will look like. What the process shall be will be determined during actual system development.[2]\n', '2011-03-04'),
(49, 81, 'Business Process', 'A business process or business method is a collection of related, structured activities or tasks that produce a specific service or product (serve a particular goal) for a particular customer or customers. It may often be visualized as a flowchart of a sequence of activities with interleaving decision points or as a Process Matrix of a sequence of activities with relevance rules based on data in the process.', '2001-01-02'),
(50, 81, 'Risk managemet', 'Risk management is the identification, assessment, and prioritization of risks (defined in ISO 31000 as the effect of uncertainty on objectives) followed by coordinated and economical application of resources to minimize, monitor, and control the probability and/or impact of unfortunate events[1] or to maximize the realization of opportunities. Risk management’s objective is to assure uncertainty does not deviate the endeavor from the business goals.[2]', '2012-01-07'),
(51, 81, 'Requirements engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\n', '2012-01-09'),
(52, 82, 'Software measures', 'Software measurement is a quantified attribute (see also: measurement) of a characteristic of a software product or the software process. It is a discipline within software engineering. The content of software measurement is defined and governed by ISO Standard ISO 15939 (software measurement process). ', '2010-01-01'),
(53, 82, 'Software Documentation', 'Software documentation is written text that accompanies computer software. It either explains how it operates or how to use it, and may mean different things to people in different roles.\nDocumentation is an important part of software engineering. Types of documentation include:\n1.	Requirements - Statements that identify attributes, capabilities, characteristics, or qualities of a system. This is the foundation for what shall be or has been implemented.\n2.	Architecture/Design - Overview of software. Includes relations to an environment and construction principles to be used in design of software components.\n3.	Technical - Documentation of code, algorithms, interfaces, and APIs.\n4.	End user - Manuals for the end-user, system administrators and support staff.\n5.	Marketing - How to market the product and analysis of the market demand.\n', '2011-03-04'),
(54, 82, 'Risk managemet', 'Risk management is the identification, assessment, and prioritization of risks (defined in ISO 31000 as the effect of uncertainty on objectives) followed by coordinated and economical application of resources to minimize, monitor, and control the probability and/or impact of unfortunate events[1] or to maximize the realization of opportunities. Risk management’s objective is to assure uncertainty does not deviate the endeavor from the business goals.[2]', '2012-01-07'),
(55, 82, 'compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-07'),
(56, 82, 'Software measures', 'Software measurement is a quantified attribute (see also: measurement) of a characteristic of a software product or the software process. It is a discipline within software engineering. The content of software measurement is defined and governed by ISO Standard ISO 15939 (software measurement process). ', '2010-01-01'),
(57, 82, 'Requirements engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\n', '2012-01-09'),
(58, 82, 'Software Documentation', 'Software documentation is written text that accompanies computer software. It either explains how it operates or how to use it, and may mean different things to people in different roles.\nDocumentation is an important part of software engineering. Types of documentation include:\n1.	Requirements - Statements that identify attributes, capabilities, characteristics, or qualities of a system. This is the foundation for what shall be or has been implemented.\n2.	Architecture/Design - Overview of software. Includes relations to an environment and construction principles to be used in design of software components.\n3.	Technical - Documentation of code, algorithms, interfaces, and APIs.\n4.	End user - Manuals for the end-user, system administrators and support staff.\n5.	Marketing - How to market the product and analysis of the market demand.\n', '2011-03-04'),
(59, 82, 'Risk managemet', 'Risk management is the identification, assessment, and prioritization of risks (defined in ISO 31000 as the effect of uncertainty on objectives) followed by coordinated and economical application of resources to minimize, monitor, and control the probability and/or impact of unfortunate events[1] or to maximize the realization of opportunities. Risk management’s objective is to assure uncertainty does not deviate the endeavor from the business goals.[2]', '2012-01-07'),
(60, 82, 'compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-07'),
(61, 82, 'Requirements engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\n', '2012-01-09'),
(62, 82, 'Requirements engineering', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\n', '2012-01-09'),
(63, 83, 'php', 'PHP is a server-side scripting language created in 1995 and designed for web development but also used as a general-purpose programming language. As of January 2013, PHP was installed on more than 240 million websites (39% of those sampled) and 2.1 million web servers.[3] Originally created by Rasmus Lerdorf in 1994,[4] the reference implementation of PHP (powered by the Zend Engine) is now produced by The PHP Group.[5] While PHP originally stood for Personal Home Page,[4] it now stands for PHP: Hypertext Preprocessor, which is a recursive backronym.[6]', '2010-01-01'),
(64, 83, 'HTML', 'HyperText Markup Language, commonly referred to as HTML, is the standard markup language used to create web pages.[1] It is written in the form of HTML elements consisting of tags enclosed in angle brackets (like <html>). HTML tags most commonly come in pairs like <h1> and </h1>, although some represent empty elements and so are unpaired, for example <img>. The first tag in such a pair is the start tag, and the second is the end tag (they are also called opening tags and closing tags).', '2011-03-05'),
(65, 84, 'php', 'PHP is a server-side scripting language created in 1995 and designed for web development but also used as a general-purpose programming language. As of January 2013, PHP was installed on more than 240 million websites (39% of those sampled) and 2.1 million web servers.[3] Originally created by Rasmus Lerdorf in 1994,[4] the reference implementation of PHP (powered by the Zend Engine) is now produced by The PHP Group.[5] While PHP originally stood for Personal Home Page,[4] it now stands for PHP: Hypertext Preprocessor, which is a recursive backronym.[6]', '2010-01-01'),
(66, 84, 'Web administration ', 'Web administration is one of the most important, but overlooked aspects of Web development. If you don''t have a good Web administrator keeping your Web site running, well, you won''t have a Web site.', '2010-01-01'),
(67, 84, 'Database', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.', '2008-04-04'),
(68, 85, 'php', 'PHP is a server-side scripting language created in 1995 and designed for web development but also used as a general-purpose programming language. As of January 2013, PHP was installed on more than 240 million websites (39% of those sampled) and 2.1 million web servers.[3] Originally created by Rasmus Lerdorf in 1994,[4] the reference implementation of PHP (powered by the Zend Engine) is now produced by The PHP Group.[5] While PHP originally stood for Personal Home Page,[4] it now stands for PHP: Hypertext Preprocessor, which is a recursive backronym.[6]', '2010-01-01'),
(69, 85, 'Web administration ', 'Web administration is one of the most important, but overlooked aspects of Web development. If you don''t have a good Web administrator keeping your Web site running, well, you won''t have a Web site.', '2010-01-01'),
(70, 85, 'Database', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.', '2008-04-04'),
(71, 86, 'web technology', 'Web technology for developers\nThe open Web presents incredible opportunities for developers. To take full advantage of these technologies, you need to know how to use them. Below you''ll find the links to MDN''s documentation on Web technologies\n', '2010-01-01'),
(72, 86, 'Web administration ', 'Web administration is one of the most important, but overlooked aspects of Web development. If you don''t have a good Web administrator keeping your Web site running, well, you won''t have a Web site.', '2010-01-01'),
(73, 86, 'Database', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.', '2008-04-04'),
(74, 87, 'web technology', 'Web technology for developers\nThe open Web presents incredible opportunities for developers. To take full advantage of these technologies, you need to know how to use them. Below you''ll find the links to MDN''s documentation on Web technologies\n', '2010-01-01'),
(75, 87, 'Web security ', 'Web application security is a branch of Information Security that deals specifically with security of websites, web applications and web services.\nAt a high level, Web application security draws on the principles of application security but applies them specifically to Internet and Web systems. Typically web applications are developed using programming languages such as PHP, Java EE, Java, Python, Ruby, ASP.NET, C#, VB.NET or Classic ASP.\n', '2010-01-01'),
(76, 87, 'Database', 'A database is an organized collection of data.[1] The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.', '2008-04-04'),
(77, 88, 'Business Process', 'A business process or business method is a collection of related, structured activities or tasks that produce a specific service or product (serve a particular goal) for a particular customer or customers. It may often be visualized as a flowchart of a sequence of activities with interleaving decision points or as a Process Matrix of a sequence of activities with relevance rules based on data in the process.', '2010-01-01'),
(78, 88, 'Use case diagram ', 'A use case diagram at its simplest is a representation of a user''s interaction with the system that shows the relationship between the user and the different use cases in which the user is involved. A use case diagram can identify the different types of users of a system and the different use cases and will often be accompanied by other types of diagrams as well.', '2011-05-07'),
(79, 88, 'Database servers', 'A database server is a computer program that provides database services to other computer programs or computers, as defined by the client–server model. The term may also refer to a computer dedicated to running such a program. Database management systems frequently provide database server functionality, and some DBMSs (e.g., MySQL) rely exclusively on the client–server model for database access.\nSuch a server is accessed either through a "front end" running on the user’s computer which displays requested data or the "back end" which runs on the server and handles tasks such as data analysis and storage.\n', '2008-04-04'),
(80, 88, 'Workflow diagram', 'A workflow diagram depicts a series of actions that define a job or how work should be done. A workflow diagram visualizes how tasks will flow between resources, whether they''re machines or people and what conditions allow the sequence to move forward. This workflow can be illustrated or described with aflowchart using abstract boxes and diamonds or it can be created with depictions of real-life objects using graphics and pictures that represent customers, forms, finance, products, shipping, payments, and more. For software development, a workflow diagram defines a series of steps a process must execute consistently.', '2012-04-07'),
(81, 89, 'JSF', 'JavaServer Faces (JSF) is a Java specification for building component-based user interfaces for web applications.[1] It was formalized as a standard through the Java Community Process and is part of the Java Platform, Enterprise Edition.\nJSF 2 uses Facelets as its default templating system. Other view technologies such as XUL can also be employed. In contrast, JSF 1.x uses JavaServer Pages (JSP) as its default templating system.\n', '2010-01-01'),
(82, 89, 'Oracle ', 'Oracle Database (commonly referred to as Oracle RDBMS or simply as Oracle) is an object-relational database management system[3] produced and marketed by Oracle Corporation.\nLarry Ellison and two friends and former co-workers, Bob Miner and Ed Oates, started a consultancy called Software Development Laboratories (SDL) in 1977. SDL developed the original version of the Oracle software. The name Oracle comes from the code-name of a CIA-funded project Ellison had worked on while previously employed by Ampex.[4]\n', '2011-05-07'),
(83, 89, 'Mysql', 'MySQL (/maɪ ˌɛskjuːˈɛl/ "My S-Q-L",[6] officially, but also called /maɪ ˈsiːkwəl/ "My Sequel") is (as of July 2013) the world''s second most[a] widely used relational database management system (RDBMS)[7] and most widely used open-source RDBMS.[8] It is named after co-founder Michael Widenius''s daughter, My.[9] The SQL acronym stands for Structured Query Language.', '2010-04-04'),
(84, 89, 'E-Government ', 'E-government (short for electronic government, also known as e-gov, Internet government, digital government, online government, or connected government) consists of the digital interactions between a citizen and their government (C2G), between governments and government agencies (G2G), between government and citizens (G2C), between government and employees (G2E), and between government and businesses/commerce (G2B). Essentially, e-government delivery models can be briefly summed up as (Jeong, 2007):[1]', '2011-04-07'),
(85, 90, 'ISE', 'An Information System (IS) is a system composed of people and computers that processes or interprets information. [1][2][3] The term is also sometimes used in more restricted senses to refer to only the software used to run a computerized database or to refer to only a computer system.\nInformation systems is an academic study of systems with a specific reference to information and the complementary networks of hardware and software that people and organizations use to collect, filter, process, create and also distribute data. An emphasis is placed on an Information System having a definitive Boundary, Users, Processors, Stores, Inputs, Outputs and the aforementioned communication networks [4]\n', '2000-01-01'),
(86, 90, 'Requirements engineering ', 'Requirements engineering (RE)[1] refers to the process of defining, documenting and maintaining requirements[2][3] and to the subfields of systems engineering and software engineering concerned with this process.\nThe first use of the term ''requirements engineering'' was probably in 1979 in a TRW technical report[4] but did not come into general use until the 1990s with the publication of anIEEE Computer Society tutorial[5] and the establishment of a conference series on requirements engineering.\nIn the waterfall model,[6] requirements engineering is presented as the first phase of the development process. Later software development methods, including the Rational Unified Process (RUP), extreme programming (XP) and Scrum assume that requirements engineering continues through the lifetime of a system.\nAlan M. Davis maintains an extensive bibliography of requirements engineering.[7]\n', '2011-05-07'),
(87, 91, 'Software engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications.', '2011-01-01'),
(88, 91, 'compiler ', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2011-05-07'),
(89, 92, 'Software engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications."', '2010-01-01'),
(90, 92, 'compiler ', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program', '2010-05-07'),
(92, 93, 'Software engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications."', '2011-01-01'),
(93, 93, 'ISE', 'An Information System (IS) is a system composed of people and computers that processes or interprets information. [1][2][3] The term is also sometimes used in more restricted senses to refer to only the software used to run a computerized database or to refer to only a computer system.\nInformation systems is an academic study of systems with a specific reference to information and the complementary networks of hardware and software that people and organizations use to collect, filter, process, create and also distribute data. An emphasis is placed on an Information System having a definitive Boundary, Users, Processors, Stores, Inputs, Outputs and the aforementioned communication networks [4]\n', '2014-05-04'),
(94, 94, 'Software engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications."', '2000-01-01'),
(95, 94, 'ISE', 'An Information System (IS) is a system composed of people and computers that processes or interprets information. [1][2][3] The term is also sometimes used in more restricted senses to refer to only the software used to run a computerized database or to refer to only a computer system.\nInformation systems is an academic study of systems with a specific reference to information and the complementary networks of hardware and software that people and organizations use to collect, filter, process, create and also distribute data. An emphasis is placed on an Information System having a definitive Boundary, Users, Processors, Stores, Inputs, Outputs and the aforementioned communication networks [4]\n', '2011-05-04'),
(96, 95, 'Software engineering', 'Software engineering is the study and an application of engineering to the design, development, and maintenance ofsoftware.[1][2][3] The Bureau of Labor Statistics'' definition is "Research, design, develop, and test operating systems-level software, compilers, and network distribution software for medical, industrial, military, communications, aerospace, business, scientific, and general computing applications."', '2000-01-01'),
(97, 95, 'oracle', 'The Oracle Corporation is a multinational computer technology corporation headquartered in Redwood City, California, United States. The company specializes in developing and marketing computer hardware systems and enterprise software products – particularly its own brands of database management systems. In 2011 Oracle was the second-largest software maker by revenue, after Microsoft.[3]', '2000-09-01'),
(98, 95, '', 'Information retrieval (IR) is the activity of obtaining information resources relevant to an information need from a collection of information resources. Searches can be based on metadata or on full-text (or other content-based) indexing.\nAutomated information retrieval systems are used to reduce what has been called "information overload". Many universities andpublic libraries use IR systems to provide access to books, journals and other documents. Web search engines are the most visibleIR applications.\n \n', '2013-07-04'),
(99, 96, 'UML', 'The Unified Modeling Language (UML) is a general-purpose modeling language in the field of software engineering, which is designed to provide a standard way to visualize the design of a system.[1]\nIt was created and developed by Grady Booch, Ivar Jacobson and James Rumbaugh at Rational Software during 1994–95, with further development led by them through 1996.[1]\nIn 1997 it was adopted as a standard by the Object Management Group (OMG), and has been managed by this organization ever since. In 2005 the Unified Modeling Language was also published by the International Organization for Standardization (ISO) as an approved ISO standard.[2] Since then it has been periodically revised to cover the latest revision of UML.[3]\n', '2010-01-10'),
(100, 96, 'Asp.net', 'Asp.net is an open source[2] server-side Web application framework designed for Web development to produce dynamic Web pages. It was developed by Microsoft to allow programmers to build dynamic web sites, web applications and web services.\nIt was first released in January 2002 with version 1.0 of the .NET Framework, and is the successor to Microsoft''s Active Server Pages (ASP) technology. ASP.NET is built on the Common Language Runtime (CLR), allowing programmers to write ASP.NET code using any supported .NET language. The ASP.NET SOAP extension framework allows ASP.NET components to process SOAP messages.\n', '2011-09-01'),
(101, 96, 'Mysql', 'MySQL (/maɪ ˌɛskjuːˈɛl/ "My S-Q-L",[6] officially, but also called /maɪ ˈsiːkwəl/ "My Sequel") is (as of July 2013) the world''s second most[a] widely used relational database management system (RDBMS)[9] and most widely used open-source RDBMS.[10] It is named after co-founder Michael Widenius''s daughter, My.[11] The SQL acronym stands for Structured Query Language.\nThe MySQL development project has made its source code available under the terms of the GNU General Public License, as well as under a variety of proprietary agreements. MySQL was owned and sponsored by a single for-profit firm, the Swedishcompany MySQL AB, now owned by Oracle Corporation.[12] For proprietary use, several paid editions are available, and offer additional functionality.\n', '2013-07-04'),
(102, 97, 'Aspect', 'Aspects redirects here. For the musical group, see Aspects (band)\nAspect may be:\n•	Aspect (computer programming), a feature that is linked to many parts of a program, but which is not necessarily the primary function of the program.\n•	Grammatical aspect, in linguistics, a component of the conjugation of a verb, having to do with the internal temporal flow of an event\n•	Lexical aspect, in linguistics, a distinction among different kinds of verb according to their relation to time\n•	Astrological aspect, the relative angle between two heavenly bodies\n•	Aspect (geography), the direction in which a slope faces\n•	Aspect (trade union), a trade union in the United Kingdom\n•	An anatomic term, see Anatomical terms of location\n•	Aspect (Dungeons & Dragons), aspect refers to a figure which is the representation of a god\n•	ASPECT (Program), is a scientific computer software for mantle convection models\n•	Map projection#Aspect, the orientation of a map projections\n', '2004-01-10'),
(103, 97, 'Testing', 'Software testing is an investigation conducted to provide stakeholders with information about the quality of the product or service under test.[1] Software testing can also provide an objective, independent view of the software to allow the business to appreciate and understand the risks of software implementation. Test techniques include the process of executing a program or application with the intent of finding software bugs (errors or other defects)..', '2011-09-01'),
(104, 97, 'Software Documentation', 'Software documentation is written text that accompanies computer software. It either explains how it operates or how to use it, and may mean different things to people in different roles.\nDocumentation is an important part of software engineering. Types of documentation include:\n1.	Requirements - Statements that identify attributes, capabilities, characteristics, or qualities of a system. This is the foundation for what shall be or has been implemented.\n2.	Architecture/Design - Overview of software. Includes relations to an environment and construction principles to be used in design of software components.\n3.	Technical - Documentation of code, algorithms, interfaces, and APIs.\n4.	End user - Manuals for the end-user, system administrators and support staff.\nMarketing - How to market the product and analysis of the market demand\n', '2003-07-04'),
(105, 98, 'MDA', 'Model-driven architecture (MDA) is a software design approach for the development of software systems. It provides a set of guidelines for the structuring of specifications, which are expressed as models. Model-driven architecture is a kind of domain engineering, and supports model-driven engineering of software systems. It was launched by theObject Management Group (OMG) in 2001.[1]', '2004-01-10'),
(106, 98, 'Web Services', 'A Web service is a method of communication between two electronic devices over a network. It is a software function provided at a network address over the Web with the service always on as in the concept of utility computing. The W3C defines a Web service generally as:-\na software system designed to support interoperable machine-to-machine interaction over a network.[1]\n \n', '2009-12-04'),
(107, 99, 'DataBase', 'A database is an organized collection of data.[1] It is the collection of tables,queries,reports,views and other objects. The data is typically organized to model aspects of reality in a way that supports processes requiring information, such as modelling the availability of rooms in hotels in a way that supports finding a hotel with vacancies.\nDatabase management systems (DBMS) are computer software applications that interact with the user, other applications, and the database itself to capture and analyze data. A general-purpose DBMS is designed to allow the definition, creation, querying, update, and administration of databases. Well-known DBMSs include MySQL, PostgreSQL,Microsoft SQL Server, Oracle, Sybase and IBM DB2. A database is not generally portable across different DBMSs, but different DBMS can interoperate by using standards such as SQL and ODBC or JDBC to allow a single application to work with more than one DBMS. Database management systems are often classified according to the database modelthat they support; the most popular database systems since the 1980s have all supported the relational model as represented by the SQL language. Sometimes a DBMS is loosely referred to as a ''database''.\n', '2004-01-10'),
(108, 99, 'DBMS', 'A database management system (DBMS) is a computer program (or more typically, a suite of them) designed to manage a database, a large set of structured data, and run operations on the data requested by numerous users. Typical examples of DBMS use includeaccounting, human resources and customer support systems.\nOriginally found only in large companies with the computer hardware needed to support large data sets, DBMSs have more recently emerged as a fairly standard part of any company back office.\n', '2003-12-10');
INSERT INTO `course` (`course_id`, `profile_id`, `name`, `details`, `date_time`) VALUES
(109, 99, 'Data Warehouse', 'In computing, a data warehouse (DW or DWH), also known as an enterprise data warehouse (EDW), is a system used for reporting anddata analysis. DWs are central repositories of integrated data from one or more disparate sources. They store current and historical data and are used for creating analytical reports for knowledge workers throughout the enterprise. Examples of reports could range from annual and quarterly comparisons and trends to detailed daily sales analyses.\nThe data stored in the warehouse is uploaded from the operational systems (such as marketing, sales, etc., shown in the figure to the right). The data may pass through an operational data store for additional operations before it is used in the DW for reporting.', '2009-12-04'),
(110, 100, 'System versus SQL naming: part 2 - Accessing database objects', 'When running SQL statements, you can run them either using system or SQL naming convention. The previous article was focused on the differences in ownership and access authorities when creating database objects with the SQL and system naming convention. This article examines the behavior differences with system and SQL naming when accessing tables and views as well as stored procedures and user-defined functions (UDFs), focusing mainly on the different behaviors when using unqualified references to those objects', '2012-12-08'),
(111, 100, 'The power of user-defined table functions - Writing and using UDTFs', 'Data located in IBM® DB2® tables can be easily accessed by using the SQL SELECT statement. However, what about accessing data that is stored in non-relational objects, such as data areas, user spaces, or in text files located in the integrated file system (IFS)? The user-defined table function (UDTF) support in IBM DB2 for i makes it possible to access data in non-relational objects with a SELECT statement. UDTFs can also be used as a method of reusing complex SELECT statements similar to almost a view. This article examines all aspects of creating and using a UDTF.', '2014-08-07'),
(112, 101, 'Choosing a JavaScript Framework', 'Brian builds a “Todo” application using four different frameworks and discusses the strengths and weakness of each framework along the way, highlighting practical considerations in the selection of a framework.', '2014-12-06'),
(113, 101, 'Introduction to PHP MVC', 'An introduction to building a PHP MVC website using the CakePHP Framework. The course includes installing and setting up your environment and walks you through the process of creating a functional web application.', '2012-12-07'),
(114, 102, 'Real World Big Data in Azure', 'The Big Data components of Azure let you build solutions which can process billions of events, using technologies you already know. In this course, we build a real world Big Data solution in two phases, starting with just .NET technologies and then adding Hadoop tools.', '2010-12-10'),
(115, 102, 'Social Networks Big Data Consuming', 'Consumer product companies and retail organizations are monitoring social media like Facebook and Twitter to get an unprecedented view into customer behavior, preferences, and product perception.', '2011-03-11'),
(116, 103, 'LOD2: Creating Knowledge out of Interlinked Data', 'The outputs of this project will range from sci-tech to socio-economic areas by providing new technologies and an underlying scientific basis for these and by applying these new technologies to a number of Semantic Web areas experiencing commercial (enterprise search, media and publishing), scientific (extraction, interlinking, ontology classification and fusion methods), and sociological (community knowledge, integration in social networks, eGovernment) success at present. The project aims to contribute high-quality interlinked versions of public Semantic Web data sets, promoting their use in new cross-domain applications by developers across the globe. The new technologies for enabling scalable management of Linked Data collections in the many billions of triples will raise the state of the art of Semantic Web data management, both commercial and open-source, providing opportunities for new products and spin-offs, and make RDF a viable choice for organizations worldwide as a premier data management format. The algorithms and (open-source) tools that the project will develop for data cleaning, linking and fusing will help creating and bootstrapping new data sets in domains that go much beyond the direct applications and data sets developed in the context of this project, to reach the overall goal of the project of making Linked Data the model of choice for next-generation IT systems and applications.', '2012-12-06'),
(117, 103, 'SemaGrow', 'During the last years, the trend to open up data and provide them freely on the Internet has intensified in volume as well as quality and value of the data made available. But most of the low-hanging fruit has been picked and it is time to move on to the next step, combining, cross-indexing and, in general, making the best out of all public data, regardless of their schema, size, and update rate; accepting that some schemas might be better suited to a given dataset and application and that there is no consensus about a "universal" schema or vocabulary for any given application, let alone for the Semantic Web and related initiatives such as the LOD cloud. In other words, we need infrastructure that besides being efficient, real-time responsive and scalable is also flexible and robust enough to allow data providers to publish in the manner and form that best suits their processes and purposes and data consumers to query in the manner and form that best suits theirs.', '2013-01-03'),
(118, 104, 'SQL Server: Transact-SQL Basic Data Retrieval', 'Learn how to construct SELECT statements to retrieve data from SQL Server databases, applicable for developers from SQL Server 2005 onwards', '2009-12-12'),
(119, 104, 'Sentiment Analysis for Twitter ', 'Sentiment Analysis for Twitter ', '2012-02-09'),
(120, 105, 'Patterns of Cloud Integration', 'In this architecture-focused course, we will discuss how the classic application integration patterns apply to cloud scenarios. It includes demonstrations of key technologies and a review of challenges and benefits of 4 core patterns.', '2013-03-07'),
(121, 105, 'Getting Started with CloudFlare™ Security', 'CloudFlare™ provides a free service to encapsulate an existing website and route traffic through their infrastructure. This allows them to apply numerous defensive measures to help secure the site from a range of online risks. In this course, we''ll go through the process of setting up a site in CloudFlare™, assessing the security profile, then strengthening the configuration to maximize the value of the additional defenses.', '2014-07-09'),
(122, 106, 'Understanding Web Service Specifications in WCF', 'SOAP isn''t dead yet! While the world is moving more and more towards REST services, SOAP is still very much alive, especially in the area of application and enterprise integrations. This course explains the WS-* standards that extend SOAP to make rich integration scenario', '2013-12-22'),
(123, 106, 'Modernizing Your Websites with Azure Platform as a Service', '"The Cloud" is an increasingly attractive proposition for hosting websites online, and Microsoft Azure is an absolute standout. Their "platform as a service" offerings for both websites and databases offer fantastic features, massive scalability, and attractive pricing. But how will you migrate your sites? What will it cost? And how about security? This course answers those questions and paves the way to move from traditional hosting and into Microsoft''s cloud.', '2014-12-09'),
(124, 107, '', '', '0000-00-00'),
(125, 108, 'All You Need To Know About AngularJS - Training On AngularJS', 'This course covers all the basics of how to get up and running with AngularJS. An InDepth training course.', '2011-03-06'),
(126, 108, 'Design Patterns in Java: Creational', 'This course is part of a 3 part series covering design patterns using Java.', '2011-04-08'),
(127, 109, 'Web Intelligence', 'Web intelligence is a combination of digital analytics, which examines how website visitors view and interact with a site’s pages and features, and business intelligence, which allows a corporation’s management to use data on customer purchasing patterns, demographics, and demand trends to make effective strategic decisions. As companies expand their reach into the global marketplace, the need to analyze how customers use company websites to learn about products and make buying decisions is becoming increasingly critical to survival and ultimate success.\n\nUC Irvine Extension and the University of British Columbia Continuing Studies have combined their resources to offer this certificate program, which provides award-winning education on digital analytics and leading-edge courses on data warehousing, business intelligence, and other business topics. These courses give students a powerful set of skills and knowledge that companies need for growth and success in the 21st century.', '2012-12-03'),
(128, 110, 'Agile Methods in Stanford', 'Agile methods are challenging conventional wisdom regarding systems development processes and practices; effectively putting process on a diet and investing in people and teams. This course will enable today''s software development professional to understand the heart of agility and will cover both the theory and practice of agile methods.', '2011-03-12'),
(129, 111, 'ORDB with UML Class Diagram in an Advanced Database Course', 'Object-relational database technology emerged as a way of enhancing object-oriented features in relational database management systems (RDBMSs). In response to this evolutionary change, the author has incorporated the technology into her advanced database course. This paper presents a teaching case on using UML (Unified Modeling Language) for objectrelational database (ORDB) design and its implementation with Oracle. Course organization, course content, class activities, and impacts on the students'' learning outcomes are discussed. The paper is intended to provide a guide for database instructors who desire to incorporate object-relational technology and design in their traditional database courses.', '2015-07-21'),
(130, 111, 'Solving Relational Database Problems with ORDBMS in an Advanced Database Course', 'In this paper, the authors introduce how to use the Object-Relational DataBase Management System (ORDBMS) to overcome the Relational DataBase (RDB) existing problems and to improve database performance in the database development. The purpose of the paper is to provide a guideline for database instructors who desire to incorporate the ORDB technology in their traditional database courses. The paper presents how to use the specific Object-Relational DataBase (ORDB) technology to solve normalization problems in transitive dependency, multi-value attributes and non-1st normal form; and also provides the solutions to data complexity problems with specific ORDBMS techniques: object view, object inheritance and object integration.', '2014-12-03'),
(131, 112, 'Agile Fundamentals, ', 'Agile Fundamentals explores how working on an Agile project has benefits for your development team, your end users, and your organization as a whole.', '2014-03-06'),
(132, 113, 'Relationship Management and Tracking for Your Career', 'Career Management, networking and a job search can be hard work. Learn how why and how to use a personal career relationship manager (CRM) to help you.', '2014-02-05'),
(133, 114, '', '', '0000-00-00'),
(134, 115, 'MySQL Continuous Database Delivery with Flyway', 'Delivering database changes pose a unique set of challenges that force many to treat database changes as a second class citizen when releasing software. In this course, you''ll learn how to use Flyway to version changes to a MySQL database. You''ll also learn how to leverage versioned changes from your code repository to get early feedback about potential problems, including how to simulate a production database update every time someone checks in a database change.', '2014-12-03'),
(135, 115, 'Relational Database Design', 'This course is for anyone who wants to understand relational database design, or data modeling in general. You will learn how to gather requirements, model them, normalize the model, and transform that model into a fully normalized relational database design.', '2013-12-07'),
(140, 119, 'Software Process Management', 'This course provides an introduction to the study of software engineering by closely examining the software development process. Several popular software development process models are examined, along with topics on the software lifecycle, quality management, and software configuration management.', '2013-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `course_concepts`
--

CREATE TABLE IF NOT EXISTS `course_concepts` (
  `cc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  `concept_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`cc_id`),
  KEY `profile_id` (`profile_id`),
  KEY `course_id` (`course_id`),
  KEY `concept_id` (`concept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=215 ;

--
-- Dumping data for table `course_concepts`
--

INSERT INTO `course_concepts` (`cc_id`, `profile_id`, `course_id`, `concept_id`) VALUES
(24, 71, 19, 76),
(25, 71, 20, 24),
(26, 72, 21, 25),
(27, 72, 22, 22),
(28, 73, 23, 63),
(29, 73, 24, 60),
(30, 74, 26, 2),
(31, 74, 26, 8),
(32, 74, 27, 2),
(33, 74, 27, 9),
(34, 74, 28, 2),
(35, 74, 28, 6),
(36, 75, 29, 37),
(37, 75, 29, 38),
(38, 75, 30, 37),
(39, 75, 30, 39),
(40, 76, 31, 85),
(41, 76, 32, 82),
(42, 77, 33, 44),
(43, 77, 34, 2),
(44, 77, 35, 3),
(45, 78, 36, 44),
(46, 78, 36, 45),
(47, 78, 37, 37),
(48, 78, 38, 37),
(49, 78, 38, 39),
(50, 78, 39, 37),
(51, 78, 39, 41),
(52, 78, 40, 2),
(53, 79, 41, 13),
(54, 79, 42, 13),
(55, 79, 42, 94),
(56, 80, 43, 37),
(57, 80, 44, 44),
(58, 80, 45, 44),
(59, 80, 45, 45),
(60, 80, 46, 44),
(61, 80, 46, 46),
(62, 81, 47, 96),
(63, 81, 48, 37),
(64, 81, 48, 41),
(65, 81, 49, 37),
(66, 81, 49, 43),
(67, 81, 50, 44),
(68, 81, 50, 46),
(69, 81, 51, 37),
(70, 81, 51, 39),
(71, 82, 52, 44),
(72, 82, 52, 56),
(73, 82, 53, 44),
(74, 82, 53, 96),
(75, 82, 54, 44),
(76, 82, 54, 46),
(77, 82, 55, 65),
(78, 82, 55, 66),
(79, 82, 55, 67),
(80, 82, 55, 68),
(81, 82, 55, 69),
(82, 82, 55, 70),
(83, 82, 56, 44),
(84, 82, 56, 56),
(85, 82, 57, 37),
(86, 82, 57, 39),
(87, 82, 58, 44),
(88, 82, 58, 96),
(89, 82, 59, 44),
(90, 82, 59, 46),
(91, 82, 60, 65),
(92, 82, 60, 66),
(93, 82, 60, 67),
(94, 82, 60, 68),
(95, 82, 60, 69),
(96, 82, 60, 70),
(97, 82, 61, 37),
(98, 82, 61, 39),
(99, 83, 63, 10),
(100, 83, 63, 86),
(101, 83, 64, 18),
(102, 84, 65, 10),
(103, 84, 65, 86),
(104, 84, 66, 10),
(105, 84, 66, 80),
(106, 84, 67, 2),
(107, 85, 68, 10),
(108, 85, 68, 86),
(109, 85, 69, 10),
(110, 85, 69, 80),
(111, 85, 70, 2),
(112, 86, 71, 10),
(113, 86, 71, 13),
(114, 86, 72, 10),
(115, 86, 72, 80),
(116, 86, 73, 2),
(117, 87, 74, 10),
(118, 87, 74, 13),
(119, 87, 75, 10),
(120, 87, 75, 78),
(121, 87, 76, 2),
(122, 88, 77, 41),
(123, 88, 78, 42),
(124, 88, 79, 2),
(125, 88, 79, 71),
(126, 88, 80, 38),
(127, 89, 81, 87),
(128, 89, 82, 74),
(129, 89, 83, 72),
(130, 89, 84, 33),
(131, 90, 85, 37),
(132, 90, 86, 39),
(133, 91, 87, 44),
(134, 91, 88, 65),
(135, 92, 89, 44),
(136, 92, 90, 65),
(137, 93, 92, 44),
(138, 93, 93, 37),
(139, 94, 94, 44),
(140, 94, 95, 37),
(141, 95, 96, 44),
(142, 95, 97, 2),
(143, 95, 97, 74),
(144, 96, 99, 55),
(145, 96, 100, 88),
(146, 96, 101, 72),
(147, 97, 102, 97),
(148, 97, 103, 44),
(149, 97, 103, 95),
(150, 97, 104, 44),
(151, 97, 104, 96),
(152, 98, 105, 63),
(153, 98, 106, 94),
(154, 99, 107, 2),
(155, 99, 108, 3),
(156, 100, 110, 2),
(157, 100, 110, 73),
(158, 100, 111, 2),
(159, 100, 111, 73),
(160, 101, 112, 10),
(161, 101, 112, 15),
(162, 101, 113, 10),
(163, 101, 113, 86),
(164, 101, 113, 93),
(165, 102, 114, 10),
(166, 102, 114, 24),
(167, 102, 115, 10),
(168, 102, 115, 24),
(169, 103, 116, 10),
(170, 103, 116, 25),
(171, 103, 117, 10),
(172, 103, 117, 25),
(173, 104, 118, 2),
(174, 104, 118, 73),
(175, 104, 118, 79),
(176, 104, 119, 10),
(177, 104, 119, 22),
(178, 104, 119, 79),
(179, 105, 120, 10),
(180, 105, 120, 12),
(181, 105, 121, 10),
(182, 105, 121, 12),
(183, 105, 121, 78),
(184, 106, 122, 10),
(185, 106, 122, 94),
(186, 106, 123, 10),
(187, 106, 123, 94),
(188, 108, 125, 10),
(189, 108, 125, 91),
(190, 108, 126, 62),
(191, 108, 126, 93),
(192, 109, 127, 81),
(193, 110, 128, 48),
(194, 110, 128, 49),
(195, 110, 128, 50),
(196, 111, 129, 2),
(197, 111, 129, 4),
(198, 111, 129, 44),
(199, 111, 129, 55),
(200, 111, 130, 2),
(201, 111, 130, 4),
(202, 111, 130, 73),
(203, 112, 131, 48),
(204, 112, 131, 49),
(205, 112, 131, 50),
(206, 112, 131, 51),
(207, 112, 131, 52),
(208, 113, 132, 33),
(209, 113, 132, 35),
(210, 115, 134, 2),
(211, 115, 134, 8),
(212, 115, 135, 2),
(213, 119, 140, 37),
(214, 119, 140, 53);

-- --------------------------------------------------------

--
-- Table structure for table `final_result`
--

CREATE TABLE IF NOT EXISTS `final_result` (
  `fr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fr_name_en` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `fr_name_ar` char(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `final_result`
--

INSERT INTO `final_result` (`fr_id`, `fr_name_en`, `fr_name_ar`) VALUES
(1, 'Not acceptable', 'غير مقبول'),
(2, 'accepted for publishing with editing', 'مقبول للنشر مع تعديل'),
(3, 'accepted for publishing without editing', 'مقبول للنشر بدون تعديل');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'judges', 'Judging users group');

-- --------------------------------------------------------

--
-- Table structure for table `judgement_measure`
--

CREATE TABLE IF NOT EXISTS `judgement_measure` (
  `jm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jm_name_ar` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `jm_name_en` char(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`jm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `judgement_measure`
--

INSERT INTO `judgement_measure` (`jm_id`, `jm_name_ar`, `jm_name_en`) VALUES
(1, 'مدى تطابق موضوع الورقة مع اختصاص المحكِّم', 'Range of matching between judge specialization  & paper domain'),
(2, 'محتوى الورقة', 'Paper content'),
(3, 'المنهجية العلمية في استنباط النتائج', 'Scientific approach in result extracting'),
(4, 'تقييم النتائج في الورقة', 'Result evaluating in the paper'),
(5, 'القيمة المضافة إلى قطاع المعرفة', 'The added value to the knowledge'),
(6, 'منهجية العرض', 'presentation approach'),
(7, 'هل نُشر مضمون البحث سابقاً؟', 'Is the research published previously?'),
(8, 'لمستوى اللغوي (الاسلوب والقواعد اللغوية)', 'Linguistic level'),
(9, 'حجم الورقة', 'Paper volume'),
(10, 'المراجع والإشارة إلى الأعمال السابقة', 'References and previous work sign'),
(11, 'الأهمية الإجمالية من وجهة نظر القارئ', 'Total importance by the reader sight'),
(12, 'هل ترشح البحث إلى جائزة أفضل ورقة علمية للعام الحالي', 'Do you candidate this research to have the prize of the best current year paper'),
(13, 'نوع البحث', 'Research type');

-- --------------------------------------------------------

--
-- Table structure for table `judgement_result`
--

CREATE TABLE IF NOT EXISTS `judgement_result` (
  `jr_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jr_paper_id` bigint(20) unsigned NOT NULL,
  `jr_judge_id` int(11) unsigned NOT NULL,
  `jr_final_result_id` int(10) unsigned NOT NULL,
  `jr_final_result_rank` float NOT NULL,
  `jr_is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`jr_id`),
  KEY `jr_final_result_id` (`jr_final_result_id`),
  KEY `jr_judge_id` (`jr_judge_id`),
  KEY `jr_paper_id` (`jr_paper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `measure_result`
--

CREATE TABLE IF NOT EXISTS `measure_result` (
  `mr_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mr_measure_id` int(10) unsigned NOT NULL,
  `mr_rank_level_id` int(10) unsigned NOT NULL,
  `mr_judgement_result_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`mr_id`),
  KEY `mr_measure_id` (`mr_measure_id`),
  KEY `mr_judgement_result_id` (`mr_judgement_result_id`),
  KEY `mr_rank_level_id` (`mr_rank_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=183 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `not_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `not_paper_id` bigint(20) unsigned NOT NULL,
  `not_user_id` int(11) unsigned NOT NULL,
  `not_text` text COLLATE utf8_unicode_ci NOT NULL,
  `not_is_read` tinyint(1) NOT NULL,
  `not_pushing_date` date NOT NULL,
  PRIMARY KEY (`not_id`),
  KEY `not_paper_id` (`not_paper_id`),
  KEY `not_user_id` (`not_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE IF NOT EXISTS `paper` (
  `paper_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `paper_type` enum('PAPER','JOURNAL') COLLATE utf8_unicode_ci NOT NULL,
  `paper_title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `paper_description` text COLLATE utf8_unicode_ci NOT NULL,
  `paper_file` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `paper_creation_date` datetime NOT NULL,
  `paper_added_by` int(11) unsigned NOT NULL,
  `paper_status` int(10) unsigned NOT NULL,
  `paper_published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`paper_id`),
  KEY `IXFK_Paper_status` (`paper_status`),
  KEY `paper_added_by` (`paper_added_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `paper_concepts`
--

CREATE TABLE IF NOT EXISTS `paper_concepts` (
  `pc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pc_paper_id` bigint(20) unsigned NOT NULL,
  `pc_concept_id` int(11) unsigned NOT NULL,
  `pc_weight` float NOT NULL,
  PRIMARY KEY (`pc_id`),
  KEY `pc_paper_id` (`pc_paper_id`,`pc_concept_id`),
  KEY `pc_concept_id` (`pc_concept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95 ;

-- --------------------------------------------------------

--
-- Table structure for table `paper_final_result`
--

CREATE TABLE IF NOT EXISTS `paper_final_result` (
  `pfr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) NOT NULL,
  `concept_id` int(11) NOT NULL,
  `result` float NOT NULL,
  PRIMARY KEY (`pfr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

--
-- Dumping data for table `paper_final_result`
--

INSERT INTO `paper_final_result` (`pfr_id`, `paper_id`, `concept_id`, `result`) VALUES
(49, 17, 10, 0.534586),
(50, 18, 10, 0.534586),
(51, 20, 10, 0.534586),
(52, 17, 11, 1),
(53, 17, 22, 0.625),
(54, 18, 22, 0.625),
(55, 17, 23, 0.5),
(56, 18, 23, 0.5),
(57, 19, 23, 0.5),
(58, 20, 23, 0.5),
(59, 17, 30, 0.5),
(60, 18, 30, 0.5),
(61, 19, 30, 0.5),
(62, 20, 30, 0.5),
(63, 18, 2, 0.534586),
(64, 19, 2, 0.534586),
(65, 20, 2, 0.534586),
(66, 18, 62, 1),
(67, 18, 65, 0.534586),
(68, 19, 65, 0.534586),
(69, 20, 65, 0.534586),
(70, 19, 3, 0.625),
(71, 20, 3, 0.625),
(72, 19, 8, 0.625),
(73, 20, 8, 0.625),
(74, 19, 24, 0.625),
(75, 20, 24, 0.625);

-- --------------------------------------------------------

--
-- Table structure for table `paper_history`
--

CREATE TABLE IF NOT EXISTS `paper_history` (
  `ph_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ph_paper_id` bigint(20) unsigned NOT NULL,
  `ph_paper_type` enum('PAPER','JOURNAL') COLLATE utf8_unicode_ci NOT NULL,
  `ph_paper_title` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `ph_paper_description` text COLLATE utf8_unicode_ci NOT NULL,
  `ph_paper_file` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `ph_editing_date` date NOT NULL,
  `ph_paper_edited_by` int(11) unsigned NOT NULL,
  `ph_paper_status` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ph_id`),
  KEY `ph_paper_id` (`ph_paper_id`),
  KEY `ph_paper_edited_by` (`ph_paper_edited_by`),
  KEY `ph_paper_status` (`ph_paper_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `paper_review`
--

CREATE TABLE IF NOT EXISTS `paper_review` (
  `pr_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pr_paper_id` bigint(20) unsigned NOT NULL,
  `pr_reviewer_id` int(11) unsigned NOT NULL,
  `pr_review_text` text COLLATE utf8_unicode_ci NOT NULL,
  `pr_review_date` date NOT NULL,
  PRIMARY KEY (`pr_id`),
  KEY `pr_paper_id` (`pr_paper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `paper_review`
--

INSERT INTO `paper_review` (`pr_id`, `pr_paper_id`, `pr_reviewer_id`, `pr_review_text`, `pr_review_date`) VALUES
(1, 23, 111, 'Ok', '2015-07-22'),
(2, 23, 74, 'ok.', '2015-07-22'),
(3, 28, 78, 'accepted for publishing without editing.', '2015-07-22'),
(4, 26, 78, 'ok.', '2015-07-22'),
(5, 23, 115, 'accept', '2015-07-23'),
(6, 26, 115, 'ok', '2015-07-23'),
(7, 42, 95, 'ertert', '2015-07-23'),
(8, 42, 84, 'asdasd', '2015-07-23'),
(9, 42, 88, 'wewewer', '2015-07-23'),
(10, 42, 87, 'erewrwer', '2015-07-23'),
(11, 49, 117, 'ok', '2015-07-23'),
(12, 49, 120, 'utyutyu', '2015-07-23'),
(13, 49, 116, '564656', '2015-07-23'),
(14, 49, 71, 'rwerwer', '2015-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `privilage`
--

CREATE TABLE IF NOT EXISTS `privilage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privilage_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `privilage_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_cat` int(10) unsigned DEFAULT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `controller_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `func` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cat` (`id_cat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `privilage`
--

INSERT INTO `privilage` (`id`, `name`, `privilage_en`, `privilage_ar`, `id_cat`, `img`, `controller_name`, `func`) VALUES
(1, 'show_users', 'Show Users', 'إظهار المستخدمين', 4, 'sub_priv.png', 'account', 'index'),
(2, 'create_users', 'Create Users', 'إنشاء مستخدمين', 4, 'sub_priv.png', 'account', 'create_user'),
(3, 'add_paper', 'Add paper', 'إضافة ورقة بحثية', 1, 'sub_priv.png', 'paper', 'add'),
(4, 'paper_judgment_result', 'View paper judgment result', 'عرض نتائج تحكيم المقال', 1, 'sub_priv.png', 'paper', 'paper_judgment_result'),
(5, 'show_paper', 'show paper', 'عرض الأوراق البحثية', 1, 'sub_priv.png', 'paper', 'show'),
(6, 'upload_paper_file', 'Upload paper file', 'رفع ملف جديد للورقة البحثية', 1, 'sub_priv.png', 'paper', 'upload_new_file'),
(7, 'delete_paper', 'Delete paper', 'حذف ورقة بحثية', 1, 'sub_priv.png', 'paper', 'delete'),
(8, 'assign_papers', 'view assigned papers', 'عرض الأوراق البحثية المسندة', 1, 'sub_priv.png', 'paper', 'assigned_papers'),
(9, 'judge_paper', 'Judge paper', 'تحكيم ورقة بحثية', 1, 'sub_priv.png', 'paper', 'judge'),
(10, 'assigning_privilage', 'Assigning Privilage', 'اسناد ميزات', 3, 'sub_priv.png', 'account', 'assig_priv'),
(11, 'assigning_role', 'Assigning Role', 'اسناد أدوار', 3, 'sub_priv.png', 'account', 'assign_role'),
(12, 'add_role', 'Add Role', 'إضافة دور', 3, 'sub_priv.png', 'account', 'add_role'),
(13, 'delete_role', 'Delete Role', 'حذف دور', 3, 'sub_priv.png', 'account', 'delete_role'),
(14, 'edit_profile', 'Edit profile', 'تعديل الملف الشخصي', 5, 'sub_priv.png', 'profile', 'edit_profile'),
(15, 'add_jobs', 'add jobs', 'إضافة أعمال', 5, 'sub_priv.png', 'profile', 'add_jobs'),
(16, 'accept_comment', 'Accept Comments', 'قبول التعليقات', 6, 'sub_priv.png', 'paper', 'accept_comments'),
(17, 'change_password', 'Change Password', 'تغير كلمة السر', 4, 'sub_priv.png', 'account', 'change_password'),
(32, 'view_paper_assigned_judges', 'paper assigned judges', 'عرض المحكمين المسندين إلى ورقة بحثية', 1, 'sub_priv.png', 'paper', 'view_paper_assigned_judges'),
(33, 'edit_paper', 'Edit Paper', 'تعديل وثيقة', 1, 'sub_priv.png', 'paper', 'edit');

-- --------------------------------------------------------

--
-- Table structure for table `priv_cat`
--

CREATE TABLE IF NOT EXISTS `priv_cat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_ar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `priv_cat`
--

INSERT INTO `priv_cat` (`id`, `cat_en`, `cat_ar`, `img`) VALUES
(1, 'Manage papers', 'إدارة الأوراق البحثية', 'manage_paper.png'),
(2, 'Manage concepts', 'إدارة المصطلحات', 'manage_concept.png'),
(3, 'Manage Privilage', 'إدارة الصلاحيات', 'manage_priv.png'),
(4, 'Manage Users', 'إدارة المستخدمين', 'manage_users.png'),
(5, 'Other Tasks', 'مهام متنوعة', 'other_task.png'),
(6, 'Manage Comments', 'إدارة التعليقات', 'manage_comment.png');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `profile_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_time` date NOT NULL,
  `task_num` int(11) NOT NULL,
  `userid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=121 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`profile_id`, `first_name`, `last_name`, `date_time`, `task_num`, `userid`) VALUES
(71, 'Michael', 'Zarozinski', '2015-07-20', 0, 4),
(72, 'Dean', 'Allemang', '2015-07-20', 0, 5),
(73, 'Abhishek ', 'Narolia', '2015-07-20', 0, 6),
(74, 'Ahmad', 'Alshikh Saleh', '2015-07-20', 0, 7),
(75, 'Mohammad', 'Hassan', '2015-07-20', 0, 8),
(76, 'Ola', 'Abo Amsha', '2015-07-20', 0, 9),
(77, 'Riham', 'Nasleh', '2015-07-20', 0, 10),
(78, 'Omar', 'Dahan', '2015-07-20', 0, 11),
(79, 'Fahd', 'Sheraz', '2015-07-20', 0, 12),
(80, 'Ahmad', 'Abo Hasna', '2015-07-20', 0, 13),
(81, 'Aamer', 'Ghiyath Hammamiah ', '2015-07-20', 0, 14),
(82, 'Ibrahim', 'alfaili ', '2015-07-20', 0, 15),
(83, 'Mohammad', 'Tahhan  ', '2015-07-20', 0, 16),
(84, 'Mohammad', 'Bassel Khallouf ', '2015-07-20', 0, 17),
(85, 'Anas', 'salman ', '2015-07-20', 0, 18),
(86, 'Hussan', 'Kazah ', '2015-07-20', 0, 19),
(87, 'Imad', 'Totanji ', '2015-07-20', 0, 20),
(88, 'Mahmoud ', 'Aljaroud', '2015-07-20', 0, 21),
(89, 'Mohammad', 'Ali', '2015-07-20', 0, 22),
(90, 'Ahmad', 'Alshantout', '2015-07-20', 0, 23),
(91, 'Maher', 'Safadi', '2015-07-20', 0, 24),
(92, 'Saeed', 'EL-Dah', '2015-07-20', 0, 25),
(93, 'Ghiath ', 'Zarzar', '2015-07-20', 0, 26),
(94, 'Alaa', 'Wahbah', '2015-07-20', 0, 27),
(95, 'Hussain', 'Rafee', '2015-07-20', 0, 28),
(96, 'Khalel', 'Tenawi', '2015-07-20', 0, 29),
(97, 'Alaa', 'Wahab', '2015-07-20', 0, 30),
(98, 'Tawfeek', 'Kaekaty', '2015-07-20', 0, 31),
(99, 'MHD', 'Yasser Koziz', '2015-07-20', 0, 32),
(100, 'Birgitta', 'Hauser', '2015-07-20', 0, 33),
(101, 'Achilleas', 'Portarinos', '2015-07-20', 0, 34),
(102, 'Mary', 'Lopez', '2015-07-20', 0, 35),
(103, 'Jeff', 'Allen', '2015-07-20', 0, 36),
(104, 'Paul', 'Garcia', '2015-07-20', 0, 37),
(105, 'Patricia', 'Phillips', '2015-07-20', 0, 38),
(106, 'Lisa', 'Collins', '2015-07-20', 0, 39),
(107, 'Kevin', 'Adams', '2015-07-20', 0, 40),
(108, 'James', 'Martin', '2015-07-20', 0, 41),
(109, 'Ronald', 'Clark', '2015-07-20', 0, 42),
(110, 'Barbra', 'Parker', '2015-07-20', 0, 43),
(111, 'Linda', 'Hill', '2015-07-20', 0, 44),
(112, 'Karen', 'Garcia', '2015-07-20', 0, 45),
(113, 'Ronald', 'King', '2015-07-20', 0, 46),
(114, 'Diana', 'Whilte', '2015-07-20', 0, 47),
(115, 'Donald', 'Brown', '2015-07-20', 0, 48),
(116, 'Ronald', 'Ron', '2015-07-20', 0, 49),
(117, 'Michael', 'Johnson', '2015-07-20', 0, 50),
(118, 'Linda', 'Wright', '2015-07-20', 0, 51),
(119, 'Nancy', 'Roberts', '2015-07-20', 0, 52),
(120, 'Jason', 'Martin', '2015-07-20', 0, 53);

-- --------------------------------------------------------

--
-- Table structure for table `profile_final_result`
--

CREATE TABLE IF NOT EXISTS `profile_final_result` (
  `pfr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `concept_id` int(11) unsigned NOT NULL,
  `result` float NOT NULL,
  PRIMARY KEY (`pfr_id`),
  KEY `concept_id` (`concept_id`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=355 ;

--
-- Dumping data for table `profile_final_result`
--

INSERT INTO `profile_final_result` (`pfr_id`, `profile_id`, `concept_id`, `result`) VALUES
(60, 74, 2, 0.3),
(61, 77, 2, 0.0222222),
(62, 78, 2, 0.0222222),
(63, 84, 2, 0.0222222),
(64, 85, 2, 0.0222222),
(65, 86, 2, 0.0222222),
(66, 87, 2, 0.0222222),
(67, 88, 2, 0.0222222),
(68, 89, 2, 0.2),
(69, 90, 2, 0.075),
(70, 95, 2, 0.0222222),
(71, 99, 2, 0.222222),
(72, 100, 2, 0.463889),
(73, 104, 2, 0.0222222),
(74, 111, 2, 0.588889),
(75, 115, 2, 0.255556),
(76, 77, 3, 0.1),
(77, 99, 3, 0.1),
(78, 111, 3, 0.2),
(79, 111, 4, 0.633333),
(80, 115, 5, 0.533333),
(81, 74, 6, 0.0913515),
(82, 99, 6, 0.304505),
(83, 118, 6, 0.487208),
(84, 90, 7, 0.2),
(85, 74, 8, 0.0926268),
(86, 115, 8, 0.0926268),
(87, 74, 9, 0.1),
(88, 105, 12, 0.633333),
(89, 79, 14, 0.0444444),
(90, 92, 14, 0.2),
(91, 114, 14, 0.0444444),
(92, 77, 15, 0.075),
(93, 79, 15, 0.2),
(94, 92, 15, 0.075),
(95, 101, 15, 0.508333),
(96, 104, 15, 0.075),
(97, 114, 15, 0.2),
(98, 77, 16, 0.075),
(99, 79, 16, 0.2),
(100, 85, 16, 0.075),
(101, 92, 16, 0.075),
(102, 104, 16, 0.075),
(103, 114, 16, 0.2),
(104, 85, 17, 0.075),
(105, 87, 17, 0.075),
(106, 92, 17, 0.2),
(107, 111, 17, 0.333333),
(108, 77, 18, 0.0719414),
(109, 79, 18, 0.0719414),
(110, 83, 18, 0.0938829),
(111, 85, 18, 0.0719414),
(112, 104, 18, 0.0719414),
(113, 114, 18, 0.187766),
(114, 92, 19, 0.2),
(115, 101, 19, 0.333333),
(116, 104, 19, 0.333333),
(117, 72, 20, 0.2),
(118, 92, 20, 0.0444444),
(119, 103, 20, 0.333333),
(120, 104, 20, 0.125),
(121, 109, 20, 0.125),
(122, 118, 20, 0.125),
(123, 118, 21, 0.2),
(124, 72, 22, 0.633333),
(125, 73, 22, 0.333333),
(126, 92, 22, 0.075),
(127, 103, 22, 0.333333),
(128, 104, 22, 0.433333),
(129, 109, 22, 0.333333),
(130, 106, 23, 0.533333),
(131, 71, 24, 0.0353379),
(132, 102, 24, 0.57856),
(133, 72, 25, 0.1125),
(134, 92, 25, 0.2),
(135, 103, 25, 0.433333),
(136, 77, 13, 0.075),
(137, 79, 13, 0.3),
(138, 84, 13, 0.075),
(139, 86, 13, 0.0375),
(140, 87, 13, 0.0375),
(141, 89, 13, 0.075),
(142, 97, 13, 0.075),
(143, 98, 13, 0.2),
(144, 72, 10, 0.2),
(145, 77, 10, 0.075),
(146, 83, 10, 0.0375),
(147, 84, 10, 0.1),
(148, 85, 10, 0.1),
(149, 86, 10, 0.1),
(150, 87, 10, 0.1),
(151, 101, 10, 0.508333),
(152, 102, 10, 0.508333),
(153, 103, 10, 0.433333),
(154, 104, 10, 0.445833),
(155, 105, 10, 0.508333),
(156, 106, 10, 0.508333),
(157, 108, 10, 0.3625),
(158, 109, 10, 0.333333),
(159, 114, 10, 0.075),
(160, 114, 26, 0.442027),
(161, 116, 26, 0.276267),
(162, 119, 26, 0.442027),
(163, 116, 27, 0.333333),
(164, 73, 28, 0.333333),
(165, 116, 29, 0.333333),
(166, 117, 30, 0.333333),
(167, 114, 31, 0.533333),
(168, 89, 33, 0.3),
(169, 97, 33, 0.2),
(170, 98, 33, 0.2),
(171, 113, 33, 0.3),
(172, 113, 35, 0.601293),
(173, 113, 35, 0.633333),
(174, 120, 36, 0.533333),
(175, 74, 37, 0.2),
(176, 75, 37, 0.588889),
(177, 78, 37, 0.1),
(178, 80, 37, 0.222222),
(179, 81, 37, 0.1),
(180, 82, 37, 0.0555556),
(181, 89, 37, 0.2),
(182, 90, 37, 0.555556),
(183, 93, 37, 0.222222),
(184, 94, 37, 0.222222),
(185, 119, 37, 0.555556),
(186, 75, 38, 0.1),
(187, 88, 38, 0.1),
(188, 89, 38, 0.2),
(189, 75, 38, 0.1),
(190, 88, 38, 0.1),
(191, 89, 38, 0.2),
(192, 75, 39, 0.2375),
(193, 78, 39, 0.0375),
(194, 81, 39, 0.0375),
(195, 82, 39, 0.1),
(196, 90, 39, 0.2375),
(197, 112, 40, 0.333333),
(198, 75, 41, 0.2),
(199, 78, 41, 0.1),
(200, 80, 41, 0.2),
(201, 81, 41, 0.1),
(202, 88, 41, 0.1),
(203, 88, 42, 0.1),
(204, 81, 43, 0.1),
(205, 71, 44, 0.075),
(206, 73, 44, 0.2),
(207, 77, 44, 0.00972222),
(208, 78, 44, 0.00972222),
(209, 80, 44, 0.1125),
(210, 81, 44, 0.00972222),
(211, 82, 44, 0.1),
(212, 91, 44, 0.00972222),
(213, 92, 44, 0.00972222),
(214, 93, 44, 0.00972222),
(215, 94, 44, 0.00972222),
(216, 95, 44, 0.00972222),
(217, 97, 44, 0.0222222),
(218, 108, 44, 0.333333),
(219, 111, 44, 0.00972222),
(220, 78, 45, 0.1),
(221, 80, 45, 0.1),
(222, 110, 45, 0.333333),
(223, 112, 45, 0.333333),
(224, 80, 46, 0.0375),
(225, 81, 46, 0.0375),
(226, 82, 46, 0.1),
(227, 110, 46, 0.333333),
(228, 114, 47, 0.2),
(229, 110, 48, 0.433333),
(230, 112, 48, 0.1),
(231, 110, 49, 0.433333),
(232, 112, 49, 0.1),
(233, 110, 50, 0.433333),
(234, 112, 50, 0.1),
(235, 112, 51, 0.1),
(236, 112, 52, 0.1),
(237, 119, 53, 0.1),
(238, 96, 55, 0.1),
(239, 111, 55, 0.1),
(240, 82, 56, 0.1),
(241, 112, 56, 0.333333),
(242, 112, 57, 0.333333),
(243, 112, 58, 0.333333),
(244, 112, 59, 0.333333),
(245, 73, 60, 0.1),
(246, 73, 61, 0.2),
(247, 73, 62, 0.2),
(248, 101, 62, 0.2),
(249, 108, 62, 0.433333),
(250, 111, 62, 0.2),
(251, 73, 63, 0.1),
(252, 98, 63, 0.1),
(253, 119, 63, 0.333333),
(254, 119, 64, 0.2),
(255, 82, 65, 0.3),
(256, 91, 65, 0.1125),
(257, 92, 65, 0.1125),
(258, 82, 66, 0.3),
(259, 91, 66, 0.075),
(260, 92, 66, 0.075),
(261, 82, 67, 0.3),
(262, 91, 67, 0.075),
(263, 92, 67, 0.075),
(264, 82, 68, 0.3),
(265, 91, 68, 0.075),
(266, 92, 68, 0.075),
(267, 82, 69, 0.3),
(268, 91, 69, 0.075),
(269, 92, 69, 0.075),
(270, 82, 69, 0.3),
(273, 82, 70, 0.3),
(274, 91, 70, 0.075),
(275, 92, 70, 0.075),
(276, 88, 71, 0.1),
(277, 89, 72, 0.3),
(278, 92, 72, 0.2),
(279, 96, 72, 0.1),
(280, 111, 72, 0.2),
(281, 115, 72, 0.333333),
(282, 100, 73, 0.433333),
(283, 104, 73, 0.0375),
(284, 111, 73, 0.3625),
(285, 89, 74, 0.3),
(286, 95, 74, 0.1),
(287, 99, 74, 0.2),
(288, 115, 74, 0.2),
(289, 74, 75, 0.2),
(290, 71, 76, 0.1),
(291, 87, 78, 0.1),
(292, 105, 78, 0.433333),
(293, 71, 79, 0.075),
(294, 95, 79, 0.075),
(295, 104, 79, 0.3),
(296, 116, 79, 0.125),
(297, 117, 79, 0.333333),
(298, 118, 79, 0.075),
(299, 120, 79, 0.2),
(300, 84, 80, 0.1),
(301, 85, 80, 0.1),
(302, 86, 80, 0.1),
(303, 109, 81, 0.433333),
(304, 76, 82, 0.433333),
(305, 77, 82, 0.2),
(306, 93, 82, 0.2),
(307, 94, 82, 0.2),
(308, 77, 83, 0.2),
(309, 92, 83, 0.2),
(310, 93, 83, 0.2),
(311, 94, 83, 0.2),
(312, 107, 83, 0.533333),
(313, 107, 84, 0.533333),
(314, 76, 85, 0.1),
(315, 77, 85, 0.075),
(316, 103, 85, 0.125),
(317, 107, 85, 0.533333),
(318, 79, 86, 0.2),
(319, 83, 86, 0.1),
(320, 84, 86, 0.1),
(321, 85, 86, 0.175),
(322, 92, 86, 0.075),
(323, 101, 86, 0.175),
(324, 104, 86, 0.075),
(325, 87, 87, 0.075),
(326, 89, 87, 0.3),
(327, 92, 88, 0.2),
(328, 96, 88, 0.1),
(329, 97, 88, 0.0444444),
(330, 98, 88, 0.111111),
(331, 114, 88, 0.111111),
(332, 83, 89, 0.333333),
(333, 114, 89, 0.2),
(334, 83, 90, 0.0444444),
(335, 85, 90, 0.0444444),
(336, 92, 90, 0.2),
(337, 108, 91, 0.3),
(338, 108, 92, 0.2),
(339, 79, 93, 0.2),
(340, 85, 93, 0.075),
(341, 87, 93, 0.075),
(342, 98, 93, 0.075),
(343, 101, 93, 0.1),
(344, 108, 93, 0.1),
(345, 79, 94, 0.0375),
(346, 98, 94, 0.0375),
(347, 105, 94, 0.333333),
(348, 106, 94, 0.1),
(349, 108, 94, 0.533333),
(350, 97, 95, 0.1),
(351, 81, 96, 0.0375),
(352, 82, 96, 0.1),
(353, 97, 96, 0.0375),
(354, 97, 97, 0.1);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `date_time` date NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=100 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`project_id`, `profile_id`, `name`, `details`, `date_time`) VALUES
(17, 71, 'Senior Software Engineer - Center for Intelligent InformationRetreval', 'Responsibilities include: the design, implementation and maintenance of advanced information retrieval systems, text extraction systems, machine learning systems and computer vision systems, including a leadership role in system design decisions, and managing the software aspects of projects.', '2013-01-05'),
(18, 71, 'Principal Software Engineer / Architect', '• Principal developer of accessible, online games enabling blind, visually impaired and fully sighted users worldwide to play together as equals\n• Architected and built a single code base (Windows and Linux), reducing development time from 2+ years to less than 6 months\n• Developed Windows based games in C++ with LAMP/C++ back end to support all major versions of Windows, Internet browsers and screen readers\n• Designed and implemented artificial intelligence for Draw Poker and Texas Hold’em games\n• Designed and implemented MySQL data warehouse, ETL process and reporting tools, allowing for first time historical analysis of customer trends and playing patterns\n• Maintained responsibility for entire life cycle of games, including alpha and beta testing, game balancing and prioritizing feature requests\n• Enhanced online store and customer purchasing experience for subscribers, including integration with PayPal\n• Represented company at American Council of the Blind conference\n', '2010-02-01'),
(19, 72, 'Ontologist', 'Assist the EDM Council in the development of the The Financial Industry Business Ontology (http://www.edmcouncil.org/financialbusiness).', '2014-05-04'),
(20, 72, 'CEO and Principal', 'I started Working Ontologist as a platform to provide consulting services around Semantic Web technology. The services we offer include: \n\n- Architecture Development for Semantic Technology solutions, \n- Technology selection (I have experience using many of the major triple stores and semantic platforms on enterprise projects), \n- Organizational consulting (Solutions Envisioning and Organization Domain Modeling)\n- Public speaking and training (I have trained over 1000 corporate professionals in Semantic Web technologies. Customized versions of the hands-on training available for many semantic platforms)\n\nAs co-author of one of the best-selling books on the Semantic Web, "Semantic Web for the Working Ontologist", I am a recognized expert and thought-leader on Semantic Web technologies. \n\nWorking Ontologist has experience in many application domains, with current focus on Life Sciences, Finance, and Supply chain sustainability.\n', '2010-02-01'),
(21, 72, 'Semantic Web for the Working Ontologist', 'Best-selling introduction to the Semantic Web.', '2010-01-05'),
(22, 73, 'Meta model', 'Metamodel or surrogate model is a model of a model, and metamodeling is the process of generating such metamodels.Metamodeling or meta-modeling is the analysis, construction and development of the frames, rules, constraints, models and theories applicable and useful for modeling a predefined class of problems. As its name implies, this concept applies the notions of meta- and modeling in software engineering and systems engineering. Metamodels are of many types and have diverse applications. A thorough discussion is presented in the following text. [2]', '2014-05-05'),
(23, 73, 'Design pattern', 'In software engineering, a design pattern is a general reusable solution to a commonly occurring problem within a given context in software design. A design pattern is not a finished design that can be transformed directly into source or machine code. It is a description or template for how to solve a problem that can be used in many different situations. Patterns are formalized best practices that the programmer can use to solve common problems when designing an application or system. Object-oriented design patterns typically show relationships and interactions between classes or objects, without specifying the final application classes or objects that are involved. Patterns that imply mutable state may be unsuited for functional programming languages, some patterns can be rendered unnecessary in languages that have built-in support for solving the problem they are trying to solve, and object-oriented patterns are not necessarily suitable for non-object-oriented languages.', '2014-02-01'),
(24, 74, 'Data Management officer', 'The Data Management Officer is responsible for helping to ensure that the process of data & information management related to the program teams is properly maintained and updated, producing solid and integrated information as well as producing reports covering team activities and programs for Program Manager and Management.', '2015-01-02'),
(25, 74, 'Distribution Assistant', 'The Distribution Assistants have the responsibility for the new comer’s distribution site management in Killis Turkey.', '2014-02-01'),
(26, 75, 'Centre for Information Systems Engineering', 'Cranfield''s Centre for Information Systems Engineering provides extensive capabilities and a focus for defence and security related information systems activities. The Centre for Information Systems Engineering is led by Steve Smith and our activities are both education and research based, serving a diverse student, client and partner base.', '2015-01-03'),
(28, 77, 'Re-Designing SVU Website', 'Re-Designing SVU Website', '2013-01-04'),
(29, 77, 'Re-Designing Student Moodle (LMS) Interface', 'Re-Designing Student Moodle (LMS) Interface', '2013-02-01'),
(31, 79, 'Dreadybrand.com', 'Dreadybrand.com', '2012-01-05'),
(32, 79, 'Babybabyonline.co.uk', 'We are delighted to welcome you to www.babybabyonline.co.uk.\nWe are a family run business that has your needs and interests at heart. As parents ourselves we understand the concerns you have when trying to make the correct purchase for your little one. This is why we offer products from brands that we believe in and trust. As specialists in infant safety, our staff are fully trained by our suppliers to ensure you receive the correct guidance when making your purchase.\nWe have now been trading just over 4 years and both our Store and Website has gone from strength to strength even in these tough economic times. Our Portfolio of Brands has increased from a few to over 20 brands, all of which have thier own unique USP''s and place in the Nursery market.\nWe hope you find what you are looking for and thank you for visiting www.babybabyonline.co.uk\n', '2011-01-06'),
(33, 80, 'HR Department Re-engineering- Central Bank of Syria', 'Using BPR "Business Process Re-engineering" to describe the execution ordering of activities and the interaction with partners.The main purpose of the project was promoting business effectiveness and efficiency and integration with technology....View', '2012-01-05'),
(34, 81, '', '', '0000-00-00'),
(35, 82, 'Compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-09'),
(36, 82, 'Compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-09'),
(37, 82, 'Compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-09'),
(38, 83, 'Crossword Game Application', 'Web Application using AJAX, this project got me immersed in responsive web applications, it has a pretty much good user experience with a pretty nice design.', '2012-01-02'),
(39, 84, 'Dfw web', 'I''ve created many projects for dfw web company based in USA ,Texas including :\nWebsite, Web-based applications.\n', '2013-01-05'),
(40, 85, 'Web TV Automation Solution ', 'Software for: \n• Content playing and streaming;\n• Interactive on-air graphics and Multi-layered simultaneous crawls, rolls, animated logos, clocks and text templates;\n• TV content management;\n• Schedule creation and play list editor.\n\nThis system is consisted of several subsystems. All subsystems are integrated and sharing data using central XML web services that are responsible for all data integration and transportation between subsystems and all system layers.\n', '2008-01-05'),
(41, 85, 'eQuest', 'eQuest (electronic questionnaire) is an integrated survey software suite that provides all the design, data collection, data validation, data storage and reporting facilities that you need when undertaking surveys or asking questionnaires.', '2008-01-05'),
(42, 86, '', '', '0000-00-00'),
(43, 87, 'Utility Management System (UMS)', 'Utility Management System (UMS) is a web based application which automates all workflows and data management in Electrical Power Distribution Companies and Water Supply Companies:\n- Billing and management of payments, subscription requests, Electric networks and meters management;\n- Reporting (auditing report, data analysis and statistics).\n\nThis System is developed under MVC Standards:\n- Presentation layer is developed by JSF;\n- Data access Layer developed by JPA using Hibernate library;\n- Controller layer developed by EJB and FacesServlet\n', '2010-01-04'),
(44, 87, 'MTZ Business Solution', 'Software kit for access management, time recording, building security and custom report generator with templates for MTZ Business Solution.\nThis system is consisted of several subsystems. Each one is an applet developed by java swing. All data are transported and managed by JDBC and XML standards. \n', '2008-02-10'),
(45, 88, '', '', '0000-00-00'),
(46, 89, 'SharePoint Intranet Portal', 'The new Syriatel intranet portal built with SharePoint 2013, featuring an extended document management, workflows, social network and more. Which cause a cultural change in document management, connect with employees across the enterprise, share ideas and reinvent the way of working together', '2014-01-02'),
(47, 89, 'My Syriatel Website', 'It is designed Syriatel Services Centre-mail in a way that allows customers the possibility Bouktoothm control is fast and easy and safe without having to visit a service center.\n\nAnd benefits all customers of subscribers lines subsequent payment cards and pre-payment of Jilin second and third lines and companies\n', '2013-01-08'),
(48, 89, 'Bills@net', 'Billing and Collection web-based application for Governmental Electrical Power Distribution Companies and Water Supply Companies in Syria.\nLanguage: Java \nDatabases: Oracle Database (9i, 10g, 11g)\nSybase Adaptive Server Anywhere 9s\nReporting: Oracle Reports 6i, Oracle Reports 10g, Excel Writer (Internal Module)\nOthers: Borland Delphi 6 (used to create Printing OCX)\n', '2012-04-10'),
(49, 89, 'ERMS (internal name)', 'Electronic Record Management System; Archiving solution.\nLanguage: Java (JSE 6, JEE 5)\nApplication Server: JBoss 5.1\nDatabase: MySQL\nFramework: Phoenix\nClient Scripting built using: Prototype JavaScript framework (1.7)\n', '2011-04-08'),
(50, 90, 'KFMC HIS Maintenance Project', 'Supporting the HIS system for KFMC, resolving bugs, applying users modifications and new requests. Generating reports & queries, designing and developing new modules to support new users requests and enhance HIS usability.', '2011-01-02'),
(51, 90, 'IVR System for Eastern Province Principality', 'To query about the status of your request as well as hearing the general requirements of all applications types to Eastern Province Principality. Please Call +96638330000 ext. 2020 to test the IVR Service', '2010-01-05'),
(52, 91, 'Compiler', 'A compiler is a computer program (or set of programs) that transforms source code written in a programming language (the source language) into another computer language (the target language, often having a binary form known as object code).[1] The most common reason for converting a source code is to create an executable program.', '2014-01-04'),
(53, 92, 'Linked Multimedia Data Approach for Building “Social” Information Systems', 'Graduate Project:\nWeb Applications have taken a great role in creating scientific, artistic, cultural, and even business content. But this content, specifically Multimedia, is restricted in the application of its own, which is a real obstacle for reusability. With the evolving standards of semantic web in describing resources, Specially RDF (Resource Description Framework), reusability has become possible. LOD (Linked Open Data) is a great example of publishing data depending on the standards of semantic web. In spite of this promising solution in sharing resources, multimedia is still difficult to reuse. Though multimedia is widely spreading in social networks with textual content, semantic web is a rich environment of the latter and not the former.\nWe aimed to build a semantic social network model, in order to make the content of videos and images annotated, motivated by the lack of annotated multimedia in terms of Linked Data principles, and the deficiency of semantic content in social environments. This model is the cornerstone for annotating videos and images, and publishing these multimedia resources as the first fragmented video dataset in LOD.\nthis project implemented with asp.net, C#, ajax, jQuery, java script, sql server, sparql, rdfs, xml, DBpedia,linkedIMDB, GeoPedia and google AP\n', '2013-01-06'),
(54, 92, 'Semantic eTendering System', 'Commercial contract and government tenders are often considered the most important pillars that stimulate the businessman to contribute and increase the gross national product, and the benefits are gained from these contracts is approximately 13.5% of the gross domestic product in European Union public sector projects.\nIn this century, when the information technology is considered the main pillar which lead the wheel evolution, there is a new concept appear, it called “eTendering”.\neTendering become as a solution to many problems in business world, such as lack of transparency, wasted time, spatial constraint, but until this day, the eTendering systems still unable to solve the problem of offers evaluation, which take a huge effort and characterized by a lack of precision.\nThe purpose of this review is an attempt to modeling an eTendering system that can solve the evaluation problem, used logic information retrieval model approach.\nthis project implemented with asp.net, C#, ajax, jQuery, java script, sql server, sparql, rdfs and xm\n', '2012-01-06'),
(55, 92, 'Video Search Engine', 'a semantic video fragment search engine that uses logic information retrieval model, with video annotation and query builder tools.\nthis project implemented with asp.net, C#, ajax, jQuery, java script, sql server, sparql, rdfs, xml, DBpedia and linkedIMDB\n', '2013-01-02'),
(56, 92, 'Mini F#.net compiler', 'Compiler,Semantic chicker,code generation, type checking , lexical analyzer,syntactical analyzer\ncode-tree visualization and building tool.\nthis compiler implemented with c++/c, assembly, .net interface and yacc tool.\n', '2011-01-10'),
(57, 92, 'Carmen Chamma', 'carmenchammas.com is a website for Astrologist Carmen Chammas, this website provides a range of services such as Astrology Consultants and Books.\nThe website is based on CMS consisting of modules such as: \n1- Payment Module\n2- Shopping Cart\n3- Statistic Module\nthis website implemented in php programming language with html, java script, jQuery, ajax and css\n', '2011-04-10'),
(58, 93, 'Bavora Content Management System Reinvention', 'I was responsible for analysis and development of Bavora which is a web based application that allows publishing, editing and modifying content, organizing, deleting as well as maintenance from a central interface.', '2011-01-06'),
(59, 93, 'School Management Information System', 'Holding responsibility for analyzing and developing systems related to the SIMS Project (School Information Management System) a project sponsored and funded by the European Union, including all related system re-engineering steps to improve the educational program in Lebanon by automating all the internal processes of the ministry of high education and all public and private schools “approximately 1500 schools”.\nThe system consists of a set of subsystems (school profile, HR, financial, student record, examinations, registration and enrollment).\n', '2014-01-04'),
(60, 94, 'Bavora Content Management System Reinvention', 'I was responsible for analysis and development of Bavora which is a web based application that allows publishing, editing and modifying content, organizing, deleting as well as maintenance from a central interface', '2011-01-06'),
(61, 94, 'School Management Information System', 'Holding responsibility for analyzing and developing systems related to the SIMS Project (School Information Management System) a project sponsored and funded by the European Union, including all related system re-engineering steps to improve the educational program in Lebanon by automating all the internal processes of the ministry of high education and all public and private schools “approximately 1500 schools”.\nThe system consists of a set of subsystems (school profile, HR, financial, student record, examinations, registration and enrollment).\n', '2014-01-04'),
(62, 95, 'Information Retrieval', '', '0000-00-00'),
(63, 96, '', '', '0000-00-00'),
(64, 97, 'Najran Electronic Services - Stores Licenses System', 'A government project for Najran municipality (in KSA). The purpose of this project is to manage and automate stores licensing processes. The main licensing processes includes issue license, renew, change ownership, modify activity, change panels, change spaces and cancel the license.\nThe project will produce a web-based application and will be developed using .NET Framework, Enterprise Library 5.0, ASP.NET and SQLServer \n', '2014-01-08'),
(65, 97, 'Blueberry jeans ware', 'Site for show blueberry industry product, and the unanimous user can add order that followed by sales department, to deliver the product to customer', '2015-01-03'),
(66, 98, 'Najran Electronic Services - Stores Licenses System', 'A government project for Najran municipality (in KSA). The purpose of this project is to manage and automate stores licensing processes. The main licensing processes includes issue license, renew, change ownership, modify activity, change panels, change spaces and cancel the license.\nThe project will produce a web-based application and will be developed using .NET Framework, Enterprise Library 5.0, ASP.NET and SQLServer \n', '2014-01-04'),
(67, 98, 'Starways Site', 'Starways Site', '2014-02-02'),
(68, 98, 'SyriaTel Attendance Justification', 'For Justify Attendance Gaps (Morning Late - Early Departure - Absence) And Overtime and follow this requests in every step.', '2012-01-04'),
(69, 98, 'Blueberry jeans ware', 'Site for show blueberry industry product, and the unanimous user can add order that followed by sales department, to deliver the product to customer', '2015-01-03'),
(70, 99, 'Implementing Oracle HRMS', 'Implementing Oracle HRMS', '2004-01-09'),
(71, 100, 'Relational Database Design', 'This course is for anyone who wants to understand relational database design, or data modeling in general. You will learn how to gather requirements, model them, normalize the model, and transform that model into a fully normalized relational database design.', '2009-02-04'),
(72, 101, 'DesignPatternsPHP', 'This is a collection of known design patterns and some sample code how to implement them in PHP. Every pattern has a small list of examples (most of them from Zend Framework, Symfony2 or Doctrine2 as I''m most familiar with this software).\n\nI think the problem with patterns is that often people do know them but don''t know when to apply which.', '2015-02-19'),
(73, 102, 'Social Networks Big Data Consuming', 'Consumer product companies and retail organizations are monitoring social media like Facebook and Twitter to get an unprecedented view into customer behavior, preferences, and product perception.', '2014-03-11'),
(74, 103, '', '', '0000-00-00'),
(75, 104, 'Twitter Summarise', 'summarize, each hour, the most widely discussed topics on Twitter. The summary should be short (e.g., tweet-length) and provide an adequate summary of the topic.', '2008-12-03'),
(76, 105, ' StratusLab -Enhancing Grid Infrastructures with virtualization and cloud technologies', 'StratusLab is focused on applying cloud technologies to grid infrastructures such as the European Grid Infrastructure (EGI) to enhance their use.  The project is developing a complete, open-source cloud distribution that allows grid and non-grid resource centres to offer and to exploit an “Infrastructure as a Service” cloud.', '2014-01-04'),
(77, 106, 'Pi-Web-Agent', 'The pi-web-agent is a web application that aims to provide a more user friendly way of interacting with the Raspberry Pi and performing basic tasks by eliminating the need of using the command line directly.', '2013-02-03'),
(78, 107, 'The ASSAP Project - Adding Subject Specificity to Accredited Programmes or "The Pool"', 'The Pool'', a Higher Education Academy/JISC funded Open Educational Resources project. ''The Pool'' is a themed collection of resources for use by English or Creative Writing lecturers in Higher Education, especially those following accredited courses for lecturers --  or for use by more experienced colleagues seeking to enhance or refresh their teaching practice. The collection comprises a variety of types of materials such as video footage, activities and texts. Used together or individually, these can be used to add a subject-dimension to accredited courses or be incorporated in learning and teaching events within departments.', '2014-04-07'),
(79, 107, 'Creative Writing: using technology for on-screen and online workshopping', 'This project forms one of a number of discipline-based projects forming part of the ''Discipline-focused Learning Technology Enhancement Academy 2010''. The underpinning model for the programme has been informed by successful outcomes from the Academy/JISC Benchmarking and Pathfinder programmes and the Academy''s acclaimed Change Academy to enable project teams to identify how they can maximise the benefits of the use of technology to enhance learning, teaching and assessment in their subject departments.', '2009-12-03'),
(80, 108, 'Create emails as dynamic as your customers', 'Life isn''t static - your emails shouldn''t be either. Use our elements to create dynamic, geotargeted content that can be updated any time. Harness the prower of our API to hook elements straight into your system and give your customers the latest information. Track everything using our powerful analytics to view impressions and number of clicks.', '2012-12-06'),
(81, 108, 'Meetup', 'Example: \nThis meetup is something special! \nAgenda\n18:30 drinks & pizza\n19:15 talks\nTalks\n1 . Erik Bryn, Ember core team member, will be talking about the plans for Ember 2.0 and how we can start preparing our apps for Ember 2.0 today.\n2. Alex Matchneer: TBA\n3. Joost de Vries: Fiddling with Ember CLI, Ember Twiddle\nA big thanks to our sponsors CloseAlert and Genkgo for helping put this meetup together on short notice! ', '2013-12-06'),
(84, 111, 'ORDB Assicurazioni ', 'questa è un''applicazione web che sfrutta una base di dati ORDB.', '2011-02-03'),
(85, 111, 'ORDB - Schedule of Rates', 'ORDB - Schedule of Rates allows you at access building pricing information quickly and accurately at the click of a mouse. It is an excellent online alternative to the BCIS Price Books. The service provides a range of datasets to suit your individual needs including civils, small and large projects, alterations and refurbishment, maintenance and dilapidations.', '2013-12-04'),
(87, 113, 'United Nations Online Network in Public Administration and Finance (UNPAN)', 'UNPAN has the mission to promote the sharing of knowledge, experiences and best practices throughout the world in sound public policies, effective public administration and efficient civil services through capacity-building and cooperation among Member States, with emphasis on south-south cooperation and UNPAN''s commitment to integrity and excellence. This site, managed by all members of the network, provides various types of worldwide activities and products in the focus areas of: public administration; public policy; e-government; governance systems and institutions; civil service reform; integrity, transparency and accountability; and management innovation and development. Technical assistance in building up the ICT infrastructures of the UNPAN member institutions has also been provided, and special attention has been given to e-information management training.', '2014-12-08'),
(88, 113, 'The Writing University', 'Electronic journals, like electronic texts in general, sometimes seem to be flying outward in a thousand directions at once. While they are relatively easy to create, by the same token, they can quietly disappear, leaving little or no trace of themselves behind. The Electronic Journals Project is something of a virtual mooring, a place for editors to secure their publications amidst turbulent sea changes, a place for visitors to tie up and have a look around.', '2013-12-09'),
(89, 114, 'specific Formal System Model', 'The purpose of this paper is to introduce a project‐specific version of the Formal Systems Model that can be used by project managers and other professionals to identify actual or potential weaknesses in a project''s structure or processes and to look for difficulties in the relationships between the project and the context in which it is or will be taking place.', '2011-04-06'),
(90, 114, 'Font Awesome', 'Font Awesome is indeed awesome: 249 icons contained within a single font, constituting "a pictographic language of web-related actions". Icons are scalable, so they look the same at any size, and you can style them with CSS. It''s fully compatible with Twitter Bootstrap 2.2.2 (see below), works with IE7 and is available for commercial use.', '2013-12-06'),
(91, 114, 'Brackets', 'An open source project begun at Adobe, Brackets is a lightweight and modern code editor focused on web technologies, with a collection of innovative features.\nThe 17th most popular project on Github, the core team consists of 12 developers, with Adobe providing seven full-time engineers to the project, plus around 80 individual contributors. However, product manager Adam Lehman cautions that it isn’t yet ready for day-to-day use.', '2013-02-06'),
(92, 115, ' RAILWAY SYSTEM DATABASE PROJECT', 'A railway system, which needs to model the following:\n Stations\n Tracks, connecting stations. You can assume for simplicity that only one track exists between any two stations. All the tracks put together form a graph.\n Trains, with an ID and a name\n Train schedules recording what time a train passes through each station on its route.', '2012-04-04'),
(95, 118, 'Distributed IR Testbed Definitions:', 'trec123-100-sample300-callan99.v1a: (115 KB gzipped, 650 KB uncompressed):\n\nDescription: A 100 collection testbed created by sampling trec123-100-bysource-callan99.v2a.\nPublication: In progress.\ntrec123-100-bysource-callan99.v2a: (3 MB gzipped, 23 MB uncompressed):\n\nDescription: A 100 collection testbed created from TREC CDs 1, 2, and 3.\nPublication: A.L. Powell, J.C. French, J. Callan, M. Connell, and C.L. Viles, "The impact of database selection on distributed searching." In Proceedings of the 23rd International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 00), Athens, 2000.\ntrec123-100-bysource-callan99.v1b: (3 MB gzipped, 23 MB uncompressed):\n\nDescription: A 100 collection testbed created from TREC CDs 1, 2, and 3. 161 AP ''90 documents from CD 3 were omitted inadvertently.\nPublication: J. French, A. Powell, J. Callan, C. Viles, T. Emmitt, K. Prey, and Y. Mou, "Comparing the performance of database selection algorithms." In the Proceedings of the 22nd International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 99), Berkeley, CA, August 15-19, 1999.\ntrecvlc1-921-bysource-callan99 (18 MB gzipped, 260 MB uncompressed):\n\nDescription: A 921 collection testbed created from the first TREC VLC corpus.\nAuthors: J. Callan and Y. Mou.\nPublication: J. French, A. Powell, J. Callan, C. Viles, T. Emmitt, K. Prey, and Y. Mou, "Comparing the performance of database selection algorithms." In the Proceedings of the 22nd International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 99), Berkeley, CA, August 15-19, 1999.', '2015-04-04'),
(96, 118, 'Data mining models for student careers', 'This project performsa data mining methodology to analyze the careers of University graduated students. We present different approaches based on clustering and sequential patterns techniques in order to identify strategies for improving the performance of students and the scheduling of exams. We introduce an ideal career as the career of an ideal student which has taken each examination just after the end of the corresponding course, without delays. We then compare the career of a generic student with the ideal one by using the different techniques just introduced. Finally, we apply the methodology to a real case study and interpret the results which underline that the more students follow the order given by the ideal career the more they get good performance in terms of graduation time and final grade.', '2015-04-04'),
(97, 119, 'MONDO', 'As Model Driven Engineering (MDE) is increasingly applied to larger and more complex systems, the current generation of modelling and model management technologies are being pushed to their limits in terms of capacity and efficiency, and as such, additional research is imperative in order to enable MDE to remain relevant with industrial practice and continue delivering its widely recognised productivity, quality, and maintainability benefits.', '2014-04-04'),
(98, 120, 'Integrating commercial ambulatory electronic health records with hospital systems: An evolutionary process', 'The increase in electronic health record implementation in all treatment venues has led to greater demands for integration within and across practice settings with different work cultures. We study the evolution of coordination processes when integrating ambulatory-specific electronic health records with hospital systems.', '2015-05-05'),
(99, 120, 'MediTouch EHR Electronic Health Record Software', 'MediTouch EHR® is a complete electronic health record (EHR) that was designed to be both web-based and offer a touchscreen user interface. As a result, the product is highly differentiated and easy-to-use. The core EHR offers functionality for charting, problem lists, medication management, electronic prescribing, allergy checks, order management, lab tests and document management, among other capabilities. The system can also be configured to meet a provider’s personal charting preferences and custom forms can be implemented to work with MediTouch’s touchscreen technology. MediTouch EHR can be deployed standalone or in conjunction with HealthFusion’s practice management system. The complete system is certified by an ONC-ATCB to meet the meaningful use criteria for ARRA stimulus funds.\n\nWhen HealthFusion set about designing its own EHR, the company decided to focus on designing a system that was revolutionary in its simplicity. Because the system is entirely web-based and hosted by HealthFusion, customers do not need to install or maintain any software on-premise.', '2014-05-05');

-- --------------------------------------------------------

--
-- Table structure for table `project_concepts`
--

CREATE TABLE IF NOT EXISTS `project_concepts` (
  `pc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `project_id` int(11) unsigned NOT NULL,
  `concept_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`pc_id`),
  KEY `profile_id` (`profile_id`),
  KEY `project_id` (`project_id`),
  KEY `concept_id` (`concept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=231 ;

--
-- Dumping data for table `project_concepts`
--

INSERT INTO `project_concepts` (`pc_id`, `profile_id`, `project_id`, `concept_id`) VALUES
(32, 71, 17, 79),
(33, 71, 18, 44),
(34, 72, 19, 10),
(35, 72, 19, 20),
(36, 72, 20, 10),
(37, 72, 20, 20),
(38, 72, 20, 22),
(39, 72, 21, 20),
(40, 72, 21, 22),
(41, 72, 21, 25),
(42, 73, 22, 44),
(43, 73, 22, 61),
(44, 73, 23, 44),
(45, 73, 23, 62),
(46, 74, 24, 2),
(47, 74, 24, 37),
(48, 74, 25, 2),
(49, 74, 25, 75),
(50, 75, 26, 37),
(51, 75, 26, 39),
(52, 75, 26, 41),
(53, 77, 28, 10),
(54, 77, 28, 13),
(55, 77, 28, 15),
(56, 77, 28, 16),
(57, 77, 28, 18),
(58, 77, 29, 82),
(59, 77, 29, 83),
(60, 77, 29, 85),
(61, 79, 31, 13),
(62, 79, 31, 15),
(63, 79, 31, 16),
(64, 79, 31, 18),
(65, 79, 31, 86),
(66, 79, 31, 93),
(67, 79, 32, 13),
(68, 79, 32, 14),
(69, 79, 32, 15),
(70, 79, 32, 16),
(71, 79, 32, 86),
(72, 79, 32, 93),
(73, 80, 33, 37),
(74, 80, 33, 41),
(75, 80, 33, 44),
(76, 82, 35, 65),
(77, 82, 35, 66),
(78, 82, 35, 67),
(79, 82, 35, 68),
(80, 82, 35, 69),
(81, 82, 35, 70),
(82, 82, 36, 65),
(83, 82, 36, 66),
(84, 82, 36, 67),
(85, 82, 36, 68),
(86, 82, 36, 69),
(87, 82, 36, 70),
(88, 83, 38, 90),
(89, 84, 39, 13),
(90, 85, 40, 17),
(91, 85, 41, 16),
(92, 85, 41, 18),
(93, 85, 41, 86),
(94, 85, 41, 90),
(95, 85, 41, 93),
(96, 87, 43, 87),
(97, 87, 43, 93),
(98, 87, 44, 17),
(99, 89, 46, 2),
(100, 89, 46, 37),
(101, 89, 46, 38),
(102, 89, 47, 2),
(103, 89, 47, 13),
(104, 89, 48, 33),
(105, 89, 48, 74),
(106, 89, 48, 87),
(107, 89, 49, 72),
(108, 89, 49, 87),
(109, 90, 50, 2),
(110, 90, 50, 7),
(111, 90, 51, 37),
(112, 90, 51, 39),
(113, 91, 52, 65),
(114, 91, 52, 66),
(115, 91, 52, 67),
(116, 91, 52, 68),
(117, 91, 52, 69),
(118, 91, 52, 70),
(119, 92, 53, 14),
(120, 92, 53, 17),
(121, 92, 53, 19),
(122, 92, 53, 20),
(123, 92, 53, 22),
(124, 92, 53, 25),
(125, 92, 53, 88),
(126, 92, 53, 90),
(127, 92, 54, 14),
(128, 92, 54, 17),
(129, 92, 54, 19),
(130, 92, 54, 72),
(131, 92, 54, 88),
(132, 92, 54, 90),
(133, 92, 55, 14),
(134, 92, 55, 19),
(135, 92, 55, 25),
(136, 92, 55, 88),
(137, 92, 56, 65),
(138, 92, 56, 66),
(139, 92, 56, 67),
(140, 92, 56, 68),
(141, 92, 56, 69),
(142, 92, 56, 70),
(143, 92, 57, 15),
(144, 92, 57, 16),
(145, 92, 57, 83),
(146, 92, 57, 86),
(147, 92, 57, 90),
(148, 93, 58, 82),
(149, 93, 58, 83),
(150, 93, 59, 37),
(151, 94, 60, 82),
(152, 94, 60, 83),
(153, 94, 61, 37),
(154, 95, 62, 79),
(155, 97, 64, 33),
(156, 97, 64, 88),
(157, 97, 65, 13),
(158, 98, 66, 33),
(159, 98, 66, 88),
(160, 98, 67, 88),
(161, 98, 67, 93),
(162, 98, 68, 13),
(163, 98, 69, 13),
(164, 99, 70, 2),
(165, 99, 70, 74),
(166, 100, 71, 2),
(167, 101, 72, 10),
(168, 101, 72, 15),
(169, 101, 72, 62),
(170, 101, 72, 86),
(171, 102, 73, 10),
(172, 102, 73, 24),
(173, 104, 75, 10),
(174, 104, 75, 15),
(175, 104, 75, 16),
(176, 104, 75, 18),
(177, 104, 75, 79),
(178, 104, 75, 86),
(179, 105, 76, 10),
(180, 105, 76, 12),
(181, 106, 77, 10),
(182, 106, 77, 23),
(183, 107, 78, 83),
(184, 107, 78, 85),
(185, 107, 79, 84),
(186, 107, 79, 85),
(187, 108, 80, 10),
(188, 108, 80, 91),
(189, 108, 80, 94),
(190, 108, 81, 10),
(191, 108, 81, 92),
(192, 108, 81, 94),
(193, 111, 84, 2),
(194, 111, 84, 4),
(195, 111, 84, 62),
(196, 111, 84, 73),
(197, 111, 85, 2),
(198, 111, 85, 3),
(199, 111, 85, 4),
(200, 111, 85, 72),
(201, 113, 87, 33),
(202, 113, 88, 35),
(203, 114, 89, 10),
(204, 114, 89, 26),
(205, 114, 89, 31),
(206, 114, 89, 47),
(207, 114, 90, 14),
(208, 114, 90, 15),
(209, 114, 90, 16),
(210, 114, 90, 18),
(211, 114, 90, 88),
(212, 114, 91, 15),
(213, 114, 91, 16),
(214, 114, 91, 18),
(215, 114, 91, 88),
(216, 114, 91, 89),
(217, 115, 92, 2),
(218, 115, 92, 5),
(219, 115, 92, 74),
(220, 118, 95, 21),
(221, 118, 95, 79),
(222, 118, 96, 6),
(223, 118, 96, 21),
(224, 119, 97, 26),
(225, 119, 97, 37),
(226, 119, 97, 64),
(227, 120, 98, 36),
(228, 120, 98, 79),
(229, 120, 99, 36),
(230, 120, 99, 79);

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `pub_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `date_time` date NOT NULL,
  PRIMARY KEY (`pub_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`pub_id`, `profile_id`, `name`, `details`, `date_time`) VALUES
(16, 72, 'Semantic Web', 'The Semantic Web is still a web, a collection of linked nodes.\nNavigation of links is currently, and will remain for humans if\nnot machines, a key mechanism for exploring the space. The\nSemantic Web is viewed by many as a knowledge base, a\ndatabase or an indexed and searchable document collection; in\nthe work discussed here we view it as a hypertext.\nThe aim of the COHSE project is to research into methods to\nimprove significantly the quality, consistency and breadth of\nlinking of Web documents at retrieval time (as readers browse\nthe documents) and authoring time (as authors create the\ndocuments). The objective is link creation rather than\nresource discovery; in contrast, many existing projects are\nconcerned primarily with the discovery of resources (reading),\nrather than the construction of hypertexts (authoring). The\nproject plans to produce a COHSE (Conceptual Open\nHypermedia ServicE) by integrating an ontological reasoning\nservice with a Web-based Open Hypermedia link service. This\nwill form a Conceptual Hypermedia system enabling\ndocuments to be linked via metadata describing their contents.\nThe bringing together of Open Hypermedia and Ontology\nservices can be seen as one particular implementation of the\nSemantic Web. Here we briefly present open and conceptual\nhypermedia, and introduce the architecture being employed\nwithin the COHSE project, and the prototype COHSE\nplatform we have developed. We present the questions that we\nnow plan to address that surround the Semantic Web when\nviewed from the perspective of a hypertext for people\n', '2004-01-01'),
(17, 73, 'E-Journal Management System with semantic approach ', 'E-Journal Management System with semantic approach.', '2015-01-10'),
(18, 74, '', '', '0000-00-00'),
(19, 75, 'RESEARCH CENTER IN INFORMATION SYSTEM ENGINEERING', 'Nowadays, developing information systems and integrating them into global business services are extremely difficult endeavors. Current challenges include unprecedented levels of complexity, increasingly large numbers of stakeholders, and the necessity to master a growing range of skills, techniques, tools and methods. Mastering such a process requires a high-level expertise and a fine weave of interactions between complementary disciplines, most importantly Software Engineering and Information Management.PReCISE promotes scientific research in and across these two disciplines, and ambitions to narrow the gap between them.', '2000-01-02'),
(20, 76, 'Impact Of IDM Lane-Changing On The Performance Of AODV On VANETs', 'In order to introduce a realistic simulation environment to support the research on vehicular Ad hoc Networks (VANETs), the human behavior must be studied, understood and included in the mobility model representing the vehicles movement. This paper studies the behavior of the driver concerning specifically the decision to change the lane. Our study is based on real world observations and safety driving rules, and introduces a new lane-changing simulator accordingly. This simulator is integrated within Intelligent Driver Model with Intersection Management (IDM-IM) and compared with Intelligent Driver Model with Lane Changes (IDM-LC) as well as IDM-IM mobility models provided by VanetMobiSim framework, concerning their effects on the performance of Ad hoc On Demand Routing Protocol (AODV) and reality imitation. \nKeywords-component; VANETs; mobility models; lane changing simulation; performance evaluation; routing protocols\n', '2013-02-04'),
(21, 76, 'Simple But Effective Ways to Improve Refugee Education', 'A summary of the most striking results of a field- based qualitative research study concerning the Syrian refugee children in the Lebanese public schools.', '2014-12-10'),
(22, 76, 'The resilience of Syrian refugee children in Lebanon : What do they need to acquire quality education?', 'A pilot study following the Education Resilience Approaches (ERA) framework to discover what affects Syrian children in Lebanese schools.', '2014-12-10'),
(31, 83, 'How can I start bootstrap?', 'When I started using bootstrap already felt it something completely new I do not know how I''ll start out but I went all these questions when I started\n\nBecause this library customer asks for its location so I decided that I Ptalmha I did not have a choice and I was surprised how easy to learn and work already facilitated by the very things I was taking a much longer time using other offices for the establishment of Style site.\n', '2014-12-11'),
(38, 90, 'INFORMATION SYSTEMS SECURITY ENGINEERING', 'Several aspects of problems with the state of information systems\nsecurity are presented; the main points concern incorrect marketing and\ncustomer expectations. They are followed by identification of solutions; the main\npoint being education. The last section explains what efforts Sparta Inc. and the\nNSA have accomplished and are doing to fix the problems cited\n', '2002-01-01'),
(45, 97, '', '', '0000-00-00'),
(46, 98, '', '', '0000-00-00'),
(47, 99, 'Data Mining: Concepts and Techniques', 'Our ability to generate and collect data has been increasing rapidly. Not only are all of our business, scientific, and government transactions now computerized, but the widespread use of digital cameras, publication tools, and bar codes also generate data. On the collection side, scanned text and image platforms, satellite remote sensing systems, and the World Wide Web have flooded us with a tremendous amount of data. This explosive growth has generated an even more urgent need for new techniques and automated tools that can help us transform this data into useful information and knowledge. Like the first edition, voted the most popular data mining book by KD Nuggets readers, this book explores concepts and techniques for the discovery of patterns hidden in large data sets, focusing on issues relating to their feasibility, usefulness, effectiveness, and scalability. However, since the publication of the first edition, great progress has been made in the development of new data mining methods, systems, and applications. This new edition substantially enhances the first edition, and new chapters have been added to address recent developments on mining complex types of data? including stream data, sequence data, graph structured data,social network data, and multi-relational data.\nPublished in 2000.\n', '2000-01-01'),
(48, 100, 'Hierarchical Queries with DB2 Connect By', 'According to the SQL Standard, hierarchical (e.g. organization charts, bill of materials) or bi-directional data (e.g. flight connections) can be evaluated by using a recursive common table expression (RCTE). RCTEs were delivered with release V5R4 of DB2 for i. Other databases such as Oracle use a non-standard method for querying data called hierarchical query clause. To allow maximum portability the hierarchical query clause is introduced with PTF SF99701 Version 9 in DB2 for i. This article will explain the syntax of the hierarchical query clause, how it can be implemented in composition with new operators, pseudo columns and special scalar functions.', '2011-02-28'),
(49, 100, 'System versus SQL naming : Part 1 - Object authority and privileges for SQL database objects', 'When executing SQL statements you can run them either using System or SQL Naming. The System Naming conventions follow the traditional methods used on IBM i systems such as library support. The SQL Naming mode on the other hand is defined in the SQL Standard and used by all other databases. When asking what is the difference between SQL and System Naming, you will normally get the answer, schema and object are either separated with a slash or a period. However there are many more differences especially with regard to the access authority and ownership of database objects created either using SQL or System Naming. This article will show the differences between SQL and System Naming primarily focusing on how the ownerships and access authorities vary when creating DB2 for i database objects (such as tables, stored procedures or triggers) with the two naming conventions.', '2012-02-03'),
(50, 101, 'Techniques to reduce cluttering of RDF visualizations', 'The Resource Description Framework (RDF) provides a generic way to represent data as a graph using Web standards. The benefits of using RDF are multiple, including an extensible and generic data model that can be used to represent almost any knowledge domain. The fact that RDF is an open standard also benefits consumers, developers and designers, giving them a common language to created applications and publish data.\n\nIn many cases, users need to consume RDF without a custom user interface. This is especially critical when users explore new vocabularies and ontologies, in the attempt to use them in their own work. Depending on the complexity of the graph, this may be a hard task for many people. In this paper, we propose a series of optimizations to an existing RDF visualizer called visualRDF that improves the legibility of visual representations of RDF graphs. We show several techniques used to improve the legibility and reduce the cluttering of the visual representation of these graphs.', '2006-02-04'),
(51, 101, 'Robust trait composition for Javascript', 'We introduce traits.js, a small, portable trait composition library for Javascript. Traits are a more robust alternative to multiple inheritance and enable object composition and reuse. traits.js is motivated by two goals: first, it is an experiment in using and extending Javascript’s recently added meta-level object description format. By reusing this standard description format, traits.js can be made more interoperable with similar libraries, and even with built-in primitives. Second, traits.js makes it convenient to create “high-integrity” objects whose integrity cannot be violated by clients, an important property when web content is composed from mutually suspicious scripts. We describe the design of traits.js and provide an operational semantics for traits-js, a minimal calculus that models the core functionality of the library.', '2012-11-05'),
(52, 102, 'How ‘big data’ can make big impact: Findings from a systematic review and a longitudinal case study', 'Big data has the potential to revolutionize the art of management. Despite the high operational and strategic impacts, there is a paucity of empirical research to assess the business value of big data. Drawing on a systematic review and case study findings, this paper presents an interpretive framework that analyzes the definitional perspectives and the applications of big data. The paper also provides a general taxonomy that helps broaden the understanding of big data and its role in capturing business value. The synthesis of the diverse concepts within the literature on big data provides deeper insights into achieving value through big data strategy and implementation.', '2011-04-04'),
(53, 102, 'Managing a Big Data project: The case of Ramco Cements Limited', 'Currently many organizations are in the process of implementing Big Data related projects in order to extract meaningful insights from their data for better decision making. Though there are various frameworks postulating the best practices which should be adopted while implementing analytics projects, they do not cater to the complexities associated with a Big Data project. In this paper our goal is two-fold: to develop a new framework that can provide organizations a holistic roadmap in conceptualizing, planning and successfully implementing Big Data projects and to validate this framework through our observation of a descriptive case study of an organization that has implemented such a project. Although the manufacturing sector has been slow in incorporating analytics in their strategic decision making, the situation is changing with increasing use of analytics for product development, operations and logistics. We explore the Big Data project at a manufacturing company, Ramco Cements Limited, India, describe the system developed by them and highlight the benefits accrued from it. We investigate the entire process by which the project is implemented using the lens of our proposed framework. Our results reveal that a clear understanding of the business problem, a detailed and well planned step-by-step project map, a cross functional project team, adoption of innovative visualization techniques, patronage and active involvement of top management and a culture of data driven decision making are essential for the success of a Big Data project.', '2013-07-12'),
(54, 103, 'Onyx: A Linked Data approach to emotion representation', 'Extracting opinions and emotions from text is becoming increasingly important, especially since the advent of micro-blogging and social networking. Opinion mining is particularly popular and now gathers many public services, datasets and lexical resources. Unfortunately, there are few available lexical and semantic resources for emotion recognition that could foster the development of new emotion aware services and applications. The diversity of theories of emotion and the absence of a common vocabulary are two of the main barriers to the development of such resources. This situation motivated the creation of Onyx, a semantic vocabulary of emotions with a focus on lexical resources and emotion analysis services. It follows a linguistic Linked Data approach, it is aligned with the Provenance Ontology, and it has been integrated with the Lexicon Model for Ontologies (lemon), a popular RDF model for representing lexical entries. This approach also means a new and interesting way to work with different theories of emotion. As part of this work, Onyx has been aligned with EmotionML and WordNet-Affect.', '2013-12-09'),
(55, 103, 'Open linked data and mobile devices as e-tourism tools. A practical approach to collaborative e-learning', 'Collaborative e-learning is based on a model where knowledge is created through interaction between members that interact by sharing experiences and knowledge.\n\nLinked Open Data (LOD) allow to link data with other related data in which people are interested. As a practical approach we introduce a system where is built cultural heritage knowledge from collaboration between open data published by a regional government, enriched with data from other sources of open linked data cloud like DBPedia.\n\nMobile technologies in e-learning allow learning in any place and moment. We try to take advantage of mobile and semantic technologies, like LOD, in a collaborative e-learning environment. Knowledge is built by enriching open data that are shared by institutions to knowledge shared on Semantic Web, by using LOD techniques, leading a scenario of collaborative and human learning by linking data sources and people who share these data.\n\nThe collaboration between web of data and open data from a public institution has resulted in practice, in a mobile tourism application that works as a tourist guide for citizens. Application also allows the collaboration of tour guides to add new knowledge about the cultural heritage resources. In brief, collaboration between different profiles generates knowledge and collaborative learning.', '2014-12-12'),
(56, 104, 'Modeling query-document dependencies with topic language models for information retrieval', 'This paper addresses deficiencies in current information retrieval models by integrating the concept of relevance into the generation model using various topical aspects of the query. The models are adapted from the latent Dirichlet allocation model, but differ in the way that the notation of query-document relevance is introduced in the modeling framework. In the first method, query terms are added to relevant documents in the training of the latent Dirichlet allocation model. In the second method, the latent Dirichlet allocation model is expanded to deal with relevant query terms. The topic of each term within a given document may be sampled using either the normal document-specific mixture weights in LDA using query-specific mixture weights. We also developed an efficient method based on the Gibbs sampling technique for parameter estimation. Experiment results based on the Text REtrieval Conference Corpus (TREC) demonstrate the superiority of the proposed models.', '2011-02-04'),
(57, 104, 'Integrating distributed sources of information for construction cost estimating using Semantic Web and Semantic Web Service technologies', 'A construction project requires collaboration of several organizations such as owner, designer, contractor, and material supplier organizations. These organizations need to exchange information to enhance their teamwork. Understanding the information received from other organizations requires specialized human resources. Construction cost estimating is one of the processes that requires information from several sources including a building information model (BIM) created by designers, estimating assembly and work item information maintained by contractors, and construction material cost data provided by material suppliers. Currently, it is not easy to integrate the information necessary for cost estimating over the Internet.\n\nThis paper discusses a new approach to construction cost estimating that uses Semantic Web technology. Semantic Web technology provides an infrastructure and a data modeling format that enables accessing, combining, and sharing information over the Internet in a machine processable format. The estimating approach presented in this paper relies on BIM, estimating knowledge, and construction material cost data expressed in a web ontology language. The approach presented in this paper makes the various sources of estimating data accessible as Simple Protocol and Resource Description Framework Query Language (SPARQL) endpoints or Semantic Web Services. We present an estimating application that integrates distributed information provided by project designers, contractors, and material suppliers for preparing cost estimates. The purpose of this paper is not to fully automate the estimating process but to streamline it by reducing human involvement in repetitive cost estimating activities.', '2013-02-02'),
(58, 105, 'Revenue management for Cloud computing providers: Decision models for service admission control under non-probabilistic uncertainty', 'Cloud computing promises the flexible delivery of computing services in a pay-as-you-go manner. It allows customers to easily scale their infrastructure and save on the overall cost of operation. However Cloud service offerings can only thrive if customers are satisfied with service performance. Allowing instantaneous access and flexible scaling while maintaining the service levels and offering competitive prices poses a significant challenge to Cloud computing providers. Furthermore services will remain available in the long run only if this business generates a stable revenue stream. To address these challenges we introduce novel policy-based service admission control models that aim at maximizing the revenue of Cloud providers while taking informational uncertainty regarding resource requirements into account. Our evaluation shows that policy-based approaches statistically significantly outperform first come first serve approaches, which are still state of the art. Furthermore the results give insights in how and to what extent uncertainty has a negative impact on revenue.', '2013-12-08'),
(59, 105, 'Security in cloud computing: Opportunities and challenges', 'The cloud computing exhibits, remarkable potential to provide cost effective, easy to manage, elastic, and powerful resources on the fly, over the Internet. The cloud computing, upsurges the capabilities of the hardware resources by optimal and shared utilization. The above mentioned features encourage the organizations and individual users to shift their applications and services to the cloud. Even the critical infrastructure, for example, power generation and distribution plants are being migrated to the cloud computing paradigm. However, the services provided by third-party cloud service providers entail additional security threats. The migration of user’s assets (data, applications, etc.) outside the administrative control in a shared environment where numerous users are collocated escalates the security concerns. This survey details the security issues that arise due to the very nature of cloud computing. Moreover, the survey presents the recent solutions presented in the literature to counter the security issues. Furthermore, a brief view of security vulnerabilities in the mobile cloud computing are also highlighted. In the end, the discussion on the open issues and future research directions is also presented.', '2015-02-09'),
(60, 106, 'A spatial web/agent-based model to support stakeholders'' negotiation regarding land development', 'Decision making in land management can be greatly enhanced if the perspectives of concerned stakeholders are taken into consideration. This often implies negotiation in order to reach an agreement based on the examination of multiple alternatives. This paper describes a spatial web/agent-based modeling system that was developed to support the negotiation process of stakeholders regarding land development in southern Alberta, Canada. This system integrates a fuzzy analytic hierarchy procedure within an agent-based model in an interactive visualization environment provided through a web interface to facilitate the learning and negotiation of the stakeholders. In the pre-negotiation phase, the stakeholders compare their evaluation criteria using linguistic expressions. Due to the uncertainty and fuzzy nature of such comparisons, a fuzzy Analytic Hierarchy Process is then used to prioritize the criteria. The negotiation starts by a development plan being submitted by a user (stakeholder) through the web interface. An agent called the proposer, which represents the proposer of the plan, receives this plan and starts negotiating with all other agents. The negotiation is conducted in a step-wise manner where the agents change their attitudes by assigning a new set of weights to their criteria. If an agreement is not achieved, a new location for development is proposed by the proposer agent. This process is repeated until a location is found that satisfies all agents to a certain predefined degree. To evaluate the performance of the model, the negotiation was simulated with four agents, one of which being the proposer agent, using two hypothetical development plans. The first plan was selected randomly; the other one was chosen in an area that is of high importance to one of the agents. While the agents managed to achieve an agreement about the location of the land development after three rounds of negotiation in the first scenario, seven rounds were required in the second scenario. The proposed web/agent-based model facilitates the interaction and learning among stakeholders when facing multiple alternatives.', '2011-12-04'),
(61, 106, 'Effects of specialization in computers, web sites, and web agents on e-commerce trust', 'Suppose you went shopping online for wines and visited several sites, each recommending particular reds and whites. Which kind of site are you likely to trust more—costco.com or wine.com? The specialization implied by the latter suggests more expertise in the domain of wines. Does it mean that you are more likely to purchase wines recommended by sites such as wine.com and vintagecellars.com.au than those recommended by generalist sites such as costco.com and samsclub.com? Our study attempts to answer this question by experimentally investigating how specialization in media technology (specifically, web agent, web site, and computer) influences individuals’ perception and attitudes towards sources in online communication, particularly consumer trust and purchase behaviors in e-commerce. All subjects (N=124) went to a specially constructed online site with a virtual shopping cart for a wine-purchasing task, as part of a 2 (specialist vs. generalist web agent)×2 (specialist vs. generalist web site)×2 (specialist computer vs. generalist computer) between-subjects experiment. Results indicate significant main effects and interactions of the agent, site, and computer specialization on trust and purchase decision time. Theoretical and practical implications are discussed.', '2011-12-04'),
(62, 107, 'Characteristics of a sustainable Learning and Content Management System (LCMS)', 'This presentation along with associated paper focuses on characteristics of sustainable learning and content management systems (LCMS). It has three folds. The first one provides a list of characteristics for a sustainable LCMS that are compiled as a result of critical literature review as well as personal experiences of the author. The second intends to reveal the results of an evaluation study in which above characteristics were used to examine available LMS and/or LCMS systems in Turkey. The third fold consists of another list for decisions makers in corporate and educational institutions about what to consider while selecting a LCMS. There is quite a big body of literature regarding characteristics of LCMS in the field. However, the majority of them are context dependent and/or do not reflect the needs of other contexts such as those in which distance education is more than a convenience. So, a dozen of this literature has been critically analyzed and common characteristics were compiled. Later, these common characteristics revised according to needs of those contexts where distance education is a necessity and the list of characteristics of a sustainable LCMS is finalized. Moreover, these characteristics were used to create a rubric to be able to evaluate the common LCMS used in Turkey. The results, a list of recommendations are prepared to help the decision makers in the corporate and educational institutions choose the sustainable LCMS for their e-learning initiatives. This presentation might especially be beneficial for especially those audiences who are considering adopting a LCMS for their institutions and those researchers who are interested in management of e-learning initiatives.', '2010-02-02'),
(63, 107, 'Student-oriented planning of e-learning contents for Moodle', 'We present a way to automatically plan student-oriented learning contents in Moodle. Rather than offering the same contents for all students, we provide personalized contents according to the students׳ background and learning objectives. Although curriculum personalization can be faced in several ways, we focus on artificial intelligence (AI) planning as a very useful formalism for mapping actions, i.e. learning contents, in terms of preconditions (precedence relationships) and causal effects to find plans, i.e. learning paths that best fit the needs of each student. A key feature is that the learning path is generated and shown in Moodle in a seamless way for both the teacher and student, respectively. We also include some experimental results to demonstrate the scalability and viability of our approach.', '2011-04-09'),
(64, 108, 'Design and programming patterns for implementing usability functionalities in web applications', 'Usability is a software system quality attribute. There are usability issues that have an impact not only on the user interface but also on the core functionality of applications. In this paper, three web applications were developed to discover patterns for implementing two usability functionalities with an impact on core functionality: Abort Operation and Progress Feedback. We applied an inductive process in order to identify reusable elements to implement the selected functionalities. For communication purposes, these elements are specified as design and programming patterns (PHP, VB.NET and Java). Another two web applications were developed in order to evaluate the patterns. The evaluation explores several issues such as ease of pattern understanding and ease of pattern use, as well as the final result of the applications.\n\nWe found that it is feasible to reuse the identified solutions specified as patterns. The results also show that usability functionalities have features, like the level of coupling with the application or the complexity of each component of the solution, that simplify or complicate their implementation. In this case, the Abort Operation functionality turned out to be more feasible to implement than the Progress Feedback functionality.', '2013-02-12'),
(65, 108, 'Design Pattern for Feature-Oriented Service Injection and Composition of Web Services for Distributed Computing Systems with SOA', 'In the world of today''s Enterprise Java Applications development, there is a need to support the principles of the Service Oriented Architecture (SOA). So in order to develop the SOA based applications any enterprise java developer should have the strong foundation in Java Web Services (JWS). As most of the leading enterprise java vendors feel that it''s perfect to use the JWS technologies for the development of their SOA applications. In this paper we will propose a new design pattern in terms of Distributed application development, where we will be concentrating on the web services invocation and the composition of web services with the support of SOA. The paper will provide a perfect idea about how the amalgamation of Visitor Design Pattern and Case-Based Reasoning Design Pattern help us for the development of the Service Invocation and Web Services Composition through SOA with the help of JWS technologies. And also with the help of newly introduced programming models like Feature-Oriented Programming (FOP), it will be flexible for us to include the new service invocation function into the service providing server as a Feature Module. We have provided with the sample code that we have developed for the application in our paper. Here we will be using the Service Oriented Architecture (SOA) with Web Services in Java to Implement the Design Pattern. The pattern is described using a java-like notation for the classes and interfaces. A simple UML Class and Sequence diagrams are depicted.', '2014-01-06'),
(66, 109, 'CONSERT: Applying semantic web technologies to context modeling in ambient intelligence', 'Representation and reasoning about context information is a main research area in Ambient Intelligence (AmI). Context modeling in such applications is facing openness and heterogeneity. To tackle such problems, we argue that usage of semantic web technologies is a promising direction. We introduce CONSERT, an approach for context meta-modeling offering a consistent and uniform means for working with domain knowledge, as well as constraints and meta-properties thereof. We provide a formalization of the model and detail its innovative implementation using techniques from the semantic web community such as ontology modeling and SPARQL. A stepwise example of modeling a commonly encountered AmI scenario showcases the expressiveness of our approach. Finally, the architecture of the representation and reasoning engine for CONSERT is presented and evaluated in terms of performance.', '2011-12-11'),
(67, 109, 'Deriving collective intelligence from reviews on the social Web using a supervised learning approach', 'The Social Web holds significant value for consumer intelligence. Today, tremendous valuable information behind the social Web has not yet been fully utilized by firms to achieve competitive advantages. In this paper, we propose a machine learning approach to model latent, heterogeneous consumer tastes from social Web reviews. Our approach employs the Mixture of Experts (ME) method to derive a set of distinct consumer clusters, each of which has a cluster-conditional taste template regression model. Furthermore, our approach is able to predict the cluster membership as well as the overall response solely from a consumer’s review ratings. We provide system architecture, model specification, and result analysis. We also compare the ME model with the Latent Regression Model. In practice, our ME learning approach will enable businesses to identify distinct consumer clusters directly from the social Web in order to satisfy consumers through customization and differentiation based on cluster-specific taste templates and cluster membership.', '2012-12-12'),
(68, 110, 'A decision support system to develop a quality management in academic digital libraries', 'Academic digital libraries are getting more benefit from the Web possibilities to help with teaching, learning and research activities. Because of it, more and more people use the services that they offer. Therefore, it is very important that the academic digital libraries provide a good service in order to satisfy the users’ expectations. The aim of this paper is to present a decision support system assisting the staff of the academic digital libraries to make decisions in order to meet the users’ needs and, in such a way, to increase the number of users utilizing them. To do so, the decision support system is composed of several decision rules which generates recommendations according to both objective and subjective criteria to improve the quality of the services offered by the academic digital libraries.', '2012-03-06'),
(69, 110, 'Quality management in European screening laboratories in blood establishments: A view of current approaches and trends', 'An entire concrete waste reuse model for producing recycled aggregate class H concrete was established with the objective of recycling concrete waste generated during anticipated demolition of older buildings in urban areas. In a redevelopment project of Obayashi Technical Research Institute, a 24-year old building was demolished and concrete waste was used to produce high-quality recycled fine and coarse aggregate using a heat grinder system. Then the quality of concrete using these recycled materials was tested and applied to fair-faced concrete structures of a new building. Fine powder, a by-product in the recycling process, was also reused as a material for clay tiles to cover the floor of the new building. This model enabled all the concrete waste to be recycled.', '2014-07-09'),
(70, 110, 'Equivalent pipe algorithm for metal spiral casing and its application in hydraulic transient computation based on equiangular spiral model', 'Equivalent pipe algorithm for metal spiral casing and its application in hydraulic transient computation based on equiangular spiral model', '2015-09-10'),
(71, 111, 'Object-relational data modelling for informetric databases', 'Informetric researchers have long chafed at the limitations of bibliographic databases for their analyses, without being able to visualize or develop real solutions to the problem. This paper describes a solution developed to provide for the specialist needs of informetric researchers. In a collaborative exercise between the fields of computer science and informetrics, data modelling was used in order to address the requirements of complex and dynamic informetric data. This paper reports on this modelling experience with its aim of building an object-relational database (ORDB) for informetric research purposes. The paper argues that ORM (object-relational model) is particularly suitable because it allows for the modelling of complex data and accommodates the various data source formats and standards used by a variety of bibliographic databases. Further, ORM captures the dynamic nature of informetric data by allowing user-defined data types and by embedding basic statistical calculating tools as object functions in these user-defined data types. The main ideas of the paper are implemented in an Oracle database management system.', '2012-05-08'),
(72, 111, 'Object-relational complex structures for XML storage', 'XML data can be stored in various database repositories, including Object-Relational Database (ORDB). Using an ORDB, we get the benefit of the relational maturity and the richness of Object-Oriented modeling, including various complex data types. These data types resemble the true nature of XML data and therefore, the conceptual semantic of XML data can be preserved. However, very often when the data is stored in an ORDB repository, they are treated as purely flat tables. Not only do we not fully utilize the facilities in current ORDB, but also we do not preserve the conceptual semantic of the XML data.\nIn this paper, we propose novel methodologies to store XML data into new ORDB data structures, such as user-defined type, row type and collection type. Our methodology has preserved the conceptual relationship structure in the XML data, including aggregation, composition and association. For XML data retrieval, we also propose query classification based on the current SQL.\nCompared to the existing techniques, this work has several contributions. Firstly, it utilizes the newest features of ORDB for storing XML data. Secondly, it covers a full database design process, from the conceptual to the implementation phase. Finally, the proposed transformation methodologies maintain the conceptual semantics of the XML data by keeping the structure of the data in different ORDB complex structures.', '2013-03-12'),
(73, 112, 'Detecting defects in software requirements specification', 'This research is concerned with detecting defects in software requirements specification. Motivated by both the problem of producing reliable requirements and the limitations of existing taxonomies to provide a satisfactory level of information about defects in the requirements phase, we focus on providing a better tool for requirements analysts. Only few attempts have been made to classify defects and defect detection techniques. Scattered knowledge about defects and defect detection techniques needs compilation and re-evaluation in order to enhance the ability to discover defects in the requirements phase. Toward this end, this work presents a taxonomy of requirements defects and the causes of their occurrences. The purpose is to reach a comprehensive understanding of both the sources of the problem and the solutions of possible defects and defect detection techniques. The taxonomy’s design is based on the analysis of each defect and its sources. In addition, this paper proposes a combined-reading technique for defects in requirements. The proposed technique avoids the shortcomings of other reading techniques. The result of applying the recommendations of this work specifically improves the quality of the requirements specification and generally software quality.', '2011-12-07'),
(74, 112, 'Formal verification of functional properties of a SCR-style software requirements specification using PVS ', 'Industrial software companies developing safety-critical systems are required to use rigorous safety analysis techniques to demonstrate compliance to regulatory bodies. In this paper, we describe an approach to formal verification of functional properties of requirements for an embedded real-time software written in software cost reduction (SCR)-style language using PVS specification and verification system. Key contributions of the paper include development of an automated method of translating SCR-style requirements into PVS input language as well as identification of property templates often needed in verification. Using specification for a nuclear power plant system, currently in operation, we demonstrate how safety demonstration on requirements can be accomplished while taking advantage of assurance provided by formal methods.', '2014-11-14'),
(75, 113, 'Implication of 80/20 Rule in Electronic Journal Usage of UGC-Infonet Consortia', 'This study aims to understand the implication of the 80/20 rule in large academic library consortia. INFLIBNET initiative-UGC-Infonet is the largest academic consortia initiative in Indian higher education that provides services to 414 institutional members across the country. A total of eighteen publishers and aggregators (N = 6854 journals) from COUNTER usage data for the year 2011 was collected from the INFLIBNET Centre. Cumulative download and cumulative percentage of the journals were calculated from the downloaded counts to generate a Pareto chart using Microsoft Excel 2007. The result of the study suggests that the 80/20 rule in large consortia is truly, if not precisely in conformity, when consolidated usage of journals across eighteen (18) publishers are put together at 85%/15%. Contrastingly, differences in the result are observed when titles are analyzed individually according to the publishers demonstrating a reverse of the 80/20 principle.', '2009-12-09'),
(76, 113, 'Electronic Journals: Cataloging and Management Practices in Academic Libraries', 'The growth and availability of electronic journals offer libraries the opportunity to provide end users with quick and easy access to more journals than ever before, thereby creating a complex new workload in academic libraries. Libraries have addressed the evolving challenges unique to electronic resources by creating new policies and workflows and dedicating staff to work on the processes, despite the lack of best practices. In the fall of 2009, a survey was distributed to ninety-five libraries at peer institutions to gather information about their policies and practices for cataloging and managing electronic journals in order to gauge the current status of electronic journal management among these peer institutions. This paper reports on the survey findings related to cataloging approach, sources for bibliographic records, methods for identifying problems, and the staff and staff hours dedicated to electronic journals.', '2012-12-12'),
(77, 114, 'A formal model for intellectual relationships among knowledge workers and knowledge organizations ', 'An academic learning network consists of multiple knowledge organizations and knowledge workers. The intellectual relationships can be derived from the interactions among them. In this paper, we propose a formal model to describe the interactions in an academic learning network and further provide an evaluation process to quantify intellectual relationships. Our approach is also integrated with a realistic social network platform SMNET.', '2011-04-07'),
(78, 115, 'Towards a forensic-aware database solution: Using a secured database replication protocol and transaction management for digital investigations', 'Databases contain an enormous amount of structured data. While the use of forensic analysis on the file system level for creating (partial) timelines, recovering deleted data and revealing concealed activities is very popular and multiple forensic toolsets exist, the systematic analysis of database management systems has only recently begun. Databases contain a large amount of temporary data files and metadata which are used by internal mechanisms. These data structures are maintained in order to ensure transaction authenticity, to perform rollbacks, or to set back the database to a predefined earlier state in case of e.g. an inconsistent state or a hardware failure. However, these data structures are intended to be used by the internal system methods only and are in general not human-readable.', '2013-04-07'),
(79, 116, 'Steady State Analysis of Boolean Models: A Dimension Reduction Approach', 'Boolean models have been used to study biological systems where it is of interest to understand the qualitative behavior of the system or when the precise regulatory mechanisms are unknown. A feature of especial interest of Boolean models are the states where the system is invariant over time, because they correspond to stable patterns of the biological system. Thus, finding steady states or fixed points is an important problem in computational systems biology.\n\nAlthough Boolean networks provide a strong mathematical framework, the analysis by simulation is difficult for models of large size. Thus, it is necessary to develop tools to analyze large Boolean models other than by exhaustive simulation.\n\nHere we present an approach based on dimension reduction that allows us to study large Boolean models by systematically removing nodes without changing the number of steady states.', '2011-07-07'),
(80, 116, 'Vector-Space Models for Information Retrieval', 'Research in information retrieval has followed several parallel, yet similar, developmental paths. Latent Semantic Indexing (LSI), because of the way it represents terms and documents in a term-document space, is considered a vector-space information retrieval model. In the following sections, general vector-space models as well as LSI are introduced.', '2013-08-12'),
(81, 116, 'Scoring, term weighting and the vector space model', 'Thus far we have dealt with indexes that support Boolean queries: a document either matches or does not match a query. In the case of large document\ncollections, the resulting number of matching documents can far exceed the number a human user could possibly sift through. Accordingly, it is essential\nfor a search engine to rank-order the documents matching a query. To do this, the search engine computes, for each matching document, a score with\nrespect to the query at hand. In this chapter we initiate the study of assigning a score to a (query, document) pair. This chapter consists of three main ideas.', '2014-12-09'),
(82, 116, 'Formal Foundation of Information Retrieval ', 'In any information retrieval system, retrieval is based on some formal model. Apart from a few − non-classical −\nmodels (which are relatively newer), every other model ultimately relies on two basic models: vector space, and\nprobabilistic (two early and classical models). Hence the vector space and probabilistic models are of a fundamental\nimportance, and thus a unified formal definition for them would allow for working out a unified, coherent and\nconsistent formal framework (mathematical theory), as a foundation, for IR. The paper shows that such foundations\ncan be elaborated. ', '2009-03-31'),
(83, 117, 'Incremental probabilistic Latent Semantic Analysis for video retrieval', 'Recent research trends in Content-based Video Retrieval have shown topic models as an effective tool to deal with the semantic gap challenge. In this scenario, this paper has a dual target: (1) it is aimed at studying how the use of different topic models (pLSA, LDA and FSTM) affects video retrieval performance; (2) a novel incremental topic model (IpLSA) is presented in order to cope with incremental scenarios in an effective and efficient way. A comprehensive comparison among these four topic models using two different retrieval systems and two reference benchmarking video databases is provided. Experiments revealed that pLSA is the best model in sparse conditions, LDA tend to outperform the rest of the models in a dense space and IpLSA is able to work properly in both cases.', '2014-02-09'),
(84, 117, 'A journey from normative to behavioral operations in supply chain management: A review using Latent Semantic Analysis', 'This study aims to systematically review the cross disciplinary literature covering the time period from 1934 to January 2013 on behavioral operations in supply chain in order to identify and define the taxonomy of the research on power influences in supply chain. A list of noted journals and search results from Science Direct and Web of Knowledge, IEEE Xplore, and INFORMS (approximately 11,000 journal articles) is used to prepare content collection. Latent Semantic Analysis (LSA) is applied as the review and knowledge extraction methodology. Using the text analysis and mining method we can combine statistical methods and expert human judgment to extract knowledge in the form of key latent factors. The LSA based analysis gives the study a scientific grounding which helps to overcome the subjectivity of collective opinion about the trends. This approach allows proposing taxonomy of the research on power influences in supply chain. The adopted systems approach is used to find research gaps in each class of taxonomy. An emerging trend is noticed in the research of behavioral operations in supply chain. Understanding such a scholarly structure and future trends will assist researchers to assimilate the divergent developments of this multidisciplinary research in one place. This review will be beneficial for practitioners as they consider behavioral aspects in decision making. We have also studied articles related to supply chain published in Expert Systems with Applications (ESWA) journal. We have speculated what an ESWA-related community would like to see in future publications. This will encourage researchers to explore the recommended areas and publish to these outlets.', '2013-04-06');
INSERT INTO `publication` (`pub_id`, `profile_id`, `name`, `details`, `date_time`) VALUES
(85, 118, 'Product concept evaluation and selection using data mining and domain ontology in a crowdsourcing environment', 'For product design and development, crowdsourcing shows huge potential for fostering creativity and has been regarded as one important approach to acquiring innovative concepts. Nevertheless, prior to the approach could be effectively implemented, the following challenges concerning crowdsourcing should be properly addressed: (1) burdensome concept review process to deal with a large amount of crowd-sourced design concepts; (2) insufficient consideration in integrating design knowledge and principles into existing data processing methods/algorithms for crowdsourcing; and (3) lack of a quantitative decision support process to identify better concepts. To tackle these problems, a product concept evaluation and selection approach, which comprises three modules, is proposed. These modules are respectively: (1) a data mining module to extract meaningful information from online crowd-sourced concepts; (2) a concept re-construction module to organize word tokens into a unified frame using domain ontology and extended design knowledge; and (3) a decision support module to select better concepts in a simplified manner. A pilot study on future PC (personal computer) design was conducted to demonstrate the proposed approach. The results show that the proposed approach is promising and may help to improve the concept review and evaluation efficiency; facilitate data processing using design knowledge; and enhance the reliability of concept selection decisions.', '2013-02-22'),
(86, 119, 'Formal model-driven engineering of critical information systems', 'Our society is increasingly dependent upon the behaviour of complex software systems. Errors in the design and implementation of these systems can have significant consequences. In August 2012, a ‘fairly major bug’ in the trading software used by Knight Capital Group lost that firm $461m in 45 minutes [1]. A software glitch in the anti-lock braking system caused Toyota to recall more than 400,000 vehicles in 2010 [2]. The total cost to the company of this and other software-related recalls in the same period was estimated at $3bn. In October 2008, 103 people were injured, 12 of them seriously, when a Qantas airliner dived repeatedly as the fly-by-wire software responded inappropriately to data from inertial reference sensors [3].', '2011-11-02'),
(87, 120, 'Electronic health records to support obesity-related patient care: Results from a survey of United States physicians', 'Obesity-related electronic health record functions increase the rates of measuring Body Mass Index, diagnosing obesity, and providing obesity services. This study describes the prevalence of obesity-related electronic health record functions in clinical practice and analyzes characteristics associated with increased obesity-related electronic health record sophistication.', '2014-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `publication_concepts`
--

CREATE TABLE IF NOT EXISTS `publication_concepts` (
  `pubc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `publication_id` int(11) unsigned NOT NULL,
  `concept_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`pubc_id`),
  KEY `profile_id` (`profile_id`),
  KEY `publication_id` (`publication_id`),
  KEY `concept_id` (`concept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=122 ;

--
-- Dumping data for table `publication_concepts`
--

INSERT INTO `publication_concepts` (`pubc_id`, `profile_id`, `publication_id`, `concept_id`) VALUES
(16, 72, 16, 22),
(17, 73, 17, 22),
(18, 73, 17, 28),
(19, 75, 19, 37),
(20, 76, 20, 82),
(21, 76, 21, 82),
(22, 76, 22, 82),
(23, 83, 31, 89),
(24, 90, 38, 37),
(25, 99, 47, 2),
(26, 99, 47, 6),
(27, 100, 48, 2),
(28, 100, 48, 73),
(29, 100, 49, 2),
(30, 100, 49, 73),
(31, 101, 50, 10),
(32, 101, 50, 19),
(33, 101, 51, 10),
(34, 101, 51, 15),
(35, 102, 52, 10),
(36, 102, 52, 24),
(37, 102, 53, 10),
(38, 102, 53, 24),
(39, 103, 54, 10),
(40, 103, 54, 20),
(41, 103, 54, 25),
(42, 103, 55, 10),
(43, 103, 55, 20),
(44, 103, 55, 22),
(45, 103, 55, 25),
(46, 103, 55, 85),
(47, 104, 56, 10),
(48, 104, 56, 79),
(49, 104, 57, 10),
(50, 104, 57, 19),
(51, 104, 57, 20),
(52, 104, 57, 22),
(53, 105, 58, 10),
(54, 105, 58, 12),
(55, 105, 59, 10),
(56, 105, 59, 12),
(57, 105, 59, 78),
(58, 105, 59, 94),
(59, 106, 60, 10),
(60, 106, 60, 23),
(61, 106, 61, 10),
(62, 106, 61, 23),
(63, 107, 62, 83),
(64, 107, 62, 84),
(65, 107, 62, 85),
(66, 107, 63, 85),
(67, 108, 64, 44),
(68, 108, 64, 62),
(69, 108, 65, 10),
(70, 108, 65, 62),
(71, 108, 65, 94),
(72, 109, 66, 10),
(73, 109, 66, 20),
(74, 109, 66, 22),
(75, 109, 66, 81),
(76, 109, 67, 10),
(77, 109, 67, 81),
(78, 110, 68, 45),
(79, 110, 68, 46),
(80, 110, 69, 48),
(81, 110, 69, 49),
(82, 110, 70, 48),
(83, 110, 70, 50),
(84, 111, 71, 2),
(85, 111, 71, 4),
(86, 111, 72, 2),
(87, 111, 72, 4),
(88, 111, 72, 17),
(89, 111, 72, 73),
(90, 112, 73, 40),
(91, 112, 73, 45),
(92, 112, 73, 56),
(93, 112, 73, 57),
(94, 112, 73, 58),
(95, 112, 74, 40),
(96, 112, 74, 56),
(97, 112, 74, 57),
(98, 112, 74, 58),
(99, 112, 74, 59),
(100, 113, 75, 35),
(101, 113, 76, 35),
(102, 114, 77, 26),
(103, 114, 77, 31),
(104, 115, 78, 2),
(105, 115, 78, 5),
(106, 115, 78, 72),
(107, 116, 79, 27),
(108, 116, 80, 29),
(109, 116, 81, 29),
(110, 116, 82, 26),
(111, 116, 82, 79),
(112, 117, 83, 30),
(113, 117, 83, 79),
(114, 117, 84, 30),
(115, 117, 84, 79),
(116, 118, 85, 6),
(117, 118, 85, 20),
(118, 119, 86, 26),
(119, 119, 86, 37),
(120, 119, 86, 63),
(121, 120, 87, 36);

-- --------------------------------------------------------

--
-- Table structure for table `rank_levels`
--

CREATE TABLE IF NOT EXISTS `rank_levels` (
  `rl_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rl_judgment_measure_id` int(10) unsigned NOT NULL,
  `rl_name_ar` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rl_name_en` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`rl_id`),
  KEY `rl_judgment_measure_id` (`rl_judgment_measure_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `rank_levels`
--

INSERT INTO `rank_levels` (`rl_id`, `rl_judgment_measure_id`, `rl_name_ar`, `rl_name_en`) VALUES
(1, 3, 'ممتازة', 'Excellent'),
(2, 3, 'جيدة', 'Good'),
(3, 3, 'متوسطة', 'Medium'),
(4, 3, 'ضعيفة', 'Weak'),
(5, 1, 'عال', 'High'),
(6, 1, 'متوسط', 'Medium'),
(7, 1, 'ضعيف', 'Weak'),
(10, 2, 'نظرية جداً', 'Very Theoretical'),
(11, 2, 'نسبة متزنة من الجانب النظري والتطبيقي', 'balanced percent in theoretical and practical side'),
(13, 2, 'تطبيقية جدا', 'Very Practical'),
(14, 4, 'ذات تطبيقات مهمة', 'Has important applications'),
(15, 4, 'ذات تطبيقات جديرة بالاهتمام', 'Has worthwhile applications'),
(16, 4, 'متوسطة الأهمية', 'Medium importance'),
(17, 4, 'ضعيفة بدون تطبيقات واضحة', 'Weak and without clear applications'),
(18, 5, 'المفاهيم النظرية جديدة، والنتائج جديدة', 'Has a new theoretical concepts and new results'),
(19, 5, 'المفاهيم النظرية معروفة، والنتائج جديدة', 'Has known theoretical concepts and new results'),
(20, 5, 'المفاهيم النظرية معروفة، والنتائج معروفة', 'Has known  theoretical concepts and known results'),
(21, 5, 'مفاهيم النظرية معروفة، والنتائج غريبة', 'Has known theoretical concepts and strange results'),
(22, 5, 'المفاهيم النظرية غريبة، والنتائج غريبة', 'Has strange theoretical concepts and strange results'),
(23, 6, 'ممتازة', 'Excellent'),
(24, 6, 'جيدة', 'Good'),
(25, 6, 'متوسطة', 'Medium'),
(26, 6, 'ضعيفة', 'Weak'),
(27, 7, 'نعم', 'Yes'),
(28, 7, 'لا', 'No'),
(29, 8, 'ممتاز', 'Excellent'),
(30, 8, 'جيد', 'Good'),
(31, 8, 'متوسط', 'Medium'),
(32, 8, 'ضعيف', 'Weak'),
(33, 9, 'عدد الصفحات مناسب دون حشو', 'Number of pages is suitable and has no padding'),
(34, 9, 'عدد الصفحات غير مناسب (ثمة حشو فيها)', 'Number of pages is not suitable and has padding'),
(35, 9, 'غير متأكد', 'Not sure'),
(36, 10, 'جيدة', 'Good'),
(37, 10, 'متوسطة', 'Medium'),
(38, 10, 'ضعيفة', 'Weak'),
(39, 11, 'ممتازة', 'Excellent'),
(40, 11, 'جيدة', 'Good'),
(41, 11, 'متوسطة', 'Medium'),
(42, 11, 'ضعيفة', 'Weak'),
(43, 12, 'نعم', 'Yes'),
(44, 12, 'لا', 'No'),
(45, 13, 'دراسة مرجعية', 'Referential study'),
(46, 13, 'بحث علمي أصيل', 'Original scientific research');

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `rate_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rate_user_id` int(11) unsigned NOT NULL,
  `rate_paper_id` bigint(20) unsigned NOT NULL,
  `rate_count` int(11) NOT NULL,
  PRIMARY KEY (`rate_id`),
  KEY `IXFK_rate_Paper` (`rate_paper_id`),
  KEY `IXFK_rate_users` (`rate_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `role_privilage`
--

CREATE TABLE IF NOT EXISTS `role_privilage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_role` mediumint(8) unsigned NOT NULL,
  `id_privilage` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_role` (`id_role`),
  KEY `id_privilage` (`id_privilage`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `role_privilage`
--

INSERT INTO `role_privilage` (`id`, `id_role`, `id_privilage`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 5),
(4, 1, 7),
(5, 1, 10),
(6, 1, 11),
(7, 1, 12),
(8, 1, 13),
(9, 1, 16),
(10, 1, 17),
(14, 2, 14),
(15, 2, 15),
(16, 2, 17),
(20, 3, 17),
(22, 2, 3),
(23, 2, 5),
(24, 3, 9),
(25, 3, 8),
(26, 3, 5),
(29, 1, 32),
(30, 2, 6),
(31, 2, 4),
(32, 2, 33);

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE IF NOT EXISTS `specialization` (
  `spe_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned NOT NULL,
  `specializaty_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`spe_id`),
  KEY `specializaty_id` (`specializaty_id`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

--
-- Dumping data for table `specialization`
--

INSERT INTO `specialization` (`spe_id`, `profile_id`, `specializaty_id`) VALUES
(14, 71, 44),
(15, 72, 22),
(16, 73, 44),
(17, 74, 2),
(18, 75, 37),
(19, 76, 82),
(20, 77, 44),
(21, 77, 37),
(22, 78, 37),
(23, 79, 13),
(24, 80, 37),
(25, 80, 44),
(26, 81, 44),
(27, 82, 44),
(28, 83, 13),
(29, 84, 13),
(30, 85, 44),
(31, 86, 44),
(32, 87, 44),
(33, 88, 44),
(34, 89, 44),
(35, 90, 2),
(36, 91, 44),
(37, 92, 44),
(38, 93, 44),
(39, 94, 44),
(40, 95, 2),
(41, 96, 44),
(42, 97, 44),
(43, 97, 13),
(44, 98, 44),
(45, 98, 13),
(46, 99, 2),
(47, 100, 2),
(48, 101, 10),
(49, 102, 10),
(50, 103, 10),
(51, 104, 10),
(52, 105, 10),
(53, 106, 10),
(54, 107, 83),
(55, 108, 13),
(56, 108, 77),
(57, 109, 13),
(58, 109, 81),
(59, 110, 45),
(60, 111, 2),
(61, 111, 4),
(62, 112, 44),
(63, 113, 32),
(64, 114, 10),
(65, 115, 2),
(66, 116, 10),
(67, 116, 26),
(68, 117, 10),
(69, 118, 6),
(70, 119, 10),
(71, 119, 37),
(72, 120, 32);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `st_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `st_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `st_name_en` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `st_name_ar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`st_id`, `st_name`, `st_name_en`, `st_name_ar`) VALUES
(2, 'pending', 'Pending', 'في الانتظار'),
(3, 'wait_for_assigning', 'Wait For Assigning', 'في انتظار الإسناد'),
(4, 'assigned', 'Assigned', 'تم الإسناد'),
(5, 'need_editing', 'Need editing', 'بحاجة لتعديل'),
(6, 'ready_to_publish', 'Ready to publish', 'جاهزة للنشر'),
(7, 'rejected', 'Rejected', 'مرفوضة');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_id` (`profile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `profile_id`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1437774369, 1, 'Admin', 'istrator', 'ADMIN', '0', NULL),
(4, '::1', 'michaelzarozinski', '$2y$08$jRou5UKLY2bWfM.u6dXROeFzGrfHZaO3NGa.GJJx4IshUi8pu5NkS', 'A5ylf3kVgC492fRpkthSMu', 'michael@gmail.com', NULL, NULL, NULL, NULL, 1437393693, 1437685712, 1, 'Michael', 'Zarozinski', 'syria', '09923567819', 71),
(5, '::1', 'deanallemang', '$2y$08$G/ukN8zOZA2ndYduFIOiKeTm9KfV6ooCRvHtMYD0X2VcYBQPHjgga', 'xzYMlKMRKT6ZdLhKTxDz7e', 'dean@gmail.com', NULL, NULL, NULL, NULL, 1437394028, 1437394054, 1, 'Dean', 'Allemang', 'syria', '0998654837', 72),
(6, '::1', 'abhishek narolia', '$2y$08$2sKhAFbgDzvxP9DR1q1M2ujmc28I/x958O.loCpLFGAE/Kbh1T2A2', 'aP2WySpgp9vqHVDa6MK/aO', 'abhishek@gmail.com', NULL, NULL, NULL, NULL, 1437394349, 1437394366, 1, 'Abhishek ', 'Narolia', 'syria', '0987864678', 73),
(7, '::1', 'ahmadalshikh saleh', '$2y$08$HvLbz57FGcD.hfdbqEgQveB1F4t4dMxdp4TccXmMaAscZzDurfDUa', 'LAtHleuXig3qCKWXX4.ize', 'ahmad@gmail.com', NULL, NULL, NULL, NULL, 1437394606, 1437561488, 1, 'Ahmad', 'Alshikh Saleh', 'syria', '0998765463', 74),
(8, '::1', 'mohammadhassan', '$2y$08$OS6hpNdEjg.iLwhyq3QxiOtwweL35bKBqa.ImnxnZtvlUJ6aBq8jK', 'iz/PYtAVUulWuBIpdZDC2e', 'mhd_hassan@gmail.com', NULL, NULL, NULL, NULL, 1437395022, 1437395043, 1, 'Mohammad', 'Hassan', 'syria', '0998753212', 75),
(9, '::1', 'olaabo amsha', '$2y$08$ErPRF2QBCjrigB76tl2xI.MXi0Y1yo4DXSjx7u7XmFDgxbLl.An2K', 'YKh.gxnYJ8ssPaI82daJre', 'olaboamsh@agmail.com', NULL, NULL, NULL, NULL, 1437395266, 1437396795, 1, 'Ola', 'Abo Amsha', 'syria', '0998645327', 76),
(10, '::1', 'rihamnasleh', '$2y$08$6RbLjXnlTmByxRYL.nnhme4D1lDAyJh.Amacba9jMq1YBu0kdrG2m', 'pGkS6Oa6i/bVAriWMDT1We', 'nasleh@gmail.com', NULL, NULL, NULL, NULL, 1437397101, 1437397120, 1, 'Riham', 'Nasleh', 'syria', '0991234356', 77),
(11, '::1', 'omardahan', '$2y$08$Rkcl7T6BETrWzkGGktYgfOKRPPcBxKRWv1vgCu17VXUu0HfsbOMBy', 'qGXHStDJUKzA0xzP/1zPn.', 'omer@gmail.com', NULL, NULL, NULL, NULL, 1437397327, 1437571232, 1, 'Omar', 'Dahan', 'syria', '0998546783', 78),
(12, '::1', 'fahdsheraz', '$2y$08$.qT40r0lbOaM8tfyUvzZbu38l6oemo0IF6Eere2aqPs5xGhMHYW8C', 'fEl2999q8vgH3DrFs1Tese', 'sheraz@gmail.com', NULL, NULL, NULL, NULL, 1437397578, 1437397594, 1, 'Fahd', 'Sheraz', 'syria', '099745362', 79),
(13, '::1', 'ahmadabo hasna', '$2y$08$CUevy6m596qWzEwVbZf7FeULzhgy1uk2YUAwMQ2Pgk7yGljDMmyvi', 'OrOE/aCbx21/.0Swydo7g.', 'a_abohasna@gmail.com', NULL, NULL, NULL, NULL, 1437420427, 1437420438, 1, 'Ahmad', 'Abo Hasna', 'syria', '0998456388', 80),
(14, '::1', 'aamerghiyath hammamiah ', '$2y$08$r9Der1CRZH.fsrJwlCgJQuXhOWU40MnC3u4YUz8Uvohs9wC73BS4S', 'EN5LFJ8tgZKoZtfw.VZb0O', 'aamer@gmail.com', NULL, NULL, NULL, NULL, 1437420812, 1437420836, 1, 'Aamer', 'Ghiyath Hammamiah ', 'syria', '0998877632', 81),
(15, '::1', 'ibrahimalfaili', '$2y$08$msZ6DxHqgF3CmjE5YWqu7eV/Wnfw1W8.QVXjxEqCHJQOLlYFpUM7u', 'N5fmuVAH7HCvPPlsgU8mbO', 'ibrahim@gmail.com', NULL, NULL, NULL, NULL, 1437421088, 1437421107, 1, 'Ibrahim', 'alfaili', 'syria', '0998245621', 82),
(16, '::1', 'mohammadtahhan', '$2y$08$bJKdI2Tt.oTB04qFA5DmpedqYjXaiKLP..hFfi.VaBz96Ag7ttHvO', '69yfgaU1PppMvf6f5o0J7.', 'tahhan@gmail.com', NULL, NULL, NULL, NULL, 1437421406, 1437421429, 1, 'Mohammad', 'Tahhan', 'syria', '0981234562', 83),
(17, '::1', 'mohammadbassel khallouf ', '$2y$08$3eYxpO183qjexbnmPJem.uW1pLogZwW1YWzENcMpVokjIR7cg9ste', '1leyuky88vY4EMlf58gVGO', 'mhd_bassel@gmail.com', NULL, NULL, NULL, NULL, 1437421679, 1437649154, 1, 'Mohammad', 'Bassel Khallouf ', 'syria', '0997456321', 84),
(18, '::1', 'anassalman ', '$2y$08$0wwlFKcjHtJeXXNYm.44i.Mcf.lZwMyisatDHD2GfYBLFgPWiMLIq', 'vhuRII/cUONHy1LWV1CneO', 'a_salman@gmail.com', NULL, NULL, NULL, NULL, 1437421916, 1437421934, 1, 'Anas', 'salman ', 'syria', '0954456783', 85),
(19, '::1', 'hussankazah ', '$2y$08$Ruije4kePpfchWHiRWP0zuJz4Mi5B7.DQguye5qrWAqj/BAJtE.Nu', 'L/7FoVYthW5QW/8TkkRX/.', 'h_kazah@gmail.com', NULL, NULL, NULL, NULL, 1437422237, 1437649437, 1, 'Hussan', 'Kazah ', 'syria', '0998655748', 86),
(20, '::1', 'imadtotanji ', '$2y$08$/lG8Db5paFHucczhE2Oxnegiy4HXpE6/nBZWSfI5YH1dKFR0U6rBm', 'QypyElm5sTvmea73qlKodO', 'itotanji@gmail.com', NULL, NULL, NULL, NULL, 1437422392, 1437649359, 1, 'Imad', 'Totanji ', 'syria', '0998443241', 87),
(21, '::1', 'mahmoud aljaroud', '$2y$08$lTAn.esUFm3KVjkschWZtOaDrNHEhmv10iVwxQdj9ZepI7GR1CddG', 'gyQqLJd.Ly6PBbpgdOmni.', 'aljaroud@gmail.com', NULL, NULL, NULL, NULL, 1437422651, 1437649261, 1, 'Mahmoud ', 'Aljaroud', 'syria', '0944567831', 88),
(22, '::1', 'mohammadali', '$2y$08$CKtQoyUlG8RnmNy0LLmGsuvvPncwL96Ffsglcyf3dxOX86mwaU6Y6', 'yOdBm1R6K0H3ZrD1QSz3PO', 'mhd_ali@gmail.com', NULL, NULL, NULL, NULL, 1437422829, 1437422881, 1, 'Mohammad', 'Ali', 'syria', '0998456783', 89),
(23, '::1', 'ahmadalshantout', '$2y$08$36rB2dSj4mhm5gDcBhGALezFw9qUoOiwad8SYJx7ZwErxNi0lYF/K', 'j65Wo5nw3KJdIHq8N5xLqe', 'alshantout@gmail.com', NULL, NULL, NULL, NULL, 1437423235, 1437423254, 1, 'Ahmad', 'Alshantout', 'syria', '0944567831', 90),
(24, '::1', 'mahersafadi', '$2y$08$k/5pdd26Mn6y2X7fl/XZOuvFcWXY/zqKTvx9HZw2yvKo67.J0aA9e', 'JaKrQDBDXxa2PZF6qKJxx.', 'm_safadi@gmail.com', NULL, NULL, NULL, NULL, 1437423910, 1437423960, 1, 'maher', 'Safadi', 'syria', '0996453672', 91),
(25, '::1', 'saeedel-dah', '$2y$08$P5t801GuqH29J3llGNYJxeDxUaX4a01ijnU5l9TdPdO5WEQDGQqAe', 'cJr2bF/b2gwu8jM0kUJ6wO', 'saeed_dah@gmail.com', NULL, NULL, NULL, NULL, 1437424135, 1437424165, 1, 'Saeed', 'EL-Dah', 'syria', '0997456321', 92),
(26, '::1', 'ghiath zarzar', '$2y$08$7l5Pht6BaxswqSa6Gp722.860pQ11HGg2BL33nJDMH2D5XYKBMnze', 'akZNG0slRE/Vj7y3m2Ryde', 'ghiath@gmail.com', NULL, NULL, NULL, NULL, 1437424653, 1437424686, 1, 'Ghiath ', 'Zarzar', 'syria', '0996456372', 93),
(27, '::1', 'alaawahbah', '$2y$08$KHQ16AiT7N42gKsXqmpX7.9XZHuBmANYfvYbsInlav/3jOH83AFgm', '.86HlN62aRXnxL8pPUZd9O', 'ghiath_w@gmail.com', NULL, NULL, NULL, NULL, 1437424962, 1437425001, 1, 'Alaa', 'Wahbah', 'syria', '0996478352', 94),
(28, '::1', 'hussainrafee', '$2y$08$bFBZA13t3/P9beS2jFcvtu/qOcIbiR1XbHGMoSqmpvz0mwO4ttOom', 'qdLaJNucq3fENcHvyglcQe', 'hussain@gmail.com', NULL, NULL, NULL, NULL, 1437427367, 1437648957, 1, 'Hussain', 'Rafee', 'syria', '0997564732', 95),
(29, '::1', 'khaleltenawi', '$2y$08$dkXup33KxwX7kW/jvKgNTu7nArErVB1YdCrndDmF3O471blOTbz6q', 'uogARwmnMnp5arnZyy7gge', 'khalel@gmail.com', NULL, NULL, NULL, NULL, 1437427531, 1437427545, 1, 'Khalel', 'Tenawi', 'syria', '0993322156', 96),
(30, '::1', 'alaawahab', '$2y$08$y8rH8NuWuITq49AZfIpw1.4gaVL6zrb3d0lNWYslKJfS8N0QKq3Nm', 'gzuWZKIFrihyXqlyF5RU0O', 'wahab@gmail.com', NULL, NULL, NULL, NULL, 1437427673, 1437427693, 1, 'Alaa', 'Wahab', 'syria', '0998875674', 97),
(31, '::1', 'tawfeekkaekaty', '$2y$08$lCvafyWZu2X4eYSzrYBbRe6ASYY2qSHDyKyG5lQluKrzF.eNJ0p.C', 'EfEBf.IHaYWPiSFmqL/SQe', 'tawfeek@gmail.com', NULL, NULL, NULL, NULL, 1437427910, 1437429140, 1, 'Tawfeek', 'Kaekaty', 'syria', '0998567843', 98),
(32, '::1', 'mhdyasser koziz', '$2y$08$55YsHlQSKrPy4G9C2HLxGuhm0Ddb/IGijJ3orsKe2C.YUzwF/1hCW', 'LqolQRSonwui87TBsmJabu', 'koziz@gmail.com', NULL, NULL, NULL, NULL, 1437429335, 1437477968, 1, 'MHD', 'Yasser Koziz', 'syria', '0998652231', 99),
(33, '::1', 'birgittahauser', '$2y$08$HrU2/jELkB/tphWHYCfaGesf2EIErnxKEmULVHUq90mfalhuDb//m', 'Z6JcdINNdlYOL8tNzNDAIO', 'hauser@gmail.com', NULL, NULL, NULL, NULL, 1437430107, 1437430130, 1, 'Birgitta', 'Hauser', 'Germane', '09998756435', 100),
(34, '::1', 'achilleasportarinos', '$2y$08$e5wToxG/ZmLSKrXNymFAfe4qgxep.UBXtgrl9NeMZI/VRRn4Uxz6S', '6Bg8gX5GTDJVZ7yXOnSoYu', 'portarinos@gmail.com', NULL, NULL, NULL, NULL, 1437430481, 1437430507, 1, 'Achilleas', 'Portarinos', 'syria', '0994563455', 101),
(35, '::1', 'markgritter', '$2y$08$dpz78m38bkhLZDMPqbNcyOa.68NFm3m1x99xRssXfXFLXDkO/ZFRi', 'QczYbmHHgTkZ08B.n0794.', 'm.gritter@gmail.com', NULL, NULL, NULL, NULL, 1437430801, 1437430815, 1, 'Mark', 'Gritter', 'Germane', '0998564352', 102),
(36, '::1', 'jeffallen', '$2y$08$T5rNhsuhPGg3e5LzZXGoOezdd9vBMnuq6HG8eb36VKBsHYIsWaVE6', 'PKGgcmsTE0MMoU3D8Dmy6.', 'jeff.allen@gmail.com', NULL, NULL, NULL, NULL, 1437431138, 1437431155, 1, 'Jeff', 'Allen', 'American', '0996453456', 103),
(37, '::1', 'paulgarcia', '$2y$08$VP3UM/haJI1alMzE3wxuYekIBlksXf.yCizjfGo9aZUaX6Z4H/zsG', 'kUoXG8kQ..Ef70yU30sQaO', 'p.garcia@gmail.com', NULL, NULL, NULL, NULL, 1437431377, 1437562263, 1, 'Paul', 'Garcia', 'American', '0995477895', 104),
(38, '::1', 'patriciaphillips', '$2y$08$EJFns3EjNaI53jzn8H1Pwu3ZbF.VbvN/WYeQcb5meFH5ixSxYWwNC', 'sYdbEYGVLgIUH8KUl6Wvee', 'phillips@hotmail.com', NULL, NULL, NULL, NULL, 1437431656, 1437431690, 1, 'Patricia', 'Phillips', 'American', '0994567346', 105),
(39, '::1', 'lisacollins', '$2y$08$/iAm2y9j41NJsQyprvilpe69eyOXSqWl7fax8CDqYBRSsF/WZh8Ym', 'eZackDCuQXgNgptyn7IxfO', 'lisa.col78@yahoo.com', NULL, NULL, NULL, NULL, 1437431905, 1437431917, 1, 'Lisa', 'Collins', 'Germane', '09954678654', 106),
(40, '::1', 'kevinadams', '$2y$08$CF42P/KqGX2kOJ0WqOSaiupLrshRdhmgEn3hu9Iwi0m/0Tf0z5AFm', 'lRusLfz9fKHQIAx3/cy1ke', 'k.adams@gmail.com', NULL, NULL, NULL, NULL, 1437432122, 1437432138, 1, 'Kevin', 'Adams', 'Egypt', '0997654667', 107),
(41, '::1', 'jamesmartin', '$2y$08$X6ZDDrJchYl.jKa9RNM6veZXBdPDijjeZXS1V5Cad9QarHDWFti7i', 'M8MxNtiYgFslmcCPzooY2O', 'martin@gmail.com', NULL, NULL, NULL, NULL, 1437432351, 1437432382, 1, 'James', 'Martin', 'syria', '09945637895', 108),
(42, '::1', 'ronaldclark', '$2y$08$pdrUa/FbS/ZYEmVZmQPHeecSyuNCi3v/BadeaI7Nywr90Ler0RM1K', 'rUmBkzmNjqJERXir3QmjFu', 'r.clark@tum.de', NULL, NULL, NULL, NULL, 1437432607, 1437432637, 1, 'Ronald', 'Clark', 'syria', '09945667883', 109),
(43, '::1', 'barbraparker', '$2y$08$mtjGuw6pL7abk3QpTNzcT.suY4pljuLFACaHeMXr6hCBqSH03AzBm', 'wZOtbE4fgCng9R5uKS6EYe', 'b.parker@stanford.ed', NULL, NULL, NULL, NULL, 1437432791, 1437432813, 1, 'Barbra', 'Parker', 'syria', '0995667843', 110),
(44, '::1', 'lindahill', '$2y$08$Frxcdqfrj.nNAtaCOFIgAevgMoH6ipRGHy8xi4EwlM.IoyRDXx1ra', 'x1LV2H.hR/izoRaf7lz9AO', 'hill.linda@gmail.com', NULL, NULL, NULL, NULL, 1437432999, 1437555600, 1, 'Linda', 'Hill', 'American', '0995678453', 111),
(45, '::1', 'karengarcia', '$2y$08$JYmr7Jnu.OCe3QXTqPWea.5ReBB2iyMHeBXdX4blzTp4LiON9a9h6', '2LoZI8IoWXaZ0Teax90Xye', 'k.garcia@gmail.com', NULL, NULL, NULL, NULL, 1437433289, 1437433305, 1, 'Karen', 'Garcia', 'American', '0994567837', 112),
(46, '::1', 'ronaldking', '$2y$08$Q77xkHNueRq6rOyZRQTgO.amfxx/okhkVPHss6YWsXN2w8uWFNvXq', 'GLD.y5a3ypVo221d3bCEKe', 'r.king87@gmail.com', NULL, NULL, NULL, NULL, 1437433532, 1437433550, 1, 'Ronald', 'King', 'American', '09923567815', 113),
(47, '::1', 'dianawhilte', '$2y$08$R3Yl6L0JtjWryUESQsKLG.TnbdyWSif4Gkq.wrh7teTNC6CGwTb02', 'xq4rWkn0GvLCuEW5y.xyBe', 'd.white@gamil.com', NULL, NULL, NULL, NULL, 1437433818, 1437433831, 1, 'Diana', 'Whilte', 'Germane', '0997778895', 114),
(48, '::1', 'donaldbrown', '$2y$08$ug.Z0pFgu/dDBOA6O7EQB.7uCyicxx8zIRAig4XbpmDU7J5IASZYG', 'RpWvtfD/MXjGtR5EdPTIHO', 'd.brown@hotmail.com', NULL, NULL, NULL, NULL, 1437434080, 1437637238, 1, 'Donald', 'Brown', 'Germane', '09986756489', 115),
(49, '::1', 'ronaldthompson', '$2y$08$UJXF0p2DiBWnHATLIlKuruqnZD.dHfQkjaSuBOBerrdwTp5fdQ8Qq', 'StLFgdVSXdR7UbCfm8xwN.', 'thompson@harvard.ed', NULL, NULL, NULL, NULL, 1437434292, 1437685649, 1, 'Ronald', 'thompson', 'American', '0996578465', 116),
(50, '::1', 'michaeljohnson', '$2y$08$ewFj3T0tO8AS6765YNErouyISZAAHH.umRe.rTf1YMTySZs09OKdi', 'i1v2VoHrXvWX1Tb0GsoH/.', 'michael@hotmail.com', NULL, NULL, NULL, NULL, 1437434480, 1437685585, 1, 'Michael', 'Johnson', 'Germane', '0995678467', 117),
(51, '::1', 'lindawright', '$2y$08$hokWu/bOBRdLGf3WDD.GHujaUYNoryftqyprqIrhe60Zr2QTEiAaa', 'R2j1VXHeSIKjqibtOsWt5e', 'wright.l@freiburg.ed', NULL, NULL, NULL, NULL, 1437434643, 1437685751, 1, 'Linda', 'Wright', 'syria', '0997686574', 118),
(52, '::1', 'nancyroberts', '$2y$08$FWArc/Z5oXAOVP8x1eyBqe8eyqib7awrWyDM9tf5ieJM54fpi7/TS', 'kXdJLfORwZy7T/gPznz88u', 'n.roberts@gmail.com', NULL, NULL, NULL, NULL, 1437434830, 1437434842, 1, 'Nancy', 'Roberts', 'syria', '0996778956', 119),
(53, '::1', 'jasonmartin', '$2y$08$2PEMT/Txv.gK9ASjFisYQ.Tq5AbV6zQ5wLAqKWDKVTACGqTirDAr.', 'YwvDszpbJ0qj4.gfWf/.DO', 'martin1@gmail.com', NULL, NULL, NULL, NULL, 1437434991, 1437751092, 1, 'Jason', 'Martin', 'American', '0997589467', 120),
(54, '::1', 'opadaeng', '$2y$08$ITO07X6m1nhDbCNmhg1vkeChL.Du7YzrP4EXrN5S8pMeFvjLVVnSe', 'yKNKgNgiRIgUjtCjnY7dhO', 'mm@mm.co', NULL, NULL, NULL, NULL, 1437491199, 1437491539, 1, 'opada', 'eng', 'opada', '12515753156', NULL),
(55, '::1', 'mariamkassar', '$2y$08$Z9AFvZpGlW0SlNyUX.XYyOd.bdTobQcbhjIMQ50XUg3WfuBjKl0OC', 'MeU307FbF3oZlLFGwPi2hO', 'mariam@gmail.com', NULL, NULL, NULL, NULL, 1437504701, 1437751125, 1, 'mariam', 'kassar', 'syria', '09923567819', NULL),
(56, '::1', 'mhdshalabi', '$2y$08$Y2NB0ihzeRvXPHcW.w4X9OQ.rBeJEMckzNomr8r82aGbDv7Us7oUO', 'B05vCHeZv.NpCdRVvp.k8e', 'm_shalabi@gmail.com', NULL, NULL, NULL, NULL, 1437640351, 1437641683, 1, 'Mhd', 'Shalabi', 'syria', '099856784', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(8, 4, 3),
(10, 5, 3),
(12, 6, 3),
(114, 7, 3),
(16, 8, 3),
(19, 9, 3),
(21, 10, 3),
(115, 11, 3),
(25, 12, 3),
(27, 13, 3),
(29, 14, 3),
(31, 15, 3),
(33, 16, 3),
(117, 17, 3),
(37, 18, 3),
(120, 19, 3),
(119, 20, 3),
(118, 21, 3),
(45, 22, 3),
(47, 23, 3),
(49, 24, 3),
(51, 25, 3),
(53, 26, 3),
(55, 27, 3),
(57, 28, 3),
(59, 29, 3),
(61, 30, 3),
(63, 31, 3),
(65, 32, 3),
(67, 33, 3),
(69, 34, 3),
(71, 35, 3),
(73, 36, 3),
(75, 37, 3),
(77, 38, 3),
(79, 39, 3),
(81, 40, 3),
(83, 41, 3),
(85, 42, 3),
(87, 43, 3),
(113, 44, 3),
(91, 45, 3),
(95, 46, 3),
(97, 47, 3),
(99, 48, 3),
(101, 49, 3),
(103, 50, 3),
(105, 51, 3),
(107, 52, 3),
(109, 53, 3),
(111, 54, 2),
(112, 55, 2),
(116, 56, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_paper`
--
ALTER TABLE `assigned_paper`
  ADD CONSTRAINT `assigned_paper_ibfk_1` FOREIGN KEY (`ap_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assigned_paper_ibfk_3` FOREIGN KEY (`ap_judge_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`com_status`) REFERENCES `status` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`com_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_comment_Paper` FOREIGN KEY (`com_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `concept`
--
ALTER TABLE `concept`
  ADD CONSTRAINT `concept_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_concepts`
--
ALTER TABLE `course_concepts`
  ADD CONSTRAINT `course_concepts_ibfk_3` FOREIGN KEY (`concept_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_concepts_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_concepts_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `judgement_result`
--
ALTER TABLE `judgement_result`
  ADD CONSTRAINT `judgement_result_ibfk_1` FOREIGN KEY (`jr_final_result_id`) REFERENCES `final_result` (`fr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `judgement_result_ibfk_3` FOREIGN KEY (`jr_judge_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `judgement_result_ibfk_4` FOREIGN KEY (`jr_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `measure_result`
--
ALTER TABLE `measure_result`
  ADD CONSTRAINT `measure_result_ibfk_1` FOREIGN KEY (`mr_measure_id`) REFERENCES `judgement_measure` (`jm_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `measure_result_ibfk_2` FOREIGN KEY (`mr_rank_level_id`) REFERENCES `rank_levels` (`rl_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `measure_result_ibfk_3` FOREIGN KEY (`mr_judgement_result_id`) REFERENCES `judgement_result` (`jr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`not_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notification_ibfk_3` FOREIGN KEY (`not_user_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paper`
--
ALTER TABLE `paper`
  ADD CONSTRAINT `paper_ibfk_1` FOREIGN KEY (`paper_status`) REFERENCES `status` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paper_ibfk_2` FOREIGN KEY (`paper_added_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paper_concepts`
--
ALTER TABLE `paper_concepts`
  ADD CONSTRAINT `paper_concepts_ibfk_1` FOREIGN KEY (`pc_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paper_concepts_ibfk_2` FOREIGN KEY (`pc_concept_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paper_history`
--
ALTER TABLE `paper_history`
  ADD CONSTRAINT `paper_history_ibfk_1` FOREIGN KEY (`ph_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paper_history_ibfk_2` FOREIGN KEY (`ph_paper_edited_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paper_history_ibfk_3` FOREIGN KEY (`ph_paper_status`) REFERENCES `status` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `privilage`
--
ALTER TABLE `privilage`
  ADD CONSTRAINT `privilage_ibfk_1` FOREIGN KEY (`id_cat`) REFERENCES `priv_cat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile_final_result`
--
ALTER TABLE `profile_final_result`
  ADD CONSTRAINT `profile_final_result_ibfk_1` FOREIGN KEY (`concept_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profile_final_result_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_concepts`
--
ALTER TABLE `project_concepts`
  ADD CONSTRAINT `project_concepts_ibfk_3` FOREIGN KEY (`concept_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_concepts_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_concepts_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `publication_concepts`
--
ALTER TABLE `publication_concepts`
  ADD CONSTRAINT `publication_concepts_ibfk_3` FOREIGN KEY (`concept_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `publication_concepts_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `publication_concepts_ibfk_2` FOREIGN KEY (`publication_id`) REFERENCES `publication` (`pub_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rank_levels`
--
ALTER TABLE `rank_levels`
  ADD CONSTRAINT `rank_levels_ibfk_1` FOREIGN KEY (`rl_judgment_measure_id`) REFERENCES `judgement_measure` (`jm_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`rate_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rate_ibfk_2` FOREIGN KEY (`rate_paper_id`) REFERENCES `paper` (`paper_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_privilage`
--
ALTER TABLE `role_privilage`
  ADD CONSTRAINT `role_privilage_ibfk_2` FOREIGN KEY (`id_privilage`) REFERENCES `privilage` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_privilage_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `specialization`
--
ALTER TABLE `specialization`
  ADD CONSTRAINT `specialization_ibfk_2` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `specialization_ibfk_1` FOREIGN KEY (`specializaty_id`) REFERENCES `concept` (`concept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
