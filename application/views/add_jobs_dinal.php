<html>
	<head>
		<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/bootstrap.min.css">
		<script src="<?php echo base_url();?>files/public/js/jquery-2.1.0.min.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>
		
		<script type="text/javascript">
			function displayRes1(item, val, text) {
			    $('#selecteditems1').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" class="pub_activeinput" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo1').val('');
			}
			function displayRes2(item, val, text) {
			    $('#selecteditems2').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo2').val('');
			}
			function displayRes3(item, val, text) {
			    $('#selecteditems3').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo3').val('');
			}
			function removeparent(e){
				e.parentNode.remove();
				var p = parseInt(e.parentNode.lastChild.textContent);
			}
			$(document).ready(function () {
				var jsonData = <?php echo $concept;?>;
				src = []; 
				for (var i = 0; i < jsonData.length; i++) {
					src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
				}
			    $('#demo1').typeahead({
			        source:src,
			        itemSelected: displayRes1
			    });
			   $('#demo2').typeahead({
			        source:src,
			        itemSelected: displayRes2
			    });
			    $('#demo3').typeahead({
			        source:src,
			        itemSelected: displayRes3
			    });
			    var all_publication = {};
				var i = 0;
				var data = {};
			    
			    var publications = {};
			    $('#add_new_publications').click(function(){
					$(".pub_activeinput").each(function() {
						var obj = $(this),name = obj.attr('name'),value = obj.val();
						if($(obj).is(':hidden')) {data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = data;
					all_publication[i] = publications; i++;
					$('#selecteditems1').html('');
					$('.pub_activeinput').attr('value','');
					//console.log(all_publication);
			    });
			    $('form#add').on('submit', function(){
			    	
					$(".pub_activeinput").each(function() {
						var obj = $(this),name = obj.attr('name'),value = obj.val();
						if($(obj).is(':hidden')) {data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = JSON.stringify(data);
					all_publication[i] = publications; i++;
					$('#selecteditems1').html('');
				    $.ajax({ 
					      type: "POST",
					      url: '<?php echo base_url();?>index.php/profile/add_jobs',
					      datatype: "json",
					      traditional: true,
					      data: {json:JSON.stringify(all_publication)}, 
					      success: function (response) {
					      	console.log(response);
					      	$('.all').html('');
					      }          
					 });
					 return false; //disable refresh
				});

					
			});
		</script>
	</head>
	<body>
		<?php echo form_open('profile/add_jobs','id= "add"'); ?>
		<!------------------------------------------------------------>
		<!--------------------------Publications---------------------->
		<!------------------------------------------------------------>
		<h4>Publications</h4>
		<hr />
		<div class="all">
			<div class="input_fields_publication">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Publication Name</label>
							<?php echo form_input('pub_name',set_value('pub_name'),'class="pub_activeinput"');?><br />
							<label>Publication Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pub_domain',set_value('pub_domain'),'id="demo1" class="pub_activeinput"'); ?><br />
							</div>
							
							<label>Publication Details</label><br />
							<textarea name="pub_details" class="pub_activeinput" rows="4"></textarea><br />
							<label>Publication Date</label>
							<input type="date" name="pub_date" class="pub_activeinput" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems1">
								
								</tbody>
							</table>
						</div>
						
					</div>
					<a href="#" id="add_new_publications">Add New Publications</a>
				</div>
				
			</div>
			<!------------------------------------------------------------>
			<!--------------------------Projects-------------------------->
			<!------------------------------------------------------------
			<hr />
			<h4>Projects</h4>
			<hr />
			<div class="input_fields_Projects">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Project Name</label>
							<?php echo form_input('pro_name',set_value('pro_name'),'');?><br />
							<label>Project Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pro_domain',set_value('pro_domain'),'id="demo2"'); ?><br />
							</div>
							
							<label>Project Details</label><br />
							<textarea name="pro_details" rows="4"></textarea><br />
							<label>Project Date</label>
							<input type="date" name="pro_date" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems2">
								
								</tbody>
							</table>
						</div>
						
					</div>
					
				</div>
				
			</div>
			------------------------------------------------------------>
			<!--------------------------Courses--------------------------->
			<!-----------------------------------------------------------
			<hr />
			<h4>Courses</h4>
			<hr />
			<div class="input_fields_Courses">
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<label>Course Name</label>
							<?php echo form_input('cour_name',set_value('cour_name'),'');?><br />
							<label>Course Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('cour_domain',set_value('cour_domain'),'id="demo3"'); ?><br />
							</div>
							
							<label>Course Details</label><br />
							<textarea name="cour_details" rows="4"></textarea><br />
							<label>Course Date</label>
							<input type="date" name="cour_date" /><br />
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th><center><span class="span1">Remove Item</span></center></th>
										<th><center><span class="span1">Domain Name</span></center></th>
									</tr>
								</thead>
								<tbody id="selecteditems3">
								
								</tbody>
							</table>
						</div>
						
					</div>
					
				</div>
				
			</div>
			-->
			<?php echo form_submit('add','Save','');?>
		</div>
	</body> 
</html>
