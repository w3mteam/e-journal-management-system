<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">Added recently</h2>
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">	
			<?php
			if((sizeof($paper_added_recently) >= 1) && (sizeof($paper_added_recently)>=2) && (sizeof($paper_added_recently)>= 3)) {
				 
			for ($i=0; $i < 3 ; $i++) { ?>
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<?php if($paper_added_recently[$i]->paper_type == 'JOURNAL'){ ?>
									<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
								<?php } else { ?>
									<img src="<?php echo base_url();?>files/public/images/home/paper_added_recently.jpg" alt="" />
								<?php } ?>
								
								<small class="text-muted"><?php echo $paper_added_recently[$i]->paper_creation_date;?></small>
								<br />
								<a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $paper_added_recently[$i]->paper_id ;?>" class="btn btn-default add-to-cart">View Details</a>
							</div>
							
						</div>
					</div>
				</div>
				<?php } } ?>
			</div>
			<div class="item">
			<?php
			if((sizeof($paper_added_recently) >= 3) && (sizeof($paper_added_recently)>=3) && (sizeof($paper_added_recently)>= 5)) { 
			for ($i=3; $i < 6 ; $i++) { ?> 
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<?php if($paper_added_recently[$i]->paper_type == 'JOURNAL'){ ?>
									<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
								<?php } else { ?>
									<img src="<?php echo base_url();?>files/public/images/home/paper_added_recently.jpg" alt="" />
								<?php } ?>
								
								<small class="text-muted"><?php echo $paper_added_recently[$i]->paper_creation_date;?></small>
								<br />
								<a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $paper_added_recently[$i]->paper_id ;?>" class="btn btn-default add-to-cart">View Details</a>
							</div>
						</div>
					</div>
				</div>
			<?php } } ?>
		    </div>
		</div>
		 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		  </a>
		  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		  </a>			
	</div>
</div><!--/recommended_items-->