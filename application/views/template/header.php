<?php
$this->load->helper('ion_auth_helper');
$this->load->helper('notification_helper');

$logged_in = logged_in();
?>
<style>
	.noti{
		-webkit-border-radius: 90px;
		-moz-border-radius: 90px;
		border-radius: 90px;
		background-color: red;
		color: #fff;
		padding-left: 6px;
    	padding-right: 6px;
		
	}
</style>
<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> 0991639489</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> w3m@gmail.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="index.html"><img src="<?php echo base_url();?>files/public/images/home/logo.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<?php if ($logged_in == TRUE){ ?>
									
									<li><a href="<?php echo base_url(); ?>index.php/account"><i class="fa fa-user"></i> Account</a></li>
								<?php } else { ?>
									<li><a href="<?php echo base_url(); ?>index.php/account/register"><i class="fa fa-user"></i> Register</a></li>
								<?php } ?>
								<?php //$this->load->helper('ion_auth');
								if ($logged_in == TRUE){ //($this->ion_auth->logged_in() == TRUE){ ?>
									<li><a href="<?php echo base_url() ;?>index.php/account/logout"><i class="fa fa-lock"></i> Logout</a></li>
								<?php } else { ?>
									<li><a href="<?php echo base_url() ;?>index.php/account/login"><i class="fa fa-lock"></i> Login</a></li>
								<?php } ?>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="<?php echo base_url();?>" class="active">Home</a></li>
								
								<li><a href="<?php echo base_url();?>index.php/main/display_all_concept">Category</a></li>
								<li><a href="<?php echo base_url();?>index.php/main/get_paper">Paper</a></li>
								<li><a href="<?php echo base_url();?>index.php/main/get_journal">Journal</a></li>
								<li><a href="<?php echo base_url();?>index.php/main/get_profile">Profiles</a></li>
								<?php if ($logged_in == TRUE){ 
									$num_of_noti = get_number_not_seen();
									?>
									
								<li id="notifications" class="dropdown"><a href="#">Notifications <?php if($num_of_noti != 0){echo '<span id="notification_count" class="noti">'.$num_of_noti.'</span>';}?><i class="fa fa-angle-down"></i></a>
                                    <?php
                                    $nots = get_unseen_notifications();
									if (!empty($nots)){
										?>
										<ul role="menu" class="sub-menu">
											<?php
											foreach ($nots as $key => $not) {
												echo '<li><a href="blog.html" style="font-size: 12px;">'.str_replace("paper", "Paper<b>[$not->paper_title]</b>", $not->not_text).'<br>'.$not->not_pushing_date.'</a></li>';
											}
											?>
	                                        
	                                    </ul>
										<?php
									}
									?>
                                </li>
                                <?php }?>
							</ul>
						</div>
					</div>
					<div class="col-sm-5">
						<head>
							<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/chosen.css" />
						</head>	
						<?php echo form_open('search_engin/manipulate_search_request','id="send_data"');?>
						<!---<input type="submit"  value="Add" class="pull-right" /><br />-->
						<div class="search_box pull-right">
							<!--
							<div class="side-by-side clearfix">
								<div>
									<em>keywords</em>
									<select data-placeholder="Choose Keywords..." class="chzn-select" multiple style="width:350px;" tabindex="4">
										<?php 
										foreach (json_decode($concept) as $key => $item) {
											echo '<option value="'.$item->concept_id.'">'.$item->name.'</option>';
										}
										?>
									</select>
								</div>
							</div>
							-->
							<div class="search_box pull-right">
								<input type="text" placeholder="Search"/>
							</div>
						</div >
						
						<?php echo form_close();?>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	

