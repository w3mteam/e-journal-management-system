<?php
$this->load->helper('foot_helper');
$four_concepts_for_footer = four_concepts_for_footer();
?>
<div class="result"></div>
<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>Semantic</span>-Journal System</h2>
							<p>Semantic Journal Systems (SJS) is a journal management and publishing system that has been developed by W3M</p>
						</div>
					</div>
					<div class="col-sm-7">
						<?php
						
						foreach ($four_concepts_for_footer as $key => $value) { ?>
							<div class="col-sm-3">
								<div class="video-gallery text-center">
									<a href="#">
										<div class="iframe-img">
											<img src="<?php echo base_url();?>files/public/images/home/<?php echo $value->prog_name; ?>.jpg" alt="" />
										</div>
										
									</a>
									<p><a href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $value->concept_id ;?>/<?php echo $value->prog_name; ?>"><?php echo $value->name; ?></a></p>
									
								</div>
							</div>
						<?php } ?> 
						
						
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="<?php echo base_url();?>files/public/images/home/map.png" alt="" />
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © <?php echo date("Y"); ?> W3M TEAM All rights reserved.</p>
					
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    
     <script src="<?php echo base_url();?>files/public/js/mootools-yui-compressed.js"></script>
	<script src="<?php echo base_url();?>files/public/js/mootools-more-1.4.0.1.js"></script>
	<script src="<?php echo base_url();?>files/public/js/chosen.js"></script>
	<script src="<?php echo base_url();?>files/public/js/Locale.en-US.Chosen.js"></script>
	<script> $$(".chzn-select").chosen(); $$(".chzn-select-deselect").chosen({allow_single_deselect:true}); </script>
	<script src="<?php echo base_url();?>files/public/js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript">
		var $j = jQuery.noConflict() 
		$j(document).ready(function () {
			$j('#send_data').click(function(){
				var keywords = [];
				$j(".search-choice").each(function(i,val){
					var name = $j(val).children("span").html();
				    var id = $j(val).children("span").attr('id');
				    keywords[i] = id;
				    //console.log(id+":"+name);
				})
				//console.log(keywords);
				b_url = "<?php echo base_url(); ?>";
				var obj = $j('form.search_engin'), // (*) references the current object/form each time
			   url = b_url+'index.php/search_engin/manipulate_search_request',
			   method = obj.attr('method'),
			   data = {};
			  data["keywords"] = JSON.stringify(keywords);
			 // console.log(data);
			  //alert("here");
			  $j.ajax({
			   // see the (*)
			   url: url,
			   type: "POST",
			   data: data,
			   success: function(response) {
			   	console.log(response);
			       //alert("success");
			       var jsonData = JSON.parse(response);
	        	   //console.log(data);		        					            	
	            	for (var i = 0; i < jsonData.length; i++) {
					  
					   //$('.result').append(jsonData[i]);
					   console.log(jsonData[i]);
	            	}
			   }
			  });
			  event.preventDefault();
			});
			 
		});
	</script> 
	
	<script src="<?php echo base_url();?>files/public/js/jquery.js"></script>
	
	<script src="<?php echo base_url();?>files/public/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>files/public/js/jquery.scrollUp.min.js"></script>
	<script src="<?php echo base_url();?>files/public/js/price-range.js"></script>
    <script src="<?php echo base_url();?>files/public/js/jquery.prettyPhoto.js"></script>
    <script> var $k = jQuery.noConflict() ;</script> 
    <script src="<?php echo base_url();?>files/public/js/main.js"></script>
</body>
</html>