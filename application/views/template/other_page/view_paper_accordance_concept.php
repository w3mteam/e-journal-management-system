<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center"><img src="<?php echo base_url();?>files/public/images/home/<?php echo $prog_name ; ?>.jpg" alt="" /></h2>
						<?php if ($paper != null) {
						foreach ($paper as $key => $value) { ?>
							<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<?php if($value->paper_type == 'PAPER') { ?>
												<img src="<?php echo base_url();?>files/public/images/home/paper.jpg" alt="" />
											<?php } else {  ?>
												<img src="<?php echo base_url();?>files/public/images/home/journal.jpg" alt="" />
											<?php } ?>
											
											<p>BY: <?php echo $value->username;?></p>
											
											<small class="text-muted"><?php echo $value->paper_creation_date;?></small>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h4><?php echo $value->paper_title;?></h4>
											    <p>BY: <?php echo $value->username;?></p>
												<a href="#" class="btn btn-default add-to-cart">Download</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $value->paper_id ;?>"><i class="fa fa-plus-square"></i>View Details </a></li>
										
									</ul>
								</div>
							</div>
						</div>
						<?php } } else{
							echo '<center><img src="'.base_url().'files/public/images/home/paper_not_found.jpg" class="img-responsive" /><center/></br>';
						}?>
						
					</div><!--features_items-->
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>