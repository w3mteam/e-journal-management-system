
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
							<div class="col-sm-5">
								<div class="view-product img_profile">	
									<img src="<?php echo base_url();?>files/public/images/home/profile.jpg" alt="" />
								</div>
	
							</div>
							<div class="col-sm-7">
								<div class="product-information"><!--/product-information-->
									<img  src="<?php echo base_url();?>files/public/images/home/new.jpg" class="newarrival" alt="" />
									<span><span><?php echo $profile['info']->first_name.' '.$profile['info']->last_name;?></span></span><br />
									<small class="text-muted">Creation Date : <?php echo $profile['info']->date_time ;?></small>
									<h4>specialization :</h4>
									<?php for ($i=0; $i < sizeof($profile['specializations']) ; $i++) { ?>
										<a href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $profile['specializations'][$i]->concept_id; ?>/<?php echo $profile['specializations'][$i]->prog_name; ?>"><p id="style_concept"><?php echo $profile['specializations'][$i]->name ;?></p></a>
									<?php } ?>
									<label style="margin-top: 13px;">Tasks Number : <?php echo $profile['info']->task_num ;?></label>
										
									
								</div><!--/product-information-->
							</div>
						</div><!--/product-details-->
						
						<div class="category-tab shop-details-tab"><!--category-tab-->
							<div class="col-sm-12">
								<ul class="nav nav-tabs">
									<li><a href="#publicationss" data-toggle="tab">Publications</a></li>
									<li><a href="#projectss" data-toggle="tab">Projects</a></li>
									<li><a href="#coursess" data-toggle="tab">Courses</a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane active in fade" id="publicationss" >
										<div class="product-image-wrapper">
											<div class="single-products">
												<div  class="productinfo">
													<?php
													if($profile['publications'] != null){
														for ($i=0; $i < sizeof($profile['publications']); $i++) { ?>
															<h2><?php echo $profile['publications'][$i]['publication']->name;?></h2>
															<p style="text-align: justify; "><?php echo $profile['publications'][$i]['publication']->details;?></p>
															
															<small class="text-muted">Creation Data:<?php echo $profile['publications'][$i]['publication']->date_time;?></small><br />
															<?php
															$concepts = $profile['publications'][$i]['concept']; 
															for ($j=0; $j < sizeof($concepts) ; $j++) { ?> 
																<a href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $concepts[$j]->concept_id; ?>/<?php echo $concepts[$j]->prog_name; ?>"><p id="profile_concept"><?php echo $concepts[$j]->name; ?></p></a>
															<?php } ?>
															<br /><br />
														<?php } 
													} else{
														echo '<p>Publications Not Found..</p>';
													} ?>
													
													
												</div>
											</div>
										</div>
								</div>
								<div class="tab-pane fade" id="projectss" >
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo">
													<?php
													if($profile['projects'] != null){
														for ($i=0; $i < sizeof($profile['projects']); $i++) { ?>
															<h2><?php echo $profile['projects'][$i]['project']->name;?></h2>
															<p style="text-align: justify; "><?php echo $profile['projects'][$i]['project']->details;?></p>
															
															<small class="text-muted">Creation Data:<?php echo $profile['projects'][$i]['project']->date_time;?></small><br />	
												
															<?php
															$concepts = $profile['projects'][$i]['concept']; 
															for ($j=0; $j < sizeof($concepts) ; $j++) { ?> 
																<a href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $concepts[$j]->concept_id; ?>/<?php echo $concepts[$j]->prog_name; ?>"><p id="profile_concept"><?php echo $concepts[$j]->name; ?></p></a>
															<?php } ?>
															<br /><br />
														<?php }
													} else{
														echo '<p>Projects Not Found..</p>';
													} ?>
													
													
												</div>
											</div>
										</div>
								</div>
								
								<div class="tab-pane fade" id="coursess" >
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo">
													<?php
													if($profile['courses'] != null){
														for ($i=0; $i < sizeof($profile['courses']); $i++) { ?>
															<h2><?php echo $profile['courses'][$i]['course']->name;?></h2>
															<p style="text-align: justify; "><?php echo $profile['courses'][$i]['course']->details;?></p>
															
															<small class="text-muted">Creation Data:<?php echo $profile['courses'][$i]['course']->date_time;?></small><br />	
															
															<?php
															$concepts = $profile['courses'][$i]['concept']; 
															for ($j=0; $j < sizeof($concepts) ; $j++) { ?> 
																<a href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $concepts[$j]->concept_id; ?>/<?php echo $concepts[$j]->prog_name; ?>"><p id="profile_concept"><?php echo $concepts[$j]->name; ?></p></a>
															<?php } ?>
															<br /><br />
														<?php } 
													} else{
														echo '<p>Courses Not Found..</p>';
													} ?>
													
													
												</div>
											</div>
										</div>
								</div>
							</div>
						</div><!--/category-tab-->
						<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>
