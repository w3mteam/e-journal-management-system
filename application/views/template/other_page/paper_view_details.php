<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
						<div class="product-details"><!--product-details-->
							<div class="col-sm-5">
								<div class="view-product img_style">
									<?php if($paper_details[0]->paper_type == 'PAPER') { ?>
										<img src="<?php echo base_url();?>files/public/images/home/paper.jpg" alt="" />
									<?php } else {  ?>
										<img src="<?php echo base_url();?>files/public/images/home/journal.jpg" alt="" />
									<?php } ?>
								</div>
	
							</div>
							<div class="col-sm-7">
								<div class="product-information"><!--/product-information-->
									<img src="<?php echo base_url();?>files/public/images/home/new.jpg" class="newarrival" alt="" />
									<span><span><?php echo $paper_details[0]->paper_title;?></span></span><br />
									<small class="text-muted"><?php echo $paper_details[0]->paper_creation_date ;?></small>
									<h4>By: <?php echo $paper_details[0]->username ;?></h4>
									<span>
										
										<label>Type: <?php echo $paper_details[0]->paper_type; ?></label>
										<a href="#" class="btn btn-fefault cart">
											Download
										</a>
									</span>
									
								</div><!--/product-information-->
							</div>
						</div><!--/product-details-->
						
						<div class="category-tab shop-details-tab"><!--category-tab-->
							<div class="col-sm-12">
								<ul class="nav nav-tabs">
									<li><a href="#details" data-toggle="tab">Details</a></li>
									<li><a href="#tag" data-toggle="tab">Tag</a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane active in fade" id="details" >
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<p><?php echo $paper_details[0]->paper_description;?></p>
												</div>
											</div>
										</div>
								</div>
								
								
								<div class="tab-pane fade" id="tag" >
									<?php 
									for ($i=0; $i <sizeof($paper_concept) ; $i++) { ?> 
										<div class="col-sm-4">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<div class="img_tag" >
															<img src="<?php echo base_url();?>files/public/images/home/<?php echo $paper_concept[$i]->prog_name ; ?>.jpg" alt="" />
														</div>
														<center><a class="btn btn-default add-to-cart" href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $paper_concept[$i]->concept_id ;?>/<?php echo $paper_concept[$i]->prog_name; ?>"><?php echo $paper_concept[$i]->name; ?></a></center>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
								
							</div>
						</div><!--/category-tab-->
						<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>