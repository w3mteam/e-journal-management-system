<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Papers</h2>
						<?php if ($paper != null) {
						foreach ($paper as $key => $value) { ?>
							
							<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="<?php echo base_url();?>files/public/images/home/paper.jpg" alt="" />
											
											<p>BY: <?php echo $value->username;?></p>
											
											<small class="text-muted"><?php echo $value->paper_creation_date;?></small>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h5><?php echo $value->paper_title;?></h5>
											    <p>BY: <?php echo $value->username;?></p>
												<a href="#" class="btn btn-default add-to-cart">Download</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $value->paper_id ;?>"><i class="fa fa-plus-square"></i>View Details </a></li>
									</ul>
								</div>
							</div>
						</div>
						<?php } }?>
						
					</div><!--features_items-->
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>