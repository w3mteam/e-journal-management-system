<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<?php $this->load->view('template/slider'); ?>
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Papers & Journals</h2>
						
						<?php if ($paper != null) {
						foreach ($paper as $key => $value) { ?>
							
							<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<?php if($value->paper_type == 'PAPER') { ?>
												<img src="<?php echo base_url();?>files/public/images/home/paper.jpg" alt="" />
											<?php } else {  ?>
												<img src="<?php echo base_url();?>files/public/images/home/journal.jpg" alt="" />
											<?php } ?>
											<h2>BY: <?php echo $value->username;?></h2>
											<p><?php echo $value->paper_type;?></p>
											
											<small class="text-muted"><?php echo $value->paper_creation_date;?></small>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<h2>BY: <?php echo $value->username;?></h2>
												<p><?php echo $value->paper_title;?></p>
												<a href="#" class="btn btn-default add-to-cart">Download</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $value->paper_id ;?>"><i class="fa fa-plus-square"></i>View Details </a></li>
									</ul>
								</div>
							</div>
						</div>
						<?php } }?>
						
					</div><!--features_items-->
					
					<div class="category-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								
								<?php for ($i=0; $i <sizeof($six_concepts) ; $i++) { ?>
									<li><a href="#<?php echo $six_concepts[$i]['prog_name'];?>" data-toggle="tab"><?php echo $six_concepts[$i]['concept_name'] ;?></a></li>
								<?php } ?>
								<li style="float: right;"><a href="<?php echo base_url();?>index.php/main/display_all_concept">See More</a></li>
							</ul>
							
						</div>
						
						<div class="tab-content">
								<?php for ($i=0; $i <sizeof($six_concepts) ; $i++) { ?>
								<div class="tab-pane fade <?php if($i==0) echo 'active in';?>" id="<?php echo $six_concepts[$i]['prog_name'];?>" >
									<?php
									$paper =  $six_concepts[$i]['paper'];
									for ($j=0; $j < sizeof($paper) ; $j++) { ?>
										<div class="col-sm-3">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<img src="<?php echo base_url();?>files/public/images/home/<?php echo $six_concepts[$i]['prog_name'] ;?>.jpg" alt="" />
														<h4><?php echo $paper[$j]->paper_type;?></h4>
														<small class="text-muted"><?php echo $paper[$j]->paper_creation_date;?></small>
														<div class="choose">
															<ul class="nav nav-pills nav-justified">
																<li><a href="<?php echo base_url();?>index.php/main/paper_view_details/<?php echo $paper[$j]->paper_id ;?>"><i class="fa fa-plus-square"></i>View Details </a></li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
									<div>
										<div class="col-sm-12">
											<a style="float: right;color: #fe980f;" href="<?php echo base_url();?>index.php/main/view_paper_accordance_concept/<?php echo $six_concepts[$i]['concept_id'];?>/<?php echo $six_concepts[$i]['prog_name'];?>">SEE MORE <?php echo $six_concepts[$i]['concept_name'] ;?></a>
										</div>
									</div>
								</div>
								
								<?php } ?>
								
						</div>
						
					</div><!--/category-tab-->
					<?php $this->load->view('template/added_recently.php');?>
					
				</div>
			</div>
		</div>
	</section>