
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
           <p><img src="<?php echo base_url(); ?>files/public/images/notifications.gif"  /> All notifications for you</p>
        </div>
		<div class="panel-body"> 
			<div class="table-responsive cart_info">	
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<th>PAPER Name</th>						
							<th>Notification Body</th>
							<th>Notification Date</th>
						</tr>
					</thead>
					<tbody>
							<?php 
							if(!empty($notifications)){
								foreach ($notifications as $noty ) { ?>
								<tr>
										<td><?php echo $noty->paper_title; ?></td>
										<td><?php echo $noty->not_text; ?></td>
										<td><?php echo $noty->not_pushing_date;?></td>
									</tr>
								<?php }
							}
							else {
								echo "There are no notifications to show";
							}
							?>
									
					</tbody>
				</table>
			</div>	
					
       </div>
	</div>
</div>


