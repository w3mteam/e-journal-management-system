

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>files/public/images/Create_new_Group.png"  />Create a new Group</p>
        </div>
		<div class="panel-body">
        	<h1><?php echo lang('create_group_heading');?></h1>
			<p><?php echo lang('create_group_subheading');?></p>
			
			<div id="infoMessage"><?php echo $message;?></div>
			
			<?php echo form_open("account/create_group");?>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<p>
						            <?php echo lang('create_group_name_label', 'group_name');?> <br />
						            <?php echo form_input($group_name,'','class="form-control"');?>
						      </p>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <p>
						            <?php echo lang('create_group_desc_label', 'description');?> <br />
						            <?php echo form_input($description,'','class="form-control"');?>
						      </p>
						</div>
					</div>
			      
			
			     
			
			      <p><?php echo form_submit('submit', lang('create_group_submit_btn'),'class="btn btn-primary btn-block"');?></p>
			
			<?php echo form_close();?>
       </div>
	</div>
</div>

