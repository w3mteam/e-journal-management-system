

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>files/public/images/Create_new_Group.png"  />Create a new Group</p>
        </div>
		<div class="panel-body">
        	

		<h1><?php echo lang('change_password_heading');?></h1>
		
		<div id="infoMessage"><?php echo $message;?></div>
		
		<?php echo form_open("account/change_password");?>
		
		      <p>
		            <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
		            <?php echo form_input($old_password,'','class="form-control"');?>
		      </p>
		
		      <p>
		            <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
		            <?php echo form_input($new_password,'','class="form-control"');?>
		      </p>
		
		      <p>
		            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
		            <?php echo form_input($new_password_confirm,'','class="form-control"');?>
		      </p>
		
		      <?php echo form_input($user_id);?>
		      <p><?php echo form_submit('submit', lang('change_password_submit_btn'),'class="btn btn-primary btn-block"');?></p>
		
		<?php echo form_close();?>

       </div>
	</div>
</div>
