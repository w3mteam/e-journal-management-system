<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Log In</h2>
						<div class="col-sm-3"></div>
						<div class="col-sm-6">
							<div class="login-form"><!--login form-->
								<center><h2><?php echo lang('login_subheading');?></h2></center>
								
									<div id="infoMessage"><?php echo $message;?></div>
									
									<?php echo form_open("account/login/".$action);?>
									
									  <p>
									    <?php echo lang('login_identity_label', 'identity');?>
									    <?php echo form_input($identity);?>
									  </p>
									
									  <p>
									    <?php echo lang('login_password_label', 'password');?>
									    <?php echo form_input($password);?>
									  </p>
									
									<span>
										
										<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
										<?php echo lang('login_remember_label', 'remember');?>
									</span>
									
									
									  <p><?php echo form_submit('submit', lang('login_submit_btn'),'id="btn-color"');?></p>
									
									<?php echo form_close();?>
									
									<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
							</div><!--/login form-->
						</div>
						<div class="col-sm-3"></div>
						
						
					</div><!--features_items-->
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>


