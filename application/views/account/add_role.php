
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/add_role.png" />Add Role</p>
		</div>
		<div class="panel-body">
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($mode == "sucess"){
							echo "<p>Role has been insert successfuly</p>";
						}else if($mode == "error"){
							echo $error;
						} ?>
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Current Role</p></th>
								</tr>
			                </thead>
			                <tbody id="items_c_r">
			                	<?php
									foreach ($roles as $value) {
										echo'<tr><td>'.$value->name.'</td></tr>';
									}
								?>
			                </tbody>
						</table>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<?php echo form_open('auth/insert_role') ?>
					<p>Enter Role:</p>
					<?php echo form_input('role',set_value('role'),'class="font-input form-control" placeholder="Enter Role" required=""'); ?>
					<br />
					<p>Enter Description</p>
					<?php echo form_textarea('description',set_value('description'),'class="font-input form-control" required=""'); ?>
					<br />
					<?php echo form_submit('insert','Add','class="btn btn-log btn-block btn-primary"');?>
						
					<?php echo form_close();?>
				</div>
			</div>
			
		</div>
	</div>
</div>




