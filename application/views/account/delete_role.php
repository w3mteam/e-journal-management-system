
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>files/public/images/delete_role.png" />Delete Role</p>
		</div>
		<div class="panel-body">
			<div class="row">
				
				<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($mode == "success"){
							echo "<p>Role has been Deleted successfuly</p>";
						}else if($mode == "error"){
							echo $error;
						} ?>
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Current Role</p></th>
								</tr>
			                </thead>
			                <tbody id="items_c_r">
			                	<?php
									foreach ($roles as $value) {
										echo'<tr><td><a style="float:left;padding-right:25px;" href="'.base_url().'index.php/auth/remove_role/'.$value->id.'"><img src="'.base_url().'files/public/images/c_drop.png" /></a>'.$value->name.'</td></tr>';
									}
								?>
								
			                </tbody>
						</table>
						
				</div>
			</div>
			
		</div>
	</div>
</div>




