<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Forgot Password</h2>
						<div class="col-sm-3"></div>
						<div class="col-sm-6">
							<div class="login-form"><!--login form-->
								<h1><?php //echo lang('forgot_password_heading');?></h1>
								<center><p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p></center>
								
								<div id="infoMessage"><?php echo $message;?></div>
								
								<?php echo form_open("account/forgot_password");?>
								
								      <p>
								      	<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
								      	<?php echo form_input($email);?>
								      </p>
								
								      <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),'id="btn-color"');?></p>
								
								<?php echo form_close();?>
							</div><!--/login form-->
						</div>
						<div class="col-sm-3"></div>
						
						
					</div><!--features_items-->
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>





