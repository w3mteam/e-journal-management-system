
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>files/public/images/assign_privilage.png" />Assigning Privilage</p>
		</div>
		<div class="panel-body">
			<?php if($result == "success"){
				if($mode != ''){
					echo $mode;
				} 
				?>
				<select name="role" id="role" size="1" class="form-control">
					<option>--</option>
					<?php
						foreach ($roles as $value) {
							echo'<option value="'.$value->id.'">'.$value->name.'</option>';
						}
					?>
				</select>
				<br />
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Current Privilage</p></th>
								</tr>
			                </thead>
			                <tbody id="items_c_p"></tbody>
						</table>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Add Privilage</p></th>
								</tr>
			                </thead>
			                <tbody id="items_a_p"></tbody>
						</table>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Delete Privilage</p></th>
								</tr>
			                </thead>
			                <tbody id="items_d_p"></tbody>
						</table>
					</div>
				</div>
			<?php
			}else{
				echo "Access denied";
			} ?>
			
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
    	$('#role').on('change', function (e) {
		    var optionSelected = $("option:selected", this);
		    var valueSelected = this.value;
		    var form_data = {'role_id' : valueSelected };
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/account/get_privilages",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		            	var jsonData = JSON.parse(data);
		        		$('#items_c_p').html('');
		        		$('#items_d_p').html('');
		            	for (var i = 0; i < jsonData.length; i++) {
		            		$('#items_d_p').append('<tr><td>' + jsonData[i].privilage_en + '<a style="float:left;padding-right:25px;" href="<?php echo base_url(); ?>index.php/account/delete_role_privilage/'+jsonData[i].id+'"><img src="<?php echo base_url();?>files/public/images/c_drop.png"  /></a></td></tr>');
		            		$('#items_c_p').append('<tr><td>' + jsonData[i].privilage_en + '</td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    $.ajax({
		        url: "<?php echo base_url();?>index.php/account/get_non_privilages",
		        type: 'POST',
		        data: form_data,
		        success: function(data){
		        		$('#items_a_p').html('');
		            	var jsonData = JSON.parse(data);
		            	for (var i = 0; i < jsonData.length; i++) {
		            		$('#items_a_p').append('<tr><td>' + jsonData[i].privilage_en + '<a style="float:left;padding-right:25px;" href="<?php echo base_url(); ?>index.php/account/add_role_privilage/'+jsonData[i].id+'/'+form_data['role_id']+'"><img src="<?php echo base_url();?>files/public/images/add.png"  /></a></td></tr>');
		            	}
		        },
		        error: function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			    }
		    });
		    return false;
		});
	});
	
</script>



