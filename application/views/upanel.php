<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Account</h2>
						<div class="col-sm-4">
							<div class="video-gallery text-center">
								<a href="<?php echo base_url();?>index.php/account/edit_user/<?php echo$user_id;?>">
									<div class="cat_img" >
										<img src="<?php echo base_url();?>files/public/images/home/edit_profile.jpg" alt="" />
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="video-gallery text-center">
								<a href="<?php echo base_url();?>index.php/paper/add/">
									<div class="cat_img" >
										<img src="<?php echo base_url();?>files/public/images/home/add_new_paper.jpg" alt="" />
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="video-gallery text-center">
								<a href="<?php echo base_url();?>index.php/paper/show/">
									<div class="cat_img" >
										<img src="<?php echo base_url();?>files/public/images/home/view_my_paper.jpg" alt="" />
									</div>
								</a>
							</div>
						</div>
						
					</div>
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>


