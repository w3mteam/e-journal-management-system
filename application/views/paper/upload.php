	<head>
		<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/chosen.css" />
	</head>
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Add Paper Or Journal</h2>
						<?php 
						echo "<div id='message'>$message</div>";
						echo form_open_multipart('paper/add/state/continue',array('id' => 'file_data')); ?>
						<div class="col-sm-6">
							<?php echo form_upload('file','','class="form-control"');?>
							 <div class = "upload-image-messages"></div>
						</div>
						<div class="col-sm-6">
							<?php echo form_submit('upload','Upload','class="form-control" id="btn-color"');?>
						</div>
						
						
						<?php echo form_close();
						
						?>

						<br>
						
					</div>
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>
<script src="<?php echo base_url();?>files/public/js/jquery-1.10.2.js"></script>

	<script type="text/javascript">
		var $j = jQuery.noConflict() ;
		$j(document).ready(function () {
			$j("#message").delay(3000).slideUp("slow");
		});
	</script>