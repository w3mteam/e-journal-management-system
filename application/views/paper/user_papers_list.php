<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<section id="cart_items">
							<?php if(isset($message)) echo $message.'<br>';
							if(!empty($papers)){ ?>
							<div class="table-responsive cart_info">
								<table class="table table-condensed">
									<thead>
										<tr class="cart_menu">
											<td class="image">Paper</td>
											<td class="description"></td>
											<td></td>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($papers as $key => $paper) { ?>
											<tr>
												<td class="cart_product">
													<?php if($paper->paper_type == 'PAPER') { ?>
														<img src="<?php echo base_url();?>files/public/images/home/paper_added_recently.jpg" alt="" />
													<?php } else if($paper->paper_type == 'JOURNAL'){  ?>
														<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
													<?php } else{ ?>
														<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
													<?php } ?>
													
												</td>
												<td class="cart_description">
													<h4><a href=""><?php echo anchor(base_url().'files/private/'.$paper->paper_type.'/'.$paper->paper_file,$paper->paper_title);?></a></h4>
													<p><small class="text-muted"><?php echo $paper->paper_creation_date ;?></small></p>
													<p><small class="text-muted">status:<span style="color: red;"><?php echo $paper->st_name_en ;?></span></small></p>
												</td>
												<td class="cart_price">
													<p>
														<?php
														if ($paper->paper_published ==0){
															echo anchor('paper/edit/'.$paper->paper_id,'Edit','class="anchor" id="style_concept"');
															if($paper->st_name =='pending' ||$paper->st_name =='need_editing'){
																echo anchor('paper/upload_new_file/'.$paper->paper_id,'Upload New File','class="anchor" id="style_concept"');
															}
														}
														
														if($paper->st_name == "need_editing"||$paper->st_name == "rejected"||$paper->st_name == "ready_to_publish"){
															echo anchor('paper/paper_judgment_result/'.$paper->paper_id,'view paper judgment result','class="anchor" id="style_concept"');
														}
														?>
													</p>
												</td>
												
											</tr>
										<?php } ?>
										
									</tbody>
								</table>
							</div>
							<?php } else {
								echo 'there is no papers to view';
							}
							?>
					</section> <!--/#cart_items-->
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>

