	<head>
		<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/chosen.css" />
	</head>
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Add Paper Or Journal</h2>
						<?php echo form_open('paper/add/add','class="add_paperForm"');
						echo "<div id='message'>$message</div>";
						?>
						<br>
						<div class="row">
							<div class="col-sm-6">
								Title: <input class="form-control" type="text" value="<?php echo set_value('title'); ?>" name="title" required=""/><br /><br />
							</div>
							<div class="col-sm-6">
								<div class="side-by-side clearfix" style="  margin-top: 19px;">
									<div>
										<select data-placeholder="Choose Keywords..." class="chzn-select form-control" multiple style="width:350px;" tabindex="3" required="">
											<?php 
											foreach (json_decode($concept) as $key => $item) {
												echo '<option value="'.$item->concept_id.'">'.$item->name.'</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								Type:
								<select name="type" class="form-control" required="">
									<option value="PAPER">Paper</option>
									<option value="JOURNAL">Journal</option>
								</select>
							</div>
							<div class="col-sm-6">
								Description:<br />
								<textarea class="form-control" required="" name="description" ><?php echo set_value('description'); ?></textarea>
							</div>
						</div>
						
						
						<div class="row">
							<div class="col-sm-6">
								<input type="submit" class="btn btn-block" id="add_paper_data" value="Add" /><br />	
							</div>
						</div>
						
						<?php 
						echo form_close();
						echo validation_errors();
						?>
						<br>
						<div id="res" style="display: none;">
							<?php 
							echo form_open_multipart('paper/add/state/continue',array('id' => 'file_data')); ?>
							<div class="col-sm-6">
								<?php echo form_upload('file','','class="form-control"');?>
								 <div class = "upload-image-messages"></div>
							</div>
							<div class="col-sm-6">
								<?php echo form_submit('upload','Upload','class="form-control" id="btn-color"');?>
							</div>
							
							
							<?php echo form_close();
							
							?>
						</div>
					</div>
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>	

<script src="<?php echo base_url();?>files/public/js/jquery-1.10.2.js"></script>

	<script src="<?php echo base_url();?>files/public/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>files/public/js/bootstrap-dialog.js"></script>
	<script type="text/javascript">
		var $j = jQuery.noConflict() ;
		$j(document).ready(function () {
			$j("#message").delay(6000).slideUp("slow");
			$j('#add_paper_data').click(function(){
				b_url = "<?php echo base_url(); ?>";
				var keywords = [];
				if($j(".search-choice").length > 0){
					$j(".search-choice").each(function(i,val){
						var name = $j(val).children("span").html();
					    var id = $j(val).children("span").attr('id');
					    keywords[i] = id;
					    console.log(id+":"+name);
					})
					var obj = $j('form.add_paperForm'), // (*) references the current object/form each time
					url = b_url+'index.php/paper/add/add',
					method = obj.attr('method'),
					data = {};
					 obj.find('[name]').each(function(index, value) {
					   // console.log(value);
					   var obj = $j(this),
					    name = obj.attr('name'),
					    value = obj.val();
					    if(value ==''){
					    	var dialogInstance2 = new BootstrapDialog();
					        dialogInstance2.setTitle('Error');
					        dialogInstance2.setMessage("You should add paper "+name);
					        dialogInstance2.setType(BootstrapDialog.TYPE_DANGER);
					        dialogInstance2.open();
					        event.preventDefault();
					    }
					    else
					   		data[name] = value;
					  });
					  data["keywords"] = JSON.stringify(keywords);
					  $j.ajax({
					   // see the (*)
					   url: url,
					   type: method,
					   data: data,
					   success: function(response) {
					   	var result = JSON.parse(response);
					    $j('#res').html(result.page);
					    $j("form.add_paperForm").slideUp("slow");
					    $j('#res').css('display','block');
					    var dialogInstance2 = new BootstrapDialog();
				        dialogInstance2.setTitle('Result');
				        dialogInstance2.setMessage(result.message);
				        dialogInstance2.setType(BootstrapDialog.TYPE_SUCCESS);
				        dialogInstance2.open();
					   }
					  });
					  event.preventDefault();
				}
				else
				{
					var dialogInstance2 = new BootstrapDialog();
			        dialogInstance2.setTitle('Error');
			        dialogInstance2.setMessage("You should choose paper related concepts");
			        dialogInstance2.setType(BootstrapDialog.TYPE_DANGER);
			        dialogInstance2.open();
				}
				
				
				
			  
			 
			 
			  
			  
			});
			 
		});
	</script>
	
