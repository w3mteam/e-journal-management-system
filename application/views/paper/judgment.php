

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-10 col-sm-10 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p><img src="<?php echo base_url(); ?>files/public/images/Control_panel_judgment.png"  />Control panel judgment</p>
        </div>
		<div class="panel-body">
        	<?php
						if(isset($message)) echo $message.'<br>';
						if(isset($paper)){
							echo form_open('paper/judge/'.$paper->paper_id);
							if(isset($measures)){
								$count = 2;
								echo '<div class="row">';
								foreach ($measures as $key => $measure) {
									
									if($count % 2 == 0){
										echo '<div class="col-sm-6 col-md-6 col-xs-12">';
										echo '<div class="measure_name">'.$measure['measures']->jm_name_en.'</div>';
										echo '<select class="form-control" name="'.$measure['measures']->jm_id.'">';
										if(isset($measure['rank_levels'])){
											foreach ($measure['rank_levels'] as $key => $level) {
												echo '<option value="'.$level->rl_id.'">'.$level->rl_name_en.'</option>';
											}
										}
										echo '</select><br>';
										echo '</div>';
									}else{
										echo '<div class="col-sm-6 col-md-6 col-xs-12">';
										echo '<div class="measure_name">'.$measure['measures']->jm_name_en.'</div>';
										echo '<select class="form-control" name="'.$measure['measures']->jm_id.'">';
										if(isset($measure['rank_levels'])){
											foreach ($measure['rank_levels'] as $key => $level) {
												echo '<option value="'.$level->rl_id.'">'.$level->rl_name_en.'</option>';
											}
										}
										echo '</select><br>';
										echo '</div>';
									}
									$count = $count+1;
								}

							echo '</div>';
							}
							
								
							echo '<hr/><div class="row">
							<div class="col-md-6 col-sm-6 col-xs-12">';
							if(isset($final_res)){
								echo '<p class="measure_name">Final result</p><select class="form-control" name="final_result">';
								foreach ($final_res as $key => $res) {
									echo '<option value="'.$res->fr_id.'">'.$res->fr_name_en.'</option>';
								}
								echo '</select><br/>';
							}
							echo '</div>
							<div class="col-md-6 col-sm-6 col-xs-12">';
							echo '<p class="measure_name">percent:% </p><input class="form-control" type="number" name="final_result_percent" min="1"max="100"  step="any" placeholder="100"/>';
							echo '</div></div>';
							echo '<p class="measure_name">Notes:<p>'.form_textarea('notes','','class="form-control"').'<br>';
							
							echo form_submit('send','Send','class="btn btn-primary btn-block"');
							echo form_close();
						}
						
						?>
       </div>
	</div>
</div>

<script src="<?php echo base_url() ?>files/public/js/jquery-1.10.2.js"></script>