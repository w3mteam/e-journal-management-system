<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Journals</h2>
						<?php 
						echo $message;
						if (isset($paper)){
							echo form_open('paper/edit/'.$paper->paper_id);
							?>
							<br>
							<div class="row">
								<div class="col-sm-6">
									Title: <input type="text" class="form-control" value="<?php echo $paper->paper_title; ?>" name="title"/>
								</div>
								<div class="col-sm-6">
									Type:
									<?php 
									$options = array(
									                  'PAPER'  => 'Paper',
									                  'JOURNAL'    => 'Journal'
									                );
									echo form_dropdown('type', $options, $paper->paper_type,'class="form-control"');?>
								</div>
							</div>
							Description:<br />
							<textarea class="form-control" name="description" ><?php echo $paper->paper_description; ?></textarea>
							
							
							<input class="btn btn-primary btn-block" type="submit" value="Edit" /><br />
							<?php 
							echo form_close();
							echo validation_errors();
						}
						
						?>
					</div><!--features_items-->
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>
