
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Paper Judgment Result</h2>
						<?php for ($i=0; $i <sizeof($paper_result) ; $i++) { ?>
							<div class="step-one">
								<h2 class="heading"><?php echo '<p>Final Result for judge ('.$i.') =  <b>'.$paper_result[$i]['final_result']->fr_name_en."</b> with value: <b>".$paper_result[$i]['final_result']->jr_final_result_rank ;?></b></p></h2>
							</div>
							<div  class="table-responsive">
			                    <table style="border: none;" class="table table-hover">
									<thead>
			                            <tr>
											<th>Measures Name</th>
											<th>Result</th>
										</tr>
			                        </thead>
			                        <tbody>
			                <?php
							$measures = $paper_result[$i]['measures'];
							for ($j=0; $j < sizeof($measures) ; $j++){ ?>
								<tr>
									<td><?php echo $measures[$j]->jm_name_en ;?></td>
									<td><?php echo $measures[$j]->rl_name_en ;?></td>
								</tr>
							<?php }	
							
							?>
								 <tbody>
						 	</table>
						 	<?php 
						 	$notes = $paper_result[$i]['notes'];
							
							if(!empty($notes)){
								echo "<b>Notes</b><br><ul>";
								foreach ($notes as $key => $note) {
									echo '<li>'.$note->pr_review_text.'</li>';	
								}
								echo '</ul>';
							}
						 	?>
	                		</div>
	                		
						<?php } ?>
						
						 
					</div>
					<br />
					<?php $this->load->view('template/added_recently.php');?>
				</div>
				
			</div>
		</div>
</section>
