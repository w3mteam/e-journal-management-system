<!--
<?php echo "hello home, ";?>
login :
<?php echo anchor('account','Here');?>
-->
<section>
		<div class="container">
			<div class="row">
				<?php $this->load->view('template/left_side_bar.php');?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Upload File</h2>
						<?php echo $message; ?>
						<?php echo form_open_multipart('paper/upload_new_file/'.$id);?>
						<div class="row">
							<div class="col-sm-6">
								<?php echo '<div style="margin-top:15px;">'.form_upload('file','','class="form-control"').'</div>'; ?>
							</div>
							<div class="col-sm-6">
								<?php echo form_submit('upload','Upload','class="btn btn-primary btn-block"'); ?>
							</div>
						</div><br />
						<?php echo form_close();?>
					</div><!--features_items-->
					<?php $this->load->view('template/added_recently.php');?>
				</div>
			</div>
		</div>
</section>


