

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>files/public/images/Create_new_Group.png"  />Paper list</p>
        </div>
		<div class="panel-body">
			<section id="cart_items">
					<?php if(isset($message)) echo $message.'<br>';
					if(!empty($papers)){ ?>
					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="image">Paper</td>
									<td class="description"></td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($papers as $key => $paper) { ?>
									<tr>
										<td class="cart_product">
											<?php if($paper->paper_type == 'PAPER') { ?>
												<img src="<?php echo base_url();?>files/public/images/home/paper_added_recently.jpg" alt="" />
											<?php } else if($paper->paper_type == 'JOURNAL'){  ?>
												<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
											<?php } else{ ?>
												<img src="<?php echo base_url();?>files/public/images/home/journal_added_recently.jpg" alt="" />
											<?php } ?>
											
										</td>
										<td class="cart_description">
											<h4><a href=""><?php echo anchor(base_url().'files/private/'.$paper->paper_type.'/'.$paper->paper_file,$paper->paper_title);?></a></h4>
											<p><small class="text-muted"><?php echo $paper->paper_creation_date ;?></small></p>
											<p><small class="text-muted" style="color: red;"><?php echo $paper->st_name ;?></small></p>
										</td>
										<td class="cart_price">
											<p>
												<?php
												
													?><a onclick="return confirm('Are you sure want to delete');" href="<?php echo base_url(); ?>index.php/paper/delete/<?php echo $paper->paper_id;?>">Delete</a><?php
													
													echo ' | ';
													echo anchor('paper/view_paper_assigned_judges/'.$paper->paper_id,'assigned judges');
												?>
											</p>
										</td>
										
									</tr>
								<?php } ?>
								
							</tbody>
						</table>
					</div>
					<?php } else {
						echo '<center><p id="msg">there is no papers to view.</p></center>';
					}
					?>
			</section> <!--/#cart_items-->
       </div>
	</div>
</div>


