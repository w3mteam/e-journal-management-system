<?php $this->load->helper('privileges');
//var_dump(get_user_privileges());die();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-journal Cpanel</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>files/public/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>files/public/css/style-cpanel.css" rel="stylesheet" />
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>files/public/css/sb-admin.css" rel="stylesheet">
    <!-- Page Specific CSS -->
    <script src="<?php echo base_url();?>files/public/js/jquery-2.1.0.min.js" type="text/javascript" ></script>
    <script src="<?php echo base_url();?>files/public/js/bootstrap.js"></script>
  </head>
  <body>
    <div id="wrapper">
      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>index.php/auth/index">E-journal Cpanel</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav navbar-right navbar-user">
           		
            	<?php $user_id=$this->session->userdata('user_id');
                      $username=$this->session->userdata('username');?>
                      
                <!-- Notification -->
                <?php $this->load->helper("notification_helper"); 
					  //$this->load->view('notifications_js');
                	  $notification_count=get_number_not_seen($user_id);	
					  $notifications_not_seen=get_unseen_notifications($user_id);		
                ?>
                
                <li  id="notifications" class="dropdown"><a class="dropdown-toggle notifications" data-toggle="dropdown" href="#"><strong>Notifications <div id="notification_count"><?php echo $notification_count;?></div>  </strong> <span id="notification-badge" class="badge badge-important"></span></a>  
		          <ul  class="dropdown-menu" role="menu" aria-labelledby="dLabel">
		          	<?php 
		          	if($notifications_not_seen){
						foreach ($notifications_not_seen as $key => $noti) {?>
						<li> 
		          	 		<div class="noti">
		          	 			<div style="float:left"><?php echo $noti->paper_title; ?></div>
		          	 			<div style="float: right"><?php echo $noti->not_pushing_date; ?> </div>
		          	 			<div style="clear:both"></div>
		          	 			<div class="notification_text"><?php  echo $noti->not_text; ?></div>
		          	 		</div> 
			          	 	<hr />
			          	 </li>  
						<?php }}else{ ?>
							<div class="noti">
								<li> there is no new notifications </li>
							</div>
							<hr />
						<?php }?>
						<li style="alignment-adjust: central"> <?php echo anchor('notification/see_all','See all notifications','');?></li>
		          </ul>  
		        </li>  
		        
                <!-- Notification -->
                <li><?php echo  anchor("auth/index/$user_id",$username); ?></li>
            	<li><?php echo  anchor("account/logout",'logout'); ?></li>
          </ul>
          <ul class="nav navbar-nav side-nav">
            <!-- <li <?php if(uri_string()=='account/index'){?>class="active"><?php } else {?> > <?php }?> <a href="<?php echo base_url();?>index.php/account/index"><img src="<?php echo base_url();?>files/public/images/show_user.png" />Manage Users</a></li>
            <li <?php if(uri_string()=='paper/show'){?>class="active"><?php } else {?> > <?php }?> <a href="<?php echo base_url();?>index.php/paper/show"><img src="<?php echo base_url();?>files/public/images/manage_paper	.png" />Manage Papers</a></li>
            <li <?php if(uri_string()=='paper/assigned_papers'){?>class="active"><?php } else {?> > <?php }?> <a href="<?php echo base_url();?>index.php/paper/assigned_papers"><img src="<?php echo base_url();?>files/public/images/assign.png" />Assinged Papers</a></li>
         	-->
         	<?php 
          		
          			$flag = TRUE;
          			$cat = get_user_privileges();
					
					foreach ($cat as $cat_value) {
						if($cat_value != false){
							if(!$flag){echo '</ul></li>';}
							$flag = TRUE;
				          	foreach ($cat_value['priv'] as $priv_value) {
				          		if($cat_value['cat']->id == $priv_value->id_cat){
				          			if($flag){ $flag = FALSE; ?>
				          				<li class="dropdown">
							              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
							              	<i class="fa fa-caret-square-o-down"></i> <img src="<?php echo base_url();?>files/public/images/<?php echo $cat_value['cat']->img;?>" /><?php echo $cat_value['cat']->cat_en;?> <b class="caret"></b>
							              </a>
							              <ul class="dropdown-menu">
							       		  <?php } ?>
					              		 <li <?php if(uri_string()== $priv_value->controller_name.'/'.$priv_value->func){?>class="active"><?php } else {?> > <?php }?><a href="<?php echo base_url();?>index.php/<?php echo $priv_value->controller_name;?>/<?php echo $priv_value->func;?>"><img src="<?php echo base_url();?>files/public/images/<?php echo $priv_value->img;?>" /><?php echo $priv_value->privilage_en;?></a></li>              
							       <?php   
			          			}
							}
						}
					}
				
			    ?>
          </ul>

          
        </div><!-- /.navbar-collapse -->
      </nav>

     
    </div><!-- /#wrapper -->

<!-- Notifications  js -->

    <script>
	var user_id="<?php  echo $this->session->userdata('user_id');?>";
	var base_url="<?php echo  base_url();?>";
	
	$('#notifications').click(function(){
		$.ajax({
			url:base_url+"index.php/notification/seen",
			data:{"user_id" : user_id} ,
			type:"post",
			success:function(response){
				//console.log(response);
				$('#notification_count').html('');
			},
			error:function(){
				//console.log('errore')
			}
		});
	});

	function check_updates(){
		$.ajax({
			url:base_url+"index.php/notification/get_unseen_notifications",
			formdata:{"user_id" : user_id} ,
			type:"post",
			success:function(response){
				//console.log(response);
				data=Json.parse(response);
				for (var i=0; i < data.length; i++) {
				  $('#notifications').prepend('<div id=noty style="border:solid 2px red"><div id=noty_text>'+data[i].not_text+'</div><div id=noty_date>'+data[i].not_pushing_date+'</div></div>')
				};	
			},
			error:function(){
				console.log('error')
			}
		});
	}
	
	//var intervalId = setInterval(check_updates,1000); 
</script>

<!-- Notifications  js -->