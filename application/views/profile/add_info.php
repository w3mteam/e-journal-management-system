
<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>

<script type="text/javascript">
	function displayRes(item, val, text) {
	    $('#selecteditems').append('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" value="'+text+'" type="hidden" /></p></div>');
	    $('#demo1').val('');
	}
	function removeparent(e){
		e.parentNode.remove();
		var p = parseInt(e.parentNode.lastChild.textContent);
	}
	$(document).ready(function () {
		var jsonData = <?php echo $concept;?>;
		src = []; 
		for (var i = 0; i < jsonData.length; i++) {
			src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
		}
	    $('#demo1').typeahead({
	        source:src,
	        itemSelected: displayRes
	    });
		
	});
</script>


<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p><img src="<?php echo base_url(); ?>files/public/images/Control_panel_judgment.png"  />Add Info</p>
        </div>
		<div class="panel-body">
        	<?php echo form_open('profile/add_information'); ?>
        	<div class="row">
        		<div class="co-md-6 col-sm-6 col-xs-12">
        			<label>First Name:</label>
					<?php echo form_input('first_name',set_value('first_name'),'class="form-control"'); ?>
        		</div>
        		<div class="co-md-6 col-sm-6 col-xs-12">
        			<label>Last Name:</label>
					<?php echo form_input('last_name',set_value('last_name'),'class="form-control"'); ?>
        		</div>
        	</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<label>Specialization In</label>
					<div id="scrollable-dropdown-menu">
						<?php echo form_input('specialization',set_value('specialization'),'id="demo1" class="form-control"'); ?><br />
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="selecteditems">
						
					</div>
				</div>
				
			</div>
			<?php echo form_submit('add','Add','class="btn btn-primary btn-block"'); ?>
			<?php echo form_close(); ?>
			<?php echo validation_errors(); ?>

       </div>
	</div>
</div>


