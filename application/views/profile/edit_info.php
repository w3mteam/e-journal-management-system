

<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>

<script type="text/javascript">
	function displayRes(item, val, text) {
	    $('#selecteditems').append('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" value="'+text+'" type="hidden" /></p></div>');
	    $('#demo1').val('');
	}
	function removeparent(e){
		e.parentNode.remove();
		var p = parseInt(e.parentNode.lastChild.textContent);
	}
	$(document).ready(function () {
		var jsonData = <?php echo $concept;?>;
		src = []; 
		for (var i = 0; i < jsonData.length; i++) {
			src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
		}
	    $('#demo1').typeahead({
	        source:src,
	        itemSelected: displayRes
	    });
		
		specs='<?php echo json_encode($specialization); ?>';
		specs=JSON.parse(specs);
		// console.log("specs");
		console.log(specs);
		for (var i=0; i < specs.length; i++) {
			//alert(specs[i].spe_id);
		  edit_specs(specs[i].spe_id,specs[i].name);
		};
		
	});
		// edit specialization
		function edit_specs(val, text) {
			
		    $('#selecteditems').html('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" value="'+text+'" type="hidden" /></p></div>');  
   			// $("#selecteditems").collapsibleset();
   		}
		
			
</script>
		



<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p><img src="<?php echo base_url(); ?>files/public/images/Control_panel_judgment.png"  />Control panel judgment</p>
        </div>
		<div class="panel-body">
        	<div class="row">
			<?php echo form_open('profile/edit_profile'); ?>
			<label>First Name:</label>
			<?php echo form_input('first_name',$profile_info->first_name,''); ?><br />
			<label>Last Name:</label>
			<?php echo form_input('last_name',$profile_info->last_name,''); ?><br />
			<label>Specialization In</label>
			<div id="scrollable-dropdown-menu">
				<?php echo form_input('specialization',set_value('specialization'),'id="demo1"'); ?><br />
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th><center><span class="span1">Remove Item</span></center></th>
							<th><center><span class="span1">Speciality name</span></center></th>
						</tr>
					</thead>
					<tbody id="selecteditems">
					
					</tbody>
				</table>
			</div>
		</div>
		<?php echo form_submit('edit','Edit'); ?>
		<?php echo form_close(); ?>
		<?php echo validation_errors(); ?>
       </div>
	</div>
</div>
