
	<head>
	<script src="<?php echo base_url();?>files/public/js/jquery-1.8.3.min.js"></script>	<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>
		
		<script type="text/javascript">
			function displayRes1(item, val, text) {
			    $('#selecteditems1').append('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" class="pub_activeinput" value="'+text+'" type="hidden" /></p></div>');
			    $('#demo1').val('');
			}
			function displayRes2(item, val, text) {
				
			    $('#selecteditems2').append('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" class="pro_activeinput" value="'+text+'" type="hidden" /></p></div>');
			    $('#demo2').val('');
			}
			function displayRes3(item, val, text) {
			    $('#selecteditems3').append('<div id="style_concept"><p><a style="color:red;margin-right:3px;text-decoration: none;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a>' + text + '<input name="'+val+'" class="course_activeinput" value="'+text+'" type="hidden" /></p></div>');
			    $('#demo3').val('');
			}
			function removeparent(e){
				e.parentNode.remove();
				var p = parseInt(e.parentNode.lastChild.textContent);
			}
			$(document).ready(function () {
				var jsonData = <?php echo $concept;?>;
				src = []; 
				all_publication = [];
				all_projects = [];
				all_courses = [];
				for (var i = 0; i < jsonData.length; i++) {
					src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
				}
			    $('#demo1').typeahead({
			        source:src,
			        itemSelected: displayRes1
			    });
			   $('#demo2').typeahead({
			        source:src,
			        itemSelected: displayRes2
			    });
			    $('#demo3').typeahead({
			        source:src,
			        itemSelected: displayRes3
			    });
			    /*********************************************************************/
			    /********************************Publication**************************/
			    /*********************************************************************/
			    $('#add_new_publications').click(function(){
			    	var pub_data = {};
			    	var publications = {}; 
					$(".pub_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pub_data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = JSON.stringify(pub_data);
					all_publication.push(publications);
					$('#selecteditems1').html('');
					$('.pub_activeinput').attr('value','');
					console.log(all_publication);
			    });
			    /*********************************************************************/
			    /********************************Project******************************/
			    /*********************************************************************/
			    $('#add_new_projects').click(function(){
			    	var pro_data = {};
			    	var projects = {}; 
					$(".pro_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pro_data[name] = value;}
						else{
							projects[name]=value;
						}
					});
					projects['pro_domain'] = JSON.stringify(pro_data);
					all_projects.push(projects);
					$('#selecteditems2').html('');
					$('.pro_activeinput').attr('value','');
					console.log(all_projects);
			    });
			    /*********************************************************************/
			    /********************************Course*******************************/
			    /*********************************************************************/
			    $('#add_new_courses').click(function(){
			    	var course_data = {};
			    	var courses = {}; 
					$(".course_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {course_data[name] = value;}
						else{
							courses[name]=value;
						}
					});
					courses['course_domain'] = JSON.stringify(course_data);
					all_courses.push(courses);
					$('#selecteditems3').html('');
					$('.course_activeinput').attr('value','');
					console.log(all_courses);
			    });
				$('form#add').on('submit', function(){
					/*********************************************************************/
				    /********************************Publication**************************/
				    /*********************************************************************/
					var pub_data = {};
			    	var publications = {}; 
					$(".pub_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pub_data[name] = value;}
						else{
							publications[name]=value;
						}
					});
					publications['pub_domain'] = JSON.stringify(pub_data);
					all_publication.push(publications);
					$('#selecteditems1').html('');
					/*********************************************************************/
				    /********************************Project******************************/
				    /*********************************************************************/
				    var pro_data = {};
			    	var projects = {}; 
					$(".pro_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {pro_data[name] = value;}
						else{
							projects[name]=value;
						}
					});
					projects['pro_domain'] = JSON.stringify(pro_data);
					all_projects.push(projects);
					$('#selecteditems2').html('');
					/*********************************************************************/
			    	/********************************Course*******************************/
			    	/*********************************************************************/
			   		var course_data = {};
			    	var courses = {}; 
					$(".course_activeinput").each(function() {
						var obj = $(this),
						name = obj.attr('name'),
    					value = obj.val();
						if($(obj).is(':hidden')) {course_data[name] = value;}
						else{
							courses[name]=value;
						}
					});
					courses['course_domain'] = JSON.stringify(course_data);
					all_courses.push(courses);
					$('#selecteditems3').html('');
					/*********************************************************************/
				    /********************************Ajax*********************************/
				    /*********************************************************************/
				    $.ajax({ 
					      type: "POST",
					      url: '<?php echo base_url();?>index.php/profile/add_jobs',
					      datatype: "json",
					      traditional: true,
					      data: {pub_json:JSON.stringify(all_publication),pro_json:JSON.stringify(all_projects),course_json:JSON.stringify(all_courses)}, 
					      success: function (response) {
					      	console.log(response);
					      	$('.all').html('');
					      	$('.all').append('<p>Information Added Successfully.</p>');
					      }          
					 });
					 return false; //disable refresh
				});
			    
			});
		</script>
	</head>


<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
                <p><img src="<?php echo base_url(); ?>files/public/images/Control_panel_judgment.png"  />Control panel judgment</p>
        </div>
		<div class="panel-body">
        	<div class="all">
			<?php echo form_open('profile/add_jobs','id= "add"'); ?>
			<!------------------------------------------------------------>
			<!--------------------------Publications---------------------->
			<!------------------------------------------------------------>
			<div id="i_f_pub"><h4 id="style_title">Publications</h4><br />
			<div class="input_fields_publication" >
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Publication Name</label>
							<?php echo form_input('pub_name',set_value('pub_name'),'class="pub_activeinput form-control"');?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Publication Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pub_domain',set_value('pub_domain'),'id="demo1" class="pub_activeinput form-control"'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Publication Details</label><br />
							<textarea name="pub_details" class="pub_activeinput form-control" rows="4"></textarea>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12"><div id="selecteditems1"></div></div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Publication Date</label>
							<input type="date" name="pub_date" class="pub_activeinput form-control" />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a href="#" id="add_new_publications" class="btn btn-success btn-block">Add New Publications</a>
						</div>
					</div>
				</div>
			</div></div>
			<br />
			<!------------------------------------------------------------>
			<!--------------------------Projects-------------------------->
			<!------------------------------------------------------------>
			<div id="i_f_pro"><h4 id="style_title">Project</h4><br />
			<div class="input_fields_project" >
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Project Name</label>
							<?php echo form_input('pro_name',set_value('pro_name'),'class="pro_activeinput form-control"');?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Project Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('pro_domain',set_value('pro_domain'),'id="demo2" class="pro_activeinput form-control"'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Project Details</label><br />
							<textarea name="pro_details" class="pro_activeinput form-control" rows="4"></textarea>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12"><div id="selecteditems2"></div></div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Project Date</label>
							<input type="date" name="pro_date" class="pro_activeinput form-control" />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a href="#i_f_pro" id="add_new_projects" class="btn btn-success btn-block">Add New Projects</a>
							
						</div>
					</div>
				</div>
			</div>
			</div>
			<br />
			<!------------------------------------------------------------>
			<!--------------------------Courses--------------------------->
			<!------------------------------------------------------------>
			<div id="i_f_cour"><h4 id="style_title">Course</h4><br />
			<div class="input_fields_course" >
				<div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Course Name</label>
							<?php echo form_input('course_name',set_value('course_name'),'class="course_activeinput form-control"');?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Course Domain</label>
							<div id="scrollable-dropdown-menu">
								<?php echo form_input('course_domain',set_value('course_domain'),'id="demo3" class="course_activeinput form-control"'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Course Details</label><br />
							<textarea name="course_details" class="course_activeinput form-control" rows="4"></textarea>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12"><div id="selecteditems3"></div></div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>Course Date</label>
							<input type="date" name="course_date" class="course_activeinput form-control" />
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a href="#i_f_cour" id="add_new_courses" class="btn btn-success btn-block">Add New Courses</a>
							
						</div>
					</div>
				</div>
				
			</div>
			</div>
			<br />
			<?php echo form_submit('Save','Add','class="btn btn-primary btn-block"');?>
		</div>
       </div>
	</div>
</div>



