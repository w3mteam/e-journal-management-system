<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Paper management library
 */
class Inferene_model_lib {
	private $ci;
	private $proj_config;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->library('support_lib');
		$this->ci->load->model('inference_model');
		$this->ci->config->load('project_config', TRUE);
		$this->proj_config = $this->ci->config->item('project_config');
		//echo "<br/>". $this->proj_config['project_weight'];
	}
	/*//تحوي مصفوفة تضم كل بروفايل وعدد كل قسم فيه (مثلاً برفايل 1 فيو بقسم الأبحاث العلمية 3 أبحاث, وبقسم المشاريع يحوي مشروع 1 ولديه 3 كورسات )
	public function number_each_section ($value='')
	{
		$sections_num=array(array());
		$all_profile = $this->ci->inference_model->get_all_profile();
		foreach ($all_profile as $key => $value) {
			$profile_id[]	= $value->profile_id;
			$all_publication[] = $this->ci->inference_model->get_all_publication($value->profile_id);
			$all_projects[]	 = $this->ci->inference_model->get_all_project($value->profile_id);
			$all_courses[]	 = $this->ci->inference_model->get_all_course($value->profile_id);
			$sections_num=array
			  (
			  	array("Profile_ID","Publications_Num","Projects_num","Courses_num"),
			  	array($profile_id,$all_publication,$all_projects,$all_courses)
			  );
		}
		//var_dump($sections_num);
		//echo "<br/>". max($sections_num[1][1]);//return max publication
		return $sections_num;
	}
	public function get_max_publication($value='')
	{
		$sections_num = $this->number_each_section();
		return max($sections_num[1][1]);
	}
	public function get_max_project($value='')
	{
		$sections_num = $this->number_each_section();
		return max($sections_num[1][2]);
	}
	public function get_max_course($value='')
	{
		$sections_num = $this->number_each_section();
		return max($sections_num[1][3]);
	}*/
	//تحوي مصفوفة تضم كل مفهوم مع البروفايل الذي يحويه  وتكرار كل مفهوم ضمن كل قسم  (مثلا المفهوم 1 ضمن البرفايل 1 يحوي على بحثين ومشروع واحد وكورس واحد )
	public function frequency_concepts_each_section ($value='')
	{
		$number_concepts_each_sec	=array(array());
		$all_valid_concept 			= $this->ci->inference_model->get_all_valid_concept();
		$all_profile 				= $this->ci->inference_model->get_all_profile();
		foreach ($all_valid_concept as $concept_key => $concept_value) {
			foreach ($all_profile as $profile_key => $profile_value) {
				$concept_id[] 	= $concept_value->concept_id;
				$profile_id[]	= $profile_value->profile_id;
				$concept_within_publications_foreach_profile[] 	= $this->ci->inference_model->get_all_concept_within_publications_foreach_profile($profile_value->profile_id,$concept_value->concept_id);
				$concept_within_projects_foreach_profile[] 		= $this->ci->inference_model->get_all_concept_within_projects_foreach_profile($profile_value->profile_id,$concept_value->concept_id);
				$concept_within_courses_foreach_profile[] 		= $this->ci->inference_model->get_all_concept_within_courses_foreach_profile($profile_value->profile_id,$concept_value->concept_id);
				
			}
		}
		$number_concepts_each_sec	=	array
		  (
		  	array("Concept_Id","Profile_Id","Publications_Num","Projects_num","Courses_num"),
		  	array($concept_id,$profile_id,$concept_within_publications_foreach_profile,$concept_within_projects_foreach_profile,$concept_within_courses_foreach_profile)
		  );
		//var_dump($number_concepts_each_sec);
		echo "<br/>". "frequency_concepts_each_section";
		return $number_concepts_each_sec;
	}
	//الحصول على أكبر قيمة لعدد الأبحاث العلمية التي تضم مفهوم ما
	private function get_max_concept_within_publication($concept_id)
	{
		$number_concepts_each_sec = $this->frequency_concepts_each_section();
		$pub = array();
		for ($i=0; $i < sizeof($number_concepts_each_sec[1][0]) ; $i++) { 
			if($number_concepts_each_sec[1][0][$i] == $concept_id){
				$pub[] = $number_concepts_each_sec[1][2][$i];
			}
		}
		echo "<br/>". "get_max_concept_within_publication";
		return max($pub);
	}
	//الحصول على أكبر قيمة لعدد المشاريع التي تضم مفهوم ما
	private function get_max_concept_within_project($concept_id)
	{
		$number_concepts_each_sec = $this->frequency_concepts_each_section();
		$pro = array();
		for ($i=0; $i < sizeof($number_concepts_each_sec[1][0]) ; $i++) { 
			if($number_concepts_each_sec[1][0][$i] == $concept_id){
				$pro[] = $number_concepts_each_sec[1][3][$i];
			}
		}
		echo "<br/>". "get_max_concept_within_project";
		return max($pro);
	}
	//الحصول على أكبر قيمة لعدد الكورسات التي تضم مفهوم ما
	private function get_max_concept_within_course($concept_id)
	{
		$number_concepts_each_sec = $this->frequency_concepts_each_section();
		$course = array();
		for ($i=0; $i < sizeof($number_concepts_each_sec[1][0]) ; $i++) { 
			if($number_concepts_each_sec[1][0][$i] == $concept_id){
				$course[] = $number_concepts_each_sec[1][4][$i];
			}
		}
		echo "<br/>". "get_max_concept_within_course";
		return max($course);
	}
	//الحصول على عدد الأبحاث العلمية ضمن بروفايل معين وذلك حسب مفهوم محدد
	public function get_num_publication_within_prof_for_concept($concept_id,$profile_id)
	{
		$number_concepts_each_sec 	= $this->frequency_concepts_each_section();
		$concept_number  			= sizeof($number_concepts_each_sec[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($number_concepts_each_sec[1][0][$i] == $concept_id)&&($number_concepts_each_sec[1][1][$i] == $profile_id)){
				//echo "<br/>". $number_concepts_each_sec[1][2][$i];
				echo "<br/>". "get_num_publication_within_prof_for_concept";
			 	return $number_concepts_each_sec[1][2][$i];
			}
		}
		
	}
	//الحصول على عدد المشاريع ضمن بروفايل معين وذلك حسب مفهوم محدد
	public function get_num_project_within_prof_for_concept($concept_id,$profile_id)
	{
		$number_concepts_each_sec 	= $this->frequency_concepts_each_section();
		$concept_number  			= sizeof($number_concepts_each_sec[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($number_concepts_each_sec[1][0][$i] == $concept_id)&&($number_concepts_each_sec[1][1][$i] == $profile_id)){
				//echo "<br/>". $number_concepts_each_sec[1][3][$i];
				echo "<br/>". "get_num_project_within_prof_for_concept";
			 	return $number_concepts_each_sec[1][3][$i];
			}
		}
	}
	//الحصول على عدد الكورسات ضمن بروفايل معين وذلك حسب مفهوم محدد
	public function get_num_course_within_prof_for_concept($concept_id,$profile_id)
	{
		$number_concepts_each_sec 	= $this->frequency_concepts_each_section();
		$concept_number  			= sizeof($number_concepts_each_sec[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($number_concepts_each_sec[1][0][$i] == $concept_id)&&($number_concepts_each_sec[1][1][$i] == $profile_id)){
				//echo "<br/>". $number_concepts_each_sec[1][4][$i];
				echo "<br/>". "get_num_course_within_prof_for_concept";
			 	return $number_concepts_each_sec[1][4][$i];
			}
		}
	}
	//calculate idf value for every concept;
	private function idf_calculate($cpncept_id)
	{
		$log_2	= log(2.71828183);
		$tf		= 0;
		$N		= 0;
		$all_profile = $this->ci->inference_model->get_all_profile();
		foreach ($all_profile as $key => $value) {
			$concept_presence_within_pub	= $this->ci->inference_model->get_concept_presence_within_pub($cpncept_id,$value->profile_id);
			$concept_presence_within_pro	= $this->ci->inference_model->get_concept_presence_within_pro($cpncept_id,$value->profile_id);
			$concept_presence_within_course	= $this->ci->inference_model->get_concept_presence_within_course($cpncept_id,$value->profile_id);
			$tf							    = $tf + $concept_presence_within_pub + $concept_presence_within_pro + $concept_presence_within_course;
		}
		foreach ($all_profile as $key => $value) {
			$profile_has_pub	= $this->ci->inference_model->profile_has_pub($value->profile_id);
			$profile_has_pro	= $this->ci->inference_model->profile_has_pro($value->profile_id);
			$profile_has_course	= $this->ci->inference_model->profile_has_course($value->profile_id);
			$N				    = $N+$profile_has_pub+$profile_has_pro+$profile_has_course;
		}
		if($tf != 0){
			$log_res	= log($N/$tf);
			$idf		= $log_res/$log_2;
			//var_dump($idf);
			echo "<br/>". "idf_calculate";
			return $idf;
		}
		
	}
	//get idf for all concept
	private function get_idf_all_concept()
	{
		$idf_array 			= array(array());
		$all_valid_concept 	= $this->ci->inference_model->get_all_valid_concept();
		foreach ($all_valid_concept as $key => $value) {
			$concept_id[]	= $value->concept_id;
			$idf[] 			= $this->idf_calculate($value->concept_id);
			$idf_array 		= array(
				array("ID_concept","idf"),
				array($concept_id,$idf)
			);
		}
		//var_dump($idf_array);
		echo "<br/>". "get_idf_all_concept";
		return $idf_array;
	}
	//get Max idf from function (get_idf_all_concept())
	private function get_max_idf()
	{
		$concept_idf 	= $this->get_idf_all_concept();
		$idf 			= $concept_idf[1][1];
		echo "<br/>". "get_max_idf";
		return max($idf);
	}
	//get "concept_id" , "idn" , "nidf" for all Concept
	public function get_idf_nidf_all_concept()
	{
		$concept_idf 	= $this->get_idf_all_concept();
		$idf 			= $concept_idf[1][1];
		$concept_id		= $concept_idf[1][0];
		$idf_nidf_array = array(array());
		$max_nidf		= $this->get_max_idf();
		for ($i=0; $i < sizeof($idf) ; $i++) { 
			$nidf[]			= $idf[$i]/$max_nidf;
		}
		$idf_nidf_array = array(
				array("Concept_id","idf","nidf"),
				array($concept_id,$idf,$nidf)
		);
		//var_dump($idf_nidf_array);
		echo "<br/>". "get_idf_nidf_all_concept";
		return $idf_nidf_array;
	}
	public function get_nidf($concept_id)
	{
		$idf_nidf_all_concept = $this->get_idf_nidf_all_concept();
		for ($i=0; $i < sizeof($idf_nidf_all_concept[1][0]); $i++) { 
			if($idf_nidf_all_concept[1][0][$i] == $concept_id){
				//echo "<br/>". $idf_nidf_all_concept[1][2][$i];
				echo "<br/>". "get_nidf";
				return $idf_nidf_all_concept[1][2][$i];
			}
		}
	}
	public function get_initialize_table()
	{
		$max_concept_within_each_section 	= array(array());
		$freq_concepts_each_section 		= $this->frequency_concepts_each_section();
		//var_dump($freq_concepts_each_section);die();
		$all_valid_concept 					= $this->ci->inference_model->get_all_valid_concept();
		foreach ($all_valid_concept as $key => $value) {
			$concept_id[] 			= $value->concept_id;
			$max_value_pub[] 		= $this->get_max_concept_within_publication($value->concept_id) ;
			$max_value_pro[] 		= $this->get_max_concept_within_project($value->concept_id) ;
			$max_value_course[] 	= $this->get_max_concept_within_course($value->concept_id) ;
			
		}
		$max_concept_within_each_section = array(
			array("Concept_id","max_publication","max_project","max_course"),
			array($concept_id,$max_value_pub,$max_value_pro,$max_value_course)
		);
		
		$number_concepts_each_sec = array(array());
		$max_val_pub			  = 0;
		$max_val_pro			  = 0;
		$max_val_course			  = 0;
		
		for ($j=0; $j < sizeof($freq_concepts_each_section[1][0]) ; $j++) {
			$con_id[] 	= $freq_concepts_each_section[1][0][$j];
			$prof_id[]	= $freq_concepts_each_section[1][1][$j];
			for ($i=0; $i < sizeof($max_concept_within_each_section[1][0]) ; $i++) { 
				if($max_concept_within_each_section[1][0][$i] == $freq_concepts_each_section[1][0][$j]){
					$max_val_pub 	= $max_concept_within_each_section[1][1][$i];
					$max_val_pro 	= $max_concept_within_each_section[1][2][$i];
					$max_val_course = $max_concept_within_each_section[1][3][$i];
					break;
				}
			}
			if($max_val_pub !=0 ){
				$pub[] 		= $freq_concepts_each_section[1][2][$j]/$max_val_pub;
			}else{
				$pub[]		= 0;
			}
			
			if($max_val_pro !=0 ){
				$pro[] 		= $freq_concepts_each_section[1][3][$j]/$max_val_pro;
			}else{
				$pro[]		= 0;
			}
			
			if($max_val_course !=0 ){
				$course[] 	= $freq_concepts_each_section[1][4][$j]/$max_val_course;
			}else{
				$course[] 	= 0;
			}
			
		}
		//var_dump($pub);die();
		$number_concepts_each_sec = array(
			array("Concept_Id","Profile_Id","Publications_Num","Projects_num","Courses_num"),
			array($con_id,$prof_id,$pub,$pro,$course)
		);
		//var_dump($number_concepts_each_sec);
		echo "<br/>". "get_initialize_table";
		return $number_concepts_each_sec;
	}

	//الحصول على قيمة  الأبحاث العلمية ضمن بروفايل معين وذلك حسب مفهوم محدد وذلك ضمن جدول الإنشلايز
	public function get_publication_val_within_init_table($concept_id,$profile_id)
	{
		$initialize_table 	= $this->get_initialize_table();
		$concept_number  	= sizeof($initialize_table[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($initialize_table[1][0][$i] == $concept_id)&&($initialize_table[1][1][$i] == $profile_id)){
				//echo "<br/>". $initialize_table[1][2][$i];
				echo "<br/>". "get_publication_val_within_init_table";
			 	return $initialize_table[1][2][$i];
			}
		}
	}
	//الحصول على قيمة المشاريع  ضمن بروفايل معين وذلك حسب مفهوم محدد وذلك ضمن جدول الإنشلايز
	public function get_project_val_within_init_table($concept_id,$profile_id)
	{
		$initialize_table 	= $this->get_initialize_table();
		$concept_number  	= sizeof($initialize_table[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($initialize_table[1][0][$i] == $concept_id)&&($initialize_table[1][1][$i] == $profile_id)){
				//echo "<br/>". $initialize_table[1][3][$i];
				echo "<br/>". "get_project_val_within_init_table";
			 	return $initialize_table[1][3][$i];
			}
		}
	}
	//الحصول على قيمة الكورساتة ضمن بروفايل معين وذلك حسب مفهوم محدد وذلك ضمن جدول الإنشلايز
	public function get_course_val_within_init_table($concept_id,$profile_id)
	{
		$initialize_table 	= $this->get_initialize_table();
		$concept_number  	= sizeof($initialize_table[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($initialize_table[1][0][$i] == $concept_id)&&($initialize_table[1][1][$i] == $profile_id)){
				//echo "<br/>". $initialize_table[1][4][$i];
				echo "<br/>". "get_course_val_within_init_table";
			 	return $initialize_table[1][4][$i];
			}
		}
	}
	public function link_Matrix_for_only_concept($concept_id)
	{
		$link_matrix_array	= array(array());
		$all_profile 		= $this->ci->inference_model->get_all_profile();
		$zero_point_five 	= 0.5;
		$pub_weight 		= $this->proj_config['publication_weight'];
		$pro_weight 		= $this->proj_config['project_weight'];
		$course_weight 		= $this->proj_config['course_weight'];
		$nidf 				= $this->get_nidf($concept_id);
		foreach ($all_profile as $profile_key => $profile_value) {
			$conc_id[] 		= $concept_id;
			$profile_id[]	= $profile_value->profile_id;
			$pub_ntf		= $this->get_publication_val_within_init_table($concept_id, $profile_value->profile_id);
			$pro_ntf		= $this->get_project_val_within_init_table($concept_id, $profile_value->profile_id);
			$course_ntf		= $this->get_course_val_within_init_table($concept_id, $profile_value->profile_id);
			if($pub_ntf > 0){
				$ntf 			= $pub_ntf;
				$second_part 	= $zero_point_five*$ntf*$nidf;
				$probapility 	= $zero_point_five+$second_part;
				$pub[] 			= $probapility*$pub_weight*$ntf;
			}else{
				$pub[]			= 0;
			}
			
			
			if($pro_ntf > 0){
				$ntf 			= $pro_ntf;
				$second_part 	= $zero_point_five*$ntf*$nidf;
				$probapility 	= $zero_point_five+$second_part;
				$pro[] 			= $probapility*$pro_weight*$ntf;
			}else{
				$pro[]			= 0;
			}
			
			if($course_ntf > 0){
				$ntf 			= $course_ntf;
				$second_part 	= $zero_point_five*$ntf*$nidf;
				$probapility 	= $zero_point_five+$second_part;
				$course[] 		= $probapility*$course_weight*$ntf;
			}else{
				$course[]		= 0;
			}
		}
		$link_matrix_array = array(
			array("Concept_Id","Profile_Id","Publication","Project","Course"),
			array($conc_id,$profile_id,$pub,$pro,$course)
		);
		//var_dump($link_matrix_array);
		echo "<br/>". "link_Matrix_for_only_concept";
		return $link_matrix_array;
	}
	public function delete_profile_final_result($value='')
	{
		$all_profile_final_result = $this->ci->inference_model->get_profile_final_result();
		if($all_profile_final_result != FALSE){
			foreach ($all_profile_final_result as $key => $value) {
				$this->ci->inference_model->delete_profile_final_result($value->pfr_id);
			}
		}
	}
	public function link_matrix($value='')
	{
		//$this->delete_profile_final_result();
		//ini_set('MAX_EXECUTION_TIME', -1);
		$all_valid_concept 	= $this->ci->inference_model->get_all_valid_concept();
		$start_date			= new DateTime($this->ci->support_lib->get_current_date());
		foreach ($all_valid_concept as $concept_key => $concept_value) {
			echo "<br/>". $concept_value->name;
			$link_matrix_array = $this->link_Matrix_for_only_concept($concept_value->concept_id);
			for ($i=0; $i < sizeof($link_matrix_array[1][0]); $i++) {
				$result = 0; 
				if($link_matrix_array[1][2][$i] > 0){
					$result = $link_matrix_array[1][2][$i];
				}
				if($link_matrix_array[1][3][$i] > 0){
					$result = $result + $link_matrix_array[1][3][$i];
				}
				
				if($link_matrix_array[1][4][$i] > 0){
					$result = $result + $link_matrix_array[1][4][$i];
				}
				if($result > 0){
					$result = $result/3;
					$data =  array('profile_id'=>$link_matrix_array[1][1][$i],'concept_id'=>$link_matrix_array[1][0][$i],'result'=>$result);
					$this->ci->inference_model->insert_profile_final_result($data);
				}
				
			}
		}
		$end_date	= new DateTime($this->ci->support_lib->get_current_date());
		$interval 	= $start_date->diff($end_date);
		echo "<br/>". 'Timmmmmmmmmmmmmmmmmmmmmmmmmme = '.$interval->format('%i');
	}
	
}