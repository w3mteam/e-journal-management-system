<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Paper management library
 */
class Search_engin_lib {
	private $ci;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->model('search_engin_model');
		$this->ci->load->library('support_lib');
	}
	public function get_initilize_table()
	{
		$publish_paper_and_concept = $this->ci->search_engin_model->get_publish_paper_and_concepts();
		$intiliaze_array 		   = array(array());
		foreach ($publish_paper_and_concept as $key => $value) {
			$concept_id[]	= $value->pc_concept_id;
			$paper_id[]		= $value->pc_paper_id;
			$max_concept	= $this->ci->search_engin_model->get_papers_num_according_to_specific_concept($value->pc_concept_id);
			$tf[]			= 1/$max_concept;
		}
		$intiliaze_array    = array(
			array("Concept_id","Paper_id",'Tf'),
			array($concept_id,$paper_id,$tf)
		);
		//var_dump($intiliaze_array);
		return $intiliaze_array;
	}
	public function idf_calculate($concept_id)
	{
		$log_2	= log(2);
		$tf		= 0;
		$N		= 0;
		$tf 	= $this->ci->search_engin_model->get_papers_num_according_to_specific_concept($concept_id);
		$N		= $this->ci->search_engin_model->get_num_publish_paper();
		$log_res	= log($N/$tf);
		$idf		= $log_res/$log_2;
		//var_dump($idf);
		return $idf;
	}
	//get idf for all concept
	public function get_idf_all_concept()
	{
		$idf_array 			= array(array());
		$all_valid_concept 	= $this->ci->search_engin_model->get_publish_concepts();
		foreach ($all_valid_concept as $key => $value) {
			$concept_id[]	= $value->pc_concept_id;
			$idf[] 			= $this->idf_calculate($value->pc_concept_id);
		}
		$idf_array 		= array(
			array("ID_concept","idf"),
			array($concept_id,$idf)
		);
		//var_dump($idf_array);
		return $idf_array;
	}
	//get Max idf from function (get_idf_all_concept())
	public function get_max_idf()
	{
		$concept_idf 	= $this->get_idf_all_concept();
		$idf 			= $concept_idf[1][1];
		//echo  max($idf);
		return max($idf);
	}
	//get "concept_id" , "idn" , "nidf" for all Concept
	public function get_idf_nidf_all_concept()
	{
		$concept_idf 	= $this->get_idf_all_concept();
		$idf 			= $concept_idf[1][1];
		$concept_id		= $concept_idf[1][0];
		$idf_nidf_array = array(array());
		$max_nidf		= $this->get_max_idf();
		for ($i=0; $i < sizeof($idf) ; $i++) { 
			$nidf[]		= $idf[$i]/$max_nidf;
		}
		$idf_nidf_array = array(
				array("Concept_id","idf","nidf"),
				array($concept_id,$idf,$nidf)
		);
		//var_dump($idf_nidf_array);
		return $idf_nidf_array;
	}
	public function get_nidf($concept_id)
	{
		$idf_nidf_all_concept = $this->get_idf_nidf_all_concept();
		for ($i=0; $i < sizeof($idf_nidf_all_concept[1][0]); $i++) { 
			if($idf_nidf_all_concept[1][0][$i] == $concept_id){
				//echo $idf_nidf_all_concept[1][2][$i];
				return $idf_nidf_all_concept[1][2][$i];
			}
		}
	}
	public function get_tf_val_within_init_table($concept_id,$paper_id)
	{
		$initialize_table 	= $this->get_initilize_table();
		$concept_number  	= sizeof($initialize_table[1][0]);
		for ($i=0; $i < $concept_number; $i++) {
			if(($initialize_table[1][0][$i] == $concept_id)&&($initialize_table[1][1][$i] == $paper_id)){
				//echo $initialize_table[1][2][$i];
			 	return $initialize_table[1][2][$i];
			}
		}
	}
	public function link_Matrix_for_only_concept($concept_id)
	{
		$nidf 				= $this->get_nidf($concept_id);
		$zero_point_five 	= 0.5;
		$link_matrix_array	= array(array());
		$all_paper 			= $this->ci->search_engin_model->get_publish_paper();
		foreach ($all_paper as $profile_key => $paper_value) {
			$conc_id[] 		= $concept_id;
			$paper_id[]		= $paper_value->paper_id;
			$ntf			= $this->get_tf_val_within_init_table($concept_id, $paper_value->paper_id);
			if($ntf > 0){
				$second_part 	= $zero_point_five*$ntf*$nidf;
				$probapility 	= $zero_point_five+$second_part;
				$ndf_value[] 	= $probapility;
			}else{
				$ndf_value[]			= 0;
			}
		}
		$link_matrix_array = array(
			array("Concept_Id","Paper_Id","Ntf"),
			array($conc_id,$paper_id,$ndf_value)
		);
		var_dump($link_matrix_array);
		return $link_matrix_array;
	}
	public function delete_paper_final_result($value='')
	{
		$all_paper_final_result = $this->ci->search_engin_model->get_paper_final_result();
		if($all_paper_final_result != FALSE){
			foreach ($all_paper_final_result as $key => $value) {
				$this->ci->search_engin_model->delete_paper_final_result($value->pfr_id);
			}
		}
	}
	public function link_matrix($value='')
	{
		$this->delete_paper_final_result();
		$all_valid_concept 	= $this->ci->search_engin_model->get_publish_concepts();
		$start_date			= new DateTime($this->ci->support_lib->get_current_date());
		foreach ($all_valid_concept as $concept_key => $concept_value) {
			$link_matrix_array = $this->link_Matrix_for_only_concept($concept_value->pc_concept_id);
			for ($i=0; $i < sizeof($link_matrix_array[1][0]); $i++) {
				$result = 0; 
				if($link_matrix_array[1][2][$i] > 0){
					$result = $link_matrix_array[1][2][$i];
				}
				if($result > 0){
					$data =  array('paper_id'=>$link_matrix_array[1][1][$i],'concept_id'=>$link_matrix_array[1][0][$i],'result'=>$result);
					$this->ci->search_engin_model->insert_paper_final_result($data);
				}
				
			}
		}
		$end_date	= new DateTime($this->ci->support_lib->get_current_date());
		$interval 	= $start_date->diff($end_date);
		//echo 'Timmmmmmmmmmmmmmmmmmmmmmmmmme = '.$interval->format('%i');
	}
	private function get_paper_for_original_keywords($keywords)
	{
		$paper = array();
		$first = true;
		//$keywords = array_unique($keywords, SORT_REGULAR);
		foreach ($keywords as $key => $keyword) {
			if($first){
				$paper = $this->ci->search_engin_model->get_paper_in_field($keyword);
				$first = false;
			}
			else {
				$tmp = $this->ci->search_engin_model->get_paper_in_field($keyword);
				if($tmp != null)
					$paper = array_intersect($paper,$tmp);	
			}
		}
		//var_dump(array_unique($paper, SORT_REGULAR));
		return $paper;
	}
	public function get_search_result($keywords,$n=0)
	{
		$get_orginal_paper = $this->get_paper_for_original_keywords($keywords);
		if(!empty($get_orginal_paper)){
			if($n > sizeof($keywords)){
				return FALSE;
			}
			return $this->get_search_result($this->replace_with_parent($keywords,$n+1),$n+1);
		}
		else {
			if($n > sizeof($keywords)){
				return FALSE;
			}
			return $this->get_search_result($this->replace_with_parent($keywords,$n),$n+1);
		}
	}
	private function replace_with_parent($keywords_array,$n)
	{
		
		$parent = $this->ci->search_engin_model->get_concept_parent($keywords_array[sizeof($keywords_array) -$n]);
		if(!$parent){
			return $keywords_array;
		}
		else {
			$keywords_array[sizeof($keywords_array) -$n] = $parent;
			return $keywords_array;
		}
		
	}
}