<?php

/**
 * 
 */
class Judgment_lib  {
	private $ci;
	private $user_id;
	private $profile_id;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->model('judgment_model');
		$this->ci->load->model('paper_model');
		$this->ci->load->library('ion_auth');
		$this->ci->config->load('journal_general_config');
		//$this->ci->load->library('support_lib');
		$this->ci->load->library('notification_lib');
		$this->user_id = $this->ci->ion_auth->get_user_id();
		$this->profile_id = $this->ci->ion_auth_model->get_profile_id($this->user_id);
	}
	public function can_judge($j_id,$paper_id)
	{
		return $this->ci->judgment_model->is_active_assignment($j_id,$paper_id);
	}
	public function judge_paper($id='')
	{
		$this->ci->form_validation->set_rules('send','','required');
		$this->ci->form_validation->set_rules('final_result_percent','','required|numeric|');
		if($this->ci->form_validation->run() === TRUE){
			$result = $_POST;
			$paper_owner = $this->ci->paper_model->get_paper_owner($id);
			$judge_rank  = $this->ci->judgment_model->get_judge_paper_rank($id,$this->profile_id);
			$this->ci->judgment_model->fill_judgment_result($result,$id,$this->profile_id,$judge_rank);
			// if the judge accept the paper without editing..
			if($result['final_result'] == 3||$result['final_result'] == 1)
			{
				//remove it's assignment only
				$this->ci->judgment_model->remove_assignment($id,$this->profile_id);
			}
			else {
				$this->ci->judgment_model->deactivate_assignment($id,$this->profile_id);
				$this->ci->notification_lib->add_notification($paper_owner,$id,"Some Judges need a few editings in your paper.. Do it");
				
			}
			if($this->judgement_process_finished($id)){
				$final_res_id = $this->caclulate_final_result($id);
				$status_id=0;
				if($final_res_id == 3){
					$this->ci->paper_model->publish_paper($id);
					$this->ci->notification_lib->add_notification($paper_owner,$id,"Congratulation!.. Your paper is published ");
					// release judges and remove assignment
					$this->ci->judgment_model->remove_assignment($id);
					$status_id = $this->ci->paper_model->get_status_id("ready_to_publish");
				}
				else if($final_res_id == 2){
					$status_id = $this->ci->paper_model->get_status_id("need_editing");
					$this->ci->judgment_model->remove_unnecessary_assignment($id);
					$this->ci->notification_lib->add_notification($paper_owner,$id,"Your paper is accepted but need a few editings.. Do it");
				}
				else {
					$status_id = $this->ci->paper_model->get_status_id("rejected");
					$this->ci->notification_lib->add_notification($paper_owner,$id,"Sorry!.. Your paper is rejected ");
				}
				
				$this->ci->paper_model->change_paper_status($status_id,$id);
				// process pending papers
				$this->process_pendding_papers();
			}
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function process_pendding_papers()
	{
		$status_id = $this->ci->paper_model->get_status_id("wait_for_assigning");
		$p_id = $this->ci->paper_model->get_first_pending_paper($status_id);
		if($p_id != FALSE){
			$this->ci->load->library('paper_lib');
			$this->ci->paper_lib->routing_paper($p_id);
		}
	}
	public function caclulate_final_result($p_id='',$method=1){
		if ($method = 1){
			$cutoff = $this->ci->config->item('final_result_cutoff');
			$final_results = $this->ci->judgment_model->get_judges_final_results_by_sum_rank($p_id);
			if($final_results[0]['jr_final_result_id'] == 3)
				if ($final_results[0]['jr_final_result_rank'] >= $cutoff )
					return $final_results[0]['jr_final_result_id'];
				else
					$this->caclulate_final_result($p_id,2);
				
			else
				return $final_results[0]['jr_final_result_id'];
		}
		else {
			$final_results = $this->ci->judgment_model->get_judges_final_results($p_id);
			$min_accepted_result_num = $this->ci->config->item('min_num_of_accept_for_publishing');
			$most_frequent_result = $this->count_results($final_results);
			$key = key ($most_frequent_result);
			if($key == 3){
				if($most_frequent_result[$key] >= $min_accepted_result_num){
					return 3;
				}
				// if the judges accepts less than requested return nothing to wait editing
				return 2;
			}
			else {
				return $key;
			}
		}
		
		
	}
	private function count_results($results='')
	{
		$counted = array_count_values($results); 
	  	arsort($counted); 
		$result_array=array();
		$max = max($counted);
		$index = array_keys($counted, $max);
	  	$result_array[$index[0]] =  $max;
		return $result_array;
	}
	public function judgement_process_finished($paper_id='')
	{ 
		$min_result_num = $this->ci->config->item('min_num_of_paper_judgement_result');
		$currnt_judgement_results_count = $this->ci->judgment_model->get_judgement_results_count($paper_id);
		return $currnt_judgement_results_count >= $min_result_num;
	}
	public function get_measures_and_levels()
	{
		$res['measures'] = $this->ci->judgment_model->get_judgment_measures();
		$res['final_res'] = $this->ci->judgment_model->get_final_results();
		return $res;
	}
	public function check_paper_id($p_id='')
	{
		if($p_id != '' && is_numeric($p_id)){
			$status_id = $this->ci->paper_model->get_status_id('assigned');
			return $this->ci->judgment_model->check_assigning($p_id,$this->profile_id,$status_id);
		}
		else {
			return FALSE;
		}
	}
}
