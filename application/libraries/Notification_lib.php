<?php


/**
 * 
 */
class Notification_lib  {
	private $ci;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->model('notification_model');
		$this->ci->load->library('support_lib');
	}
	public function get_noti_msg($key='')
	{
		if($key == 'assign_paper'){
			return $this->ci->lang->line('assign_paper_not_msg');
		}
	}
	
	public function add_notification($user_id,$paper_id,$not_text)
	{
		$data=array('not_user_id' 	=> $user_id,
					'not_paper_id' 	=> $paper_id,
					'not_text' 		=> $not_text,
					'not_is_read' 	=> 0,
					'not_pushing_date' =>  $this->ci->support_lib->get_current_date()
				);
		$this->ci->notification_model->add($data);	
		
	}
}
