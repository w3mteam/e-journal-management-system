<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Paper management library
 */
class Paper_lib {
	private $ci;
	private $user_id;
	private $profile_id;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->library('ion_auth');
		$this->ci->load->library('support_lib');
		$this->ci->load->model('paper_model');
		$this->ci->load->model('judgment_model');
		$this->ci->load->model('ion_auth_model');
		$this->ci->config->load('journal_general_config');
		$this->user_id = $this->ci->ion_auth->get_user_id();
		$this->profile_id = $this->ci->ion_auth_model->get_profile_id($this->user_id);
	}
	
	public function get_assigned_judges($p_id='')
	{
		return $this->ci->paper_model->get_paper_assigned_judges($p_id);
	}
	public function get_paper_judgment_result($id)
	{
		return $this->ci->judgment_model->get_paper_judgment_result($id);
	}
	/* this function For Adding paper process */
	public function add_paper()
	{
		$info = $this->check_paper_validations();
		if(!$info){
			return FALSE;
		}
		else {
			$info['paper_creation_date'] = $this->ci->support_lib->get_current_date();
			$info['paper_added_by'] = $this->user_id;
			$keywords = json_decode($info['keywords']);
			unset($info['keywords']);
			$id = $this->ci->paper_model->insert_paper($info);
			if($id){
				$key_wieghts = array();
				$i = 1;
				if($keywords != null){
					foreach ($keywords as $key => $keyword) {
						$key_wieghts[$i-1]['pc_paper_id'] = $id;
						$key_wieghts[$i-1]['pc_concept_id'] = $keyword;
						$key_wieghts[$i-1]['pc_weight'] = 1/$i;
						$i++;
					}
				}
				if (empty($key_wieghts)){
					return FALSE;
				}
				$this->ci->paper_model->insert_paper_concepts($key_wieghts);
				$this->ci->session->set_userdata('paper_id',$id);
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}
	/* this function For Adding paper file process */
	public function add_paper_file($id)
	{
		$config['upload_path'] = './files/private/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '5000';
		
		$this->ci->load->library('upload', $config);

		if (!$this->ci->upload->do_upload("file"))
		{
			return $this->ci->upload->display_errors();
		}
		else{
			$file_data = $this->ci->upload->data();
			
			$paper_data = $this->ci->paper_model->get_paper($id);
			$file_name = md5($file_data['raw_name'].strtotime("now")).$file_data['file_ext'];
			copy($file_data['full_path'],'./files/private/'.$paper_data->paper_type.'/'.$file_name);
			
			$info['paper_file']=$file_name;
			$this->ci->paper_model->edit_paper_file($info,$id);
			
			//routing just in add paper case
			if($paper_data->paper_file == ''){
				$this->routing_paper_process($id);
			}
			
			//  in case update paper file mode
			if($paper_data->paper_file != ''){
				$status_id = $this->ci->paper_model->get_status_id("assigned");
				$this->ci->paper_model->change_paper_status($status_id,$id);
				$judges = $this->ci->judgment_model->activate_assignment($id);
				$this->ci->judgment_model->deactivate_judgment_results($id);
				foreach ($judges as $key => $judge) {
					$this->ci->notification_lib->add_notification($this->ci->ion_auth_model->get_user_id($judge->ap_judge_id),$id,"Paper edited.. you can re judge it again");
				}
				
				unlink('files/private/'.$paper_data->paper_type.'/'.$paper_data->paper_file);
			}
            unlink($file_data['full_path']);
			return false;
		}
		
		
	}

	public function routing_paper_process($id)
	{
		// start routing process ..
		$judges = $this->routing_paper_with_exponential_complexity($id);
	
		
		if($judges){
			$this->assign_paper($id, $judges);
			$paper_owner = $this->ci->paper_model->get_paper_owner($id);
			$this->ci->notification_lib->add_notification($paper_owner,$id,"Your paper is assigned to suitable judges..get back later");
		}
		else {
			$status_id = $this->ci->paper_model->get_status_id("wait_for_assigning");
			$this->ci->paper_model->change_paper_status($status_id,$id);
			
		}
	}
	public function routing_paper($paper_id,$keywords='',$n=0)
	{
		$min_num_of_paper_judges = $this->ci->config->item('min_num_of_paper_judges');
		if($keywords == '')
			$keywords = $this->ci->paper_model->get_paper_keywords($paper_id);
		$best_judges = $this->load_balancing($this->get_judges_for_original_keywords($keywords));
		
		
		if(!empty($best_judges)){
			if(sizeof($best_judges) > $min_num_of_paper_judges)
				return array_slice($best_judges, 0, $min_num_of_paper_judges, true);
			else if(sizeof($best_judges) == $min_num_of_paper_judges) {
				return $best_judges;
			}
			else {
				if($n >= sizeof($keywords)){
					return $best_judges;
				}
				return $this->routing_paper($paper_id,$this->replace_with_parent($keywords,$n+1),$n+1);

			}
		}
		else {
			if($n >= sizeof($keywords)){
				return FALSE;
			}
			return $this->routing_paper($paper_id,$this->replace_with_parent($keywords,$n+1),$n+1);
		}
	}
	public function routing_paper_with_exponential_complexity($paper_id)
	{
		$min_num_of_paper_judges = $this->ci->config->item('min_num_of_paper_judges');
		
		$keywords = $this->ci->paper_model->get_paper_keywords($paper_id);
		$best_judges = $this->load_balancing($this->get_judges_for_original_keywords($keywords));
		
		if(empty($best_judges)|| sizeof($best_judges) < $min_num_of_paper_judges){
			$best_judges = $this->try_single_replacement($keywords,$min_num_of_paper_judges);
			if(sizeof($best_judges) < $min_num_of_paper_judges){
				$best_judges = $this->routing_paper($paper_id,$keywords);
			}
			if(sizeof($best_judges) < $min_num_of_paper_judges){
				return FALSE;
			}
			return array_slice($best_judges, 0, $min_num_of_paper_judges, true);			
		}
		else {
			return array_slice($best_judges, 0, $min_num_of_paper_judges, true);
		}	
				
	}
	private function try_single_replacement($keywords='',$min_num_of_paper_judges)
	{
		$best_judges = array();
		for ($i=1; $i <= sizeof($keywords); $i++) { 
			$replaced_keywords = $this->replace_with_parent($keywords, $i);
			$best_judges = $this->load_balancing($this->get_judges_for_original_keywords($replaced_keywords));
			if(sizeof($best_judges) >= $min_num_of_paper_judges){
				return $best_judges;
			}
		}
		return $best_judges;
	}
	public function no_overloaded_judges_C1($judges='')
	{
		$this->ci->load->model('judgment_model');
		$result = array();
		$max_judgment_paper_num = $this->ci->config->item('max_judgment_paper_num');
		
		foreach ($judges as $key => $judge) {
			$balance = $this->ci->judgment_model->get_judge_balance($judge['id']);
			if($balance <  $max_judgment_paper_num){
				$result[] = $judge;
			}
		}
		return $result;
	}
	private function load_balancing($judges='')
	{
		if(!empty($judges)){
			$filter1 = $this->no_overloaded_judges_C1($judges);
			return $filter1;
		}
		else {
			return $judges;
		}
		
	}
	private function replace_with_parent($keywords_array,$n)
	{
		
		$parent = $this->ci->profile_model->get_concept_parent($keywords_array[sizeof($keywords_array) -$n]);
		if(!$parent){
			return $keywords_array;
		}
		else {
			$keywords_array[sizeof($keywords_array) -$n]->pc_concept_id = $parent->concept_id;
			return $keywords_array;
		}
		
	}
	private function get_judges_for_original_keywords($keywords)
	{
		$judges = array();
		$this->ci->load->model('judgment_model');
		$first = true;
		if(!empty($keywords)){
			foreach ($keywords as $key => $keyword) {
				if($first){
					$judges = $this->ci->judgment_model->get_top_judges_in_field($keyword->pc_concept_id);
					
					if($judges != FALSE){
						$first = false;
					}
				}
				else {
					
					$tmp = $this->ci->judgment_model->get_top_judges_in_field($keyword->pc_concept_id);
					if($tmp != FALSE)
						$judges = $this->custom_intersect($judges,$tmp);
				}
			}
		}
		
		return $judges;
	}
	private function custom_intersect($array1,$array2)
	{
		$combined = array();
		foreach ($array1 as $a) {
		    foreach ($array2 as $b) {
		        if ($a['id'] == $b['id']) {
		            $combined[] = array('id'=>$a['id'],'rank'=>max($a['rank'],$b['rank']));
		        }
		    }
		}
		return $combined;
	}
	/* this function For Editing paper process */
	public function edit_paper($p_id)
	{
		$info = $this->check_paper_validations(true);
		if(!$info){
			return FALSE;
		}
		else {
			
			// if paper type changed move the paper file to corresponding folder
			$old_info = $this->ci->paper_model->changed_paper_type($info['paper_type'],$p_id);
			if($old_info){
				copy("files/private/".$old_info->paper_type.'/'.$old_info->paper_file, 
				"files/private/".$info['paper_type'].'/'.$old_info->paper_file);
				unlink("files/private/".$old_info->paper_type.'/'.$old_info->paper_file);
			}
			// update paper info
			$id = $this->edit_paper_info($info,$p_id);
			if($id){
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}
	/* this function For Check paper id validity */
	public function check_paper_id($p_id='')
	{
		if($p_id != '' && is_numeric($p_id)){
			return $this->ci->paper_model->check_ownership($p_id,$this->user_id)||$this->ci->ion_auth->is_admin();
		}
		else {
			return FALSE;
		}
	}
	public function remove_paper($p_id)
	{
		if($this->check_paper_id($p_id)){
			$paper = $this->ci->paper_model->delete_paper($p_id);
			// if remove failed
			if(!$paper){
				return FALSE;
			}
			unlink("files/private/".$paper->paper_type."/".$paper->paper_file);
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	// helper function to validate entered data in add or edit paper
	private function check_paper_validations($edit=false)
	{
		$this->ci->form_validation->set_rules('title','Title','required|xss_clean|max_length[255]');
		$this->ci->form_validation->set_rules('type','Type','required|xss_clean|callback_checkTypeValue');
		$this->ci->form_validation->set_rules('description','Description','required|xss_clean|callback_checkTypeValue');
		if($this->ci->form_validation->run() === TRUE){
			$data['paper_title'] = $this->ci->input->post('title');
			$paper_type = $this->ci->input->post('type');
			if($paper_type == 'PAPER' || $paper_type == 'paper'){
				$data['paper_type'] = "PAPER";
			}
			else {
				$data['paper_type'] = "JOURNAL";
			}
			if(!$edit)
				$data['keywords'] = $this->ci->input->post('keywords');
			$data['paper_description'] = $this->ci->input->post('description');
			$data['paper_status'] = $this->ci->paper_model->get_status_id("pending");
			return $data;
		}
		else {
			return FALSE;
		}
	}
	private function backup_paper_data($id='')
	{
		$paper = $this->ci->paper_model->get_paper($id);
		$info['ph_paper_id'] = $paper->paper_id;
		$info['ph_paper_type'] = $paper->paper_type;
		$info['ph_paper_title'] = $paper->paper_title;
		$info['ph_paper_description'] = $paper->paper_description;
		$info['ph_paper_file'] = $paper->paper_file;
		$info['ph_editing_date'] = $this->ci->support_lib->get_current_date();
		$info['ph_paper_file'] = $paper->paper_file;
		$info['ph_paper_edited_by'] = $this->user_id;
		$info['ph_paper_status'] = $paper->paper_status;
		return $info;
		
	}
	// helper function to check paper type value
	private function checkTypeValue($value)
	{
		if($value == 'PAPER' || $value == 'JOURNAL'){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	private function assign_paper($paper_id,$judges_ids)
	{
		
			$max_judgment_period = $this->ci->config->item('max_judgment_period');
			foreach ($judges_ids as $key => $jud) {
				$info['ap_paper_id'] = $paper_id;
				$info['ap_judge_id'] = $jud['id'];
				$info['ap_linkmatrix_rank'] = $jud['rank'];
				$info['ap_assign_start_date	'] = $this->ci->support_lib->get_current_date();
				$info['ap_assign_end_date'] = Date('y:m:d', strtotime("+$max_judgment_period days"));
				
				$this->ci->paper_model->assing_paper($info);
				$this->ci->paper_model->increase_judge_tasks($jud['id']);
				$this->ci->notification_lib->add_notification($this->ci->ion_auth_model->get_user_id($jud['id']),$paper_id,"There is a new paper assigned to judge");
			}
			$status_id = $this->ci->paper_model->get_status_id("assigned");
			$this->ci->paper_model->change_paper_status($status_id,$paper_id);
			
	}
	private function edit_paper_info($info,$p_id)
	{
		$backed_info = $this->backup_paper_data($p_id);
		// update paper info
		return $this->ci->paper_model->update_paper($info,$p_id,$backed_info);
	}
	// unused func
	private function drop_down_courtesy($info){
		$result=array();
		if(!empty($info)){
			foreach ($info as $key => $item) {
				$result[$item->paper_id] = $item->paper_title;	
			}
		}
		return $result;
	}
	public function get_assigned_papers()
	{
		$status_id = $this->ci->paper_model->get_status_id('assigned');
		
		return $this->ci->paper_model->get_assigned_papers($this->profile_id,$status_id);
	}
	public function get_pending_edit_papers()
	{
		return $this->ci->paper_model->get_pending_edit_papers($this->profile_id);	
	}
	public function get_all_related_papers()
	{
		return $this->ci->paper_model->get_related_papers($this->profile_id,$status);
	}
}
