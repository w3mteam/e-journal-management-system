<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  Paper management library
 */
class Profile_lib {
	private $ci;
	private $user_id;
	private $profile_id;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->load->model('profile_model');
		$this->ci->load->library('support_lib');
		$this->ci->load->library('ion_auth');
		$this->user_id = $this->ci->ion_auth->get_user_id();
		$this->profile_id = $this->ci->ion_auth_model->get_profile_id($this->user_id);
	}
	public function has_profile()
	{
		return $this->ci->profile_model->has_profile($this->user_id);
	}
	public function get_profile_info($prof_id)
	{
		$result = array();
		$result['info'] = $this->ci->profile_model->get_profile_basic_info($prof_id);
		$result['specializations'] = $this->ci->profile_model->get_profile_spec($prof_id);
		$result['projects'] = $this->ci->profile_model->get_profile_projects($prof_id);
		$result['publications'] = $this->ci->profile_model->get_profile_publications($prof_id);
		$result['courses'] = $this->ci->profile_model->get_profile_courses($prof_id);
		return $result;
	}
	public function check_profile_id($id='')
	{
		if($id != '' && is_numeric($id)){
			if($this->ci->profile_model->profile_exist($id)){
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}
	public function add_information()
	{
		$info = $this->check_profile_info_validations();
		if(!$info){
			return FALSE;
		}
		else {
			$info['date_time'] = $this->ci->support_lib->get_current_date();
			$info['userid'] = $this->user_id;
			$id = $this->ci->profile_model->insert_profile_info($info);
			if($id){
				$this->ci->ion_auth_model->set_profile_to_user($id,$this->user_id);
				$this->ci->session->set_userdata('profile_id',$id);
			}
			else {
				return FALSE;
			}
			if($this->add_specializtion($id)){
				return TRUE;
			}else{
				return FALSE;
			}
			
		}
	}
	private function add_specializtion($id){
		foreach ($_POST as $key => $value) {
			if(is_numeric($key)){
				$data[] = array('profile_id'=>$id,'specializaty_id'=>$key);
			}
		}
		if($this->ci->profile_model->insert_specializtion($data)){
			return TRUE;
		}else{
			return false;
		}
	}
	private function check_profile_info_validations()
	{
		$this->ci->form_validation->set_rules('first_name','First Name','required|xss_clean|max_length[255]');
		$this->ci->form_validation->set_rules('last_name','Last Name','required|xss_clean|max_length[255]');
		if($this->ci->form_validation->run() === TRUE){
			$data['first_name'] = $this->ci->input->post('first_name');
			$data['last_name'] = $this->ci->input->post('last_name');
			return $data;
		}
		else {
			return FALSE;
		}
	}
}