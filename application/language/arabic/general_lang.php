<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 
/* Paper Section */
$lang['description'] = 'الوصف';
$lang['title'] = 'العنوان';
$lang['type'] = 'النمط';
$lang['success_add'] = 'تم إضافة المعلومات بنجاح';
$lang['success_edit'] = 'تم تعديل المعلومات بنجاح';
$lang['fail_edit'] = 'فشل في تعديل البيانات';
$lang['faild_delete'] = 'فشل في حذف البيانات';
$lang['fail_add'] = 'فشل في إضافة المعلومات';
$lang['no_privilages'] = "403 لا تملك الصلاحيات اللازمة لعمل هذه العملية";
$lang['invalid_id'] = "الرقم المدخل غير صحيح";
$lang['assign_paper_not_msg'] = 'لديك ورقة بحثية جديدة بحاجة للمراجعة';

$lang['failed_assign'] = 'فشل في إسناد الورقة البحثية';
$lang['success_assign'] = 'تم إسناد الورقة البحثية بنجاح';
$lang['cant_judge'] = 'لا يمكنك تحكيم هذه الورقة البحثية';