<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$config['max_judgment_period'] = 15; // days
$config['min_num_of_paper_judges'] = 5;
$config['max_judgment_paper_num'] = 5; // maximum number of paper that judge can take
$config['min_num_of_paper_judgement_result'] =4;
$config['min_num_of_accept_for_publishing'] = 3;
$config['final_result_cutoff'] = 1.6;
