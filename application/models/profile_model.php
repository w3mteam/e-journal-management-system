<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Profile_model extends CI_Model {
	
	// private $profile_id;
	public function __construct()
	{
		// $this->load->model('ion_auth_model');
		// $this->profile_id =$this->ion_auth_model->get_profile_id($this->user_id);
	}
	public function profile_exist($id='')
	{
		$q = $this->db->get_where('profile',array('profile_id'=>$id));
		if($q->num_rows() > 0)
			return TRUE;
		
		return FALSE;
	}
	public function get_profile_proj($prof_id='')
	{
		
		$q = $this->db->get_where('project',array('project.profile_id'=>$prof_id));
		if($q->num_rows() > 0){
			return $q->result() ;
		}else
			return FALSE;
	}
	public function get_profile_projects($prof_id='')
	{
		$pro = $this->get_profile_proj($prof_id);
		if($pro != null){
			foreach ($pro as $key => $value) {
				$this->db->join('concept','concept.concept_id = project_concepts.concept_id');
				$q = $this->db->get_where('project_concepts',array('project_concepts.project_id'=>$value->project_id));
				if($q->num_rows() > 0){
					$project_list[] = array('project'=>$value,'concept'=>$q->result());
				}else
					return FALSE;
			}
			return $project_list;
		}
		
	}
	public function get_profile_pub($prof_id='')
	{
		$q = $this->db->get_where('publication',array('publication.profile_id'=>$prof_id));
		if($q->num_rows() > 0){
			return $q->result() ;
		}else
			return FALSE;
	}
	public function get_profile_publications($prof_id=''){
		$pub = $this->get_profile_pub($prof_id);
		if($pub != null){
			foreach ($pub as $key => $value) {
				$this->db->join('concept','concept.concept_id = publication_concepts.concept_id');
				$q = $this->db->get_where('publication_concepts',array('publication_concepts.publication_id'=>$value->pub_id));
				
				if($q->num_rows() > 0){
					$publication_list[] = array('publication'=>$value,'concept'=>$q->result());
				}else
					return FALSE;
			}
			return $publication_list;
		}
		
		
	}
	public function get_profile_cour($prof_id='')
	{
		$q = $this->db->get_where('course',array('course.profile_id'=>$prof_id));
		if($q->num_rows() > 0){
			return $q->result() ;
		}else
			return FALSE;
	}
	public function get_profile_courses($prof_id='')
	{
		$cour = $this->get_profile_cour($prof_id);
		if($cour != null){
			foreach ($cour as $key => $value) {
				$this->db->join('concept','concept.concept_id = course_concepts.concept_id');
				$q = $this->db->get_where('course_concepts',array('course_concepts.course_id'=>$value->course_id));
				if($q->num_rows() > 0){
					$course_list[] = array('course'=>$value,'concept'=>$q->result());
				}else
					return FALSE;
			}
			return $course_list;
		}
		
	}
	public function get_profile_basic_info($prof_id='')
	{
		$q = $this->db->get_where('profile',array('profile_id'=>$prof_id));
		if($q->num_rows() > 0){
			return $q->row() ;
		}else
			return FALSE;
	}
	public function get_profile_info($u_id='')
	{
		$q = $this->db->get_where('profile',array('userid'=>$u_id));
		if($q->num_rows() > 0){
			return $q->row() ;
		}else
			return FALSE;
	}
	
	public function get_profile_spec($profile_id='')
	{
		$this->db->join('concept','concept.concept_id=specialization.specializaty_id');	
		$q = $this->db->get_where('specialization',array('profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return $q->result() ;
		}else
			return FALSE;
	}
	public function has_profile($u_id='')
	{
		$q = $this->db->get_where('users',array('id'=>$u_id));
		if($q->num_rows() > 0){
			if($q->row()->profile_id == "NULL"){
				return FALSE;
			}
			return $q->row()->profile_id ;
		}
	}
	
	public function get_concept_parent($paper_concept='')
	{
		$q = $this->db->get_where('concept',array('concept_id'=>$paper_concept->pc_concept_id));
		if($q->result() > 0){
			$parent_id = $q->row()->parent_id;
			if($parent_id != "NULL"){
				$q = $this->db->get_where('concept',array('concept_id'=>$parent_id));
				return $q->row();
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
	public function get_concept()
	{
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function insert_profile_info($data='')
	{
		$this->db->insert('profile',$data);
		if ($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else {
			return FALSE;
		}
	}
	public function insert_specializtion($data='')
	{
		
		foreach ($data as $key => $value) {
			$info = array('profile_id'=>$value['profile_id'],'specializaty_id'=>$value['specializaty_id']);
			$this->db->insert('specialization',$info);
		}
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
		
	}
	 public function insert_publication($data)
	 {
		$this->db->insert('publication',$data);
		if ($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else {
			return FALSE;
		}
	}
 	public function insert_project($data)
	{
		$this->db->insert('project',$data);
		if ($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else {
			return FALSE;
		}
	}
	public function insert_course($data)
	{
		$this->db->insert('course',$data);
		if ($this->db->affected_rows() > 0){
			return $this->db->insert_id();
		}
		else {
			return FALSE;
		}
	}
    public function insert_publication_concepts($data)
	{
	 	//echo "yessssssssssssssssssssssssssssssssssssssssssssssssssss";
		$this->db->insert('publication_concepts',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function insert_project_concepts($data)
	{
	 	//echo "yessssssssssssssssssssssssssssssssssssssssssssssssssss";
		$this->db->insert('project_concepts',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function insert_course_concepts($data)
	{
	 	//echo "yessssssssssssssssssssssssssssssssssssssssssssssssssss";
		$this->db->insert('course_concepts',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function update_concept_valid($id)
	{
		$this->db->where('concept_id',$id);
        $this->db->update('concept', array('valid'=> '1'));
	}
	public function get_profile($value='')
	{
		$q = $this->db->get('profile');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_first_profile_id($value='')
	{
		$q = $this->db->get('profile');
		if ($q->num_rows()>0){
			return $q->row();
		}
		else {
			return 0;
		}
	}
}
	