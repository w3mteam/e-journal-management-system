<?php
/**
 * 
 */
class Notification_model extends CI_Model {
	
	function __construct() {
		parent::__construct();
		
	}
	public function add($data)
	{
		$this->db->insert('notification',$data);
		if($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else
			return  FALSE;
	}
	
	public function seen($user_id)
	{
		$this->db->where('not_user_id',$user_id);
		$this->db->update('notification',array('not_is_read' => 1));
		if($this->db->affected_rows() > 0)
			return TRUE;
		else
			return FALSE;
	}
	
	public function get_number_not_seen($user_id)
	{
		$q=$this->db->get_where('notification',array('not_user_id' => $user_id , 'not_is_read' => 0));
		if($q->num_rows() >0)
			return $q->num_rows();
		else
			return 0;
	}
	
	public function get_notifications($user_id)
	{
		$this->db->join('paper','paper.paper_id = notification.not_paper_id ');	
		$this->db->order_by('not_pushing_date','desc');
		$q=$this->db->get_where('notification',array('not_user_id' => $user_id ));
		if($q->num_rows() > 0 )
			return $q->result();
		else
			return FALSE;
	}
	
	public function get_unseen_notifications($user_id)
	{
		$this->db->join('paper','paper.paper_id = notification.not_paper_id ');	
		$this->db->order_by('not_pushing_date','desc');
		$this->db->limit(5,0);
		$q=$this->db->get_where('notification',array('not_user_id' => $user_id ,'not_is_read' => 0));
		if($q->num_rows() > 0 )
			return $q->result();
		else
			return FALSE;
	}
	
}
