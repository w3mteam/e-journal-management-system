	<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Paper_model extends CI_Model {
	
	public function get_paper_assigned_judges($p_id='')
	{
		$this->db->select('paper_id,paper_title,paper_type,paper_file,task_num,profile_id,first_name,last_name,ap_assign_start_date,ap_assign_end_date');
		$this->db->join('paper','paper.paper_id = assigned_paper.ap_paper_id');
		$this->db->join('profile','profile.profile_id = assigned_paper.ap_judge_id');
		$q = $this->db->get_where('assigned_paper',array('ap_paper_id'=>$p_id));
		if ($q->num_rows()>0){
			$final_res = array();
			foreach ($q->result() as $key => $ap_judge) {
				$this->db->join('concept','concept.concept_id = paper_concepts.pc_concept_id');
				$query = $this->db->get_where('paper_concepts',array('pc_paper_id'=>$ap_judge->paper_id));
				$final_res[] = array('judge'=>$ap_judge,'paper_concepts'=>$query->result());
			}
			return $final_res;
		}
		return FALSE;
		
	}
	public function get_all_papers()
	{
		$this->db->join('status','status.st_id=paper.paper_status');
		$q = $this->db->get('paper');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	
	public function get_first_pending_paper($st_id)
	{
		$this->db->select('paper_id');
		$this->db->order_by('paper_creation_date','asc');
		$q = $this->db->get_where('paper',array('paper_status'=>$st_id));
		if ($q->num_rows()>0){
			return $q->row()->paper_id;
		}
		else {
			return FALSE;
		}
	}
	
	public function publish_paper($id='')
	{
		$this->db->where('paper_id',$id);
		$this->db->update('paper',array('paper_published'=>1));
	}
	public function get_concept()
	{
		$this->db->select('concept_id,name');
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_current_paper_concepts($p_id='')
	{
		$this->db->select('concept.concept_id,name');
		$this->db->order_by('pc_weight','desc');
		$this->db->join('concept','concept.concept_id= paper_concepts.pc_concept_id');
		$q = $this->db->get_where('paper_concepts',array('pc_paper_id'=>$p_id));
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_paper_keywords($p_id='')
	{
		$this->db->order_by('pc_weight','desc');
		$q = $this->db->get_where('paper_concepts',array('pc_paper_id'=>$p_id));
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function insert_paper_concepts($key_wieghts='')
	{
		
		$this->db->insert_batch('paper_concepts',$key_wieghts);
	}
	public function get_paper_owner($p_id='')
	{
		$q = $this->db->get_where('paper',array('paper_id'=>$p_id));
		if($q->num_rows()>0){
			return $q->row()->paper_added_by;
		}
	}
	public function check_ownership($p_id,$user_id)
	{
		$q = $this->db->get_where('paper',array('paper_id'=>$p_id,'paper_added_by'=>$user_id));
		if($q->num_rows()>0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function get_paper($id='')
	{
		$this->db->where('paper_id',$id);
		$q = $this->db->get('paper');
		if ($q->num_rows()>0){
			return $q->row();
		}
		else {
			return 0;
		}
	}
	public function insert_paper($data='')
	{
		$this->db->insert('paper',$data);
		$insert_id = $this->db->insert_id();
		if ($this->db->affected_rows() > 0){
			return $insert_id;
		}
		else {
			return FALSE;
		}
	}
	public function edit_paper_file($data,$id)
	{
		$this->db->where('paper_id',$id);
		$this->db->update('paper',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	public function update_paper($data='',$paper_id,$backed_info)
	{
		$this->db->insert('paper_history',$backed_info);
		$this->db->where('paper_id',$paper_id);
		$this->db->update('paper',$data);	
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}	
	}
	public function delete_paper($paper_id='')
	{
		$this->db->where('paper_id',$paper_id);
		$q = $this->db->get('paper');
		$this->db->where('paper_id',$paper_id);
		$this->db->delete('paper');	
		if ($this->db->affected_rows() > 0){
			return $q->first_row();
		}
		else {
			return FALSE;
		}		
	}
	public function change_paper_status($st_id='',$paper_id='')
	{
		if($paper_id != ''){
			$this->db->where('paper_id',$paper_id);
		}
		$q = $this->db->update('paper',array('paper_status'=>$st_id));
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function get_status_id($name='')
	{
		$q = $this->db->get_where('status',array('st_name'=>$name));
		if ($q->num_rows()>0){
			return $q->row()->st_id;
		}
		else {
			return 0;
		}
	}
	public function get_my_papers($id='')
	{
		$this->db->join('status','paper.paper_status=status.st_id');
		$q = $this->db->get_where('paper',array('paper_added_by'=>$id));
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	
	public function changed_paper_type($new_type='',$p_id)
	{
		$q = $this->db->get_where('paper',array('paper_id'=>$p_id));
		if($q->row()->paper_type == $new_type){
			return FALSE;
		}
		else {
			return $q->row();
		}
	}
	public function get_papers_by_status($status='')
	{
		$this->db->where('status.st_name',$status);
		$this->db->join('status','paper.paper_status=status.st_id');
		$q = $this->db->get('paper');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function get_pending_and_assigned_papers()
	{
		$this->db->where('status.st_name','pending');
		$this->db->or_where('status.st_name','assigned');
		$this->db->join('status','paper.paper_status=status.st_id');
		$q = $this->db->get('paper');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function assing_paper($info='')
	{
		$this->db->insert('assigned_paper',$info);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}		
	}
	// TOFix
	public function set_notification($info='')
	{
		$this->db->insert('notification',$info);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}

	public function get_assigned_papers($u_id,$status='')
	{
		$this->db->where(array('ap_judge_id'=>$u_id,'paper_status'=>$status,
								'ap_is_active'=>1));
		
		$this->db->join('paper','assigned_paper.ap_paper_id=paper.paper_id');
		$this->db->join('status','paper.paper_status=status.st_id');
		$q = $this->db->get('assigned_paper');
		//var_dump($q->result()); var_dump($status);
		//die();
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function get_pending_edit_papers($u_id)
	{
		$this->db->where('ap_judge_id',$u_id);
		$this->db->where('st_name','need_editing');
		$this->db->join('paper','assigned_paper.ap_paper_id=paper.paper_id');
		$this->db->join('status','paper.paper_status=status.st_id');
		$q = $this->db->get('assigned_paper');
		//die();
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function get_related_papers($u_id)
	{
		$this->db->where('ap_judge_id',$u_id);
		$this->db->join('assigned_paper','assigned_paper.ap_paper_id=paper.paper_id');
		$this->db->join('status','paper.paper_status = status.st_id');
		$this->db->group_by('paper_status');
		// need review
		$this->db->having('paper_status',array('assigned','need_editing','ready_to_publish','rejected'));
		$q = $this->db->get('paper');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function increase_judge_tasks($prof_id='')
	{
		$this->db->where('profile_id',$prof_id);
		$q = $this->db->get('profile');
		if ($q->num_rows()>0){
			$t_num = $q->row()->task_num;
			$t_num++;
			$this->db->where('profile_id',$prof_id);
			$this->db->update('profile',array('task_num'=>$t_num));
			if ($this->db->affected_rows() > 0){
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
		
	}
}


