<?php
/**
 * 
 */
class Judgment_model extends CI_Model {
	
	public function is_active_assignment($j_id='',$p_id)
	{
		$this->db->where(array('ap_paper_id'=>$p_id,'ap_judge_id'=>$j_id));
		$q = $this->db->get('assigned_paper');
		if($q->num_rows > 0){
			return $q->row()->ap_is_active;
		}
		return FALSE;
	}
	public function deactivate_assignment($p_id,$j_id='')
	{
		if($j_id =='')
			$this->db->where(array('ap_paper_id'=>$p_id));
		else
			$this->db->where(array('ap_paper_id'=>$p_id,'ap_judge_id'=>$j_id));
		$this->db->update('assigned_paper',array('ap_is_active'=>0));
	}
	public function activate_assignment($p_id='')
	{
		$this->db->where(array('ap_paper_id'=>$p_id));
		$this->db->update('assigned_paper',array('ap_is_active'=>1));
		$this->db->select('ap_judge_id');
		$this->db->where(array('ap_paper_id'=>$p_id));
		$q = $this->db->get('assigned_paper');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	public function deactivate_judgment_results($p_id='')
	{
		$this->db->where(array('jr_paper_id'=>$p_id));
		$this->db->update('judgement_result',array('jr_is_active'=>0));
	}
	public function get_judge_balance($j_id='')
	{
		$q = $this->db->get_where('profile',array('profile_id'=>$j_id));
		if($q->num_rows > 0){
			return $q->row()->task_num;
		}
		return FALSE;
	}
	public function get_top_judges_in_field($concept_id='')
	{
		$this->db->select('profile_id,result');
		$this->db->where('concept_id',$concept_id);
		$this->db->order_by('result','desc');
		$q = $this->db->get('profile_final_result');
		if($q->num_rows > 0){
			$result = array();
			foreach ($q->result() as $key => $value) {
				$result[] = array('id'=>(int)$value->profile_id,'rank'=>$value->result);
			}
			return $result;
		}
		return FALSE;
	}
	public function change_assignment_status($p_id,$j_id,$status_id)
	{
		$this->db->where(array('ap_paper_id'=>$p_id,'ap_judge_id'=>$j_id));
		$this->db->update('assigned_paper',array('ap_status'=>$status_id));
	}
	public function remove_unnecessary_assignment($id='')
	{
		$where =array('ap_paper_id'=>$p_id,'ap_is_active'=>1);
		
		$this->db->where($where);
		$this->db->delete('assigned_paper');
	}
	public function remove_assignment($p_id,$j_id='')
	{
		$where =array('ap_paper_id'=>$p_id);
		if($j_id != ''){
			$where['ap_judge_id'] = $j_id;
			$q = $this->db->get_where('profile',array('profile_id'=>$j_id));
			$task_num = $q->row()->task_num;
			$task_num--;
			$this->db->where('profile_id',$j_id);
			$this->db->update('profile',array('task_num'=>$task_num));
		}
		else {
			$query = $this->db->get_where('assigned_paper',$where);
			if($query->num_rows()>0){
				foreach ($query->result() as $key => $judge) {
					$q = $this->db->get_where('profile',array('profile_id'=>$judge->ap_judge_id));
					$task_num = $q->row()->task_num;
					$task_num--;
					$this->db->where('profile_id',$judge->ap_judge_id);
					$this->db->update('profile',array('task_num'=>$task_num));
				}
			}
		}
		$this->db->where($where);
		$this->db->delete('assigned_paper');
		
		
	}
	public function get_judge_paper_rank($paper_id,$judge_id)
	{
		$this->db->select('ap_linkmatrix_rank');
		$q = $this->db->get_where('assigned_paper',array('ap_paper_id'=>$paper_id,'ap_judge_id'=>$judge_id));
		if($q->num_rows > 0){
			return $q->row()->ap_linkmatrix_rank;
		}
		return FALSE;
	}
	public function get_paper_judgment_result($p_id)
	{
		$this->db->join('final_result','final_result.fr_id = judgement_result.jr_final_result_id');
		$q = $this->db->get_where('judgement_result',array('jr_paper_id'=>$p_id,'jr_is_active'=>1));
		$j_results = $q->result();
		$final_results = array();
		foreach ($j_results as $key => $res) {
			$this->db->join('judgement_measure','judgement_measure.jm_id = measure_result.mr_measure_id');
			$this->db->join('rank_levels','rank_levels.rl_id = measure_result.mr_rank_level_id');
			$measures = $this->db->get_where('measure_result',array('mr_judgement_result_id'=>$res->jr_id));
			$notes = $this->db->get_where('paper_review',array('pr_paper_id'=>$p_id,'pr_reviewer_id'=>$res->jr_judge_id));
			$final_results[] = array('final_result'=>$res,'measures'=>$measures->result(),'notes'=>$notes->result());
		}
		return $final_results;
	}
	public function get_judgement_results_count($p_id)
	{
		$q = $this->db->get_where('judgement_result',array('jr_paper_id'=>$p_id,'jr_is_active'=>1));
		return $q->num_rows ;
	}
	public function fill_judgment_result($res='',$p_id,$j_id,$judge_rank)
	{
		$judgment_result['jr_paper_id'] = $p_id;
		$judgment_result['jr_judge_id'] = $j_id;
		$judgment_result['jr_final_result_id'] = $res['final_result'];
		$judgment_result['jr_final_result_rank'] = $res['final_result_percent']/100*$judge_rank*$res['final_result'];
		$this->db->insert('judgement_result',$judgment_result);
		$inserted_judgement_result_id = $this->db->insert_id();
		foreach ($res as $key => $value) {
			if(is_numeric($key)){
				$measure_result['mr_measure_id'] = $key;
				$measure_result['mr_rank_level_id'] = $value;
				$measure_result['mr_judgement_result_id'] = $inserted_judgement_result_id;
				$this->db->insert('measure_result',$measure_result);
			}
		}
		if($res['notes']!=''){
			$paper_review['pr_paper_id'] = $p_id;
			$paper_review['pr_reviewer_id'] = $j_id;
			$paper_review['pr_review_text'] = $res['notes'];
			$paper_review['pr_review_date'] = date('Y-m-d H:i:s');
			$this->db->insert('paper_review',$paper_review);
		}
	}
	public function get_judges_final_results($p_id='')
	{
		$this->db->select('jr_final_result_id ');
		
		if($p_id != ''){
			$this->db->where('jr_paper_id',$p_id);
		}
		$q = $this->db->get('judgement_result');
		if($q->num_rows > 0){
			$q_res = $q->result_array();
			
			$result=array();
			foreach ($q_res as $key => $value) {
				$result[] = $value['jr_final_result_id'];
			}
			return $result;
		}
		return FALSE;
	}
	public function get_judges_final_results_by_sum_rank($p_id='')
	{
		$this->db->select('jr_final_result_id ');
		$this->db->select_sum('jr_final_result_rank');
		$this->db->group_by('jr_final_result_id');
		$this->db->order_by('jr_final_result_rank','desc');
		if($p_id != ''){
			$this->db->where('jr_paper_id',$p_id);
		}
		$q = $this->db->get('judgement_result');
		if($q->num_rows > 0){
			$q_res = $q->result_array();
			return $q_res;
		}
		return FALSE;
	}
	public function get_final_results()
	{
		$q = $this->db->get('final_result');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	
	public function get_judgment_measures()
	{
		$q = $this->db->get('judgement_measure');
		$res = array();
		if($q->num_rows > 0){
			$measures = $q->result();
			foreach ($measures as $key => $measure) {
				$m = $this->db->get_where('rank_levels',array('rl_judgment_measure_id'=>$measure->jm_id));
				$res[] = array('measures'=>$measure,'rank_levels'=>$m->result());
			}
			return $res;
		}
		return FALSE;
	}
	
	public function get_rank_levels()
	{
		$q = $this->db->get('rank_levels');
		if($q->num_rows > 0){
			return $q->result();
		}
		return FALSE;
	}
	
	public function check_assigning($p_id='',$u_id,$status)
	{
		$this->db->join('paper','assigned_paper.ap_paper_id=paper.paper_id');
		$q = $this->db->get_where('assigned_paper',
				array('ap_paper_id'=>$p_id,'ap_judge_id'=>$u_id,'paper_status'=>$status));
		if($q->num_rows()>0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}
