<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Main_model extends CI_Model {
	public function get_all_concept($value='')
	{
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_valid_concept()
	{
		
		$this->db->where(array('valid'=>'1'));
		$this->db->limit(40);
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_publish_paper($value='')
	{
		$this->db->join('users','paper.paper_added_by = users.id');
		
		$this->db->limit(6);
		$q = $this->db->get_where('paper',array('paper.paper_status'=>'6'));
		if ($q->num_rows() > 0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_paper_concept($paper_id)
	{
		$this->db->join('concept','paper_concepts.pc_concept_id=concept.concept_id');
		$q = $this->db->get_where('paper_concepts',array('paper_concepts.pc_paper_id'=>$paper_id));
		if ($q->num_rows() > 0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_six_valid_concept($value='')
	{
		$this->db->where(array('valid'=>1));
		$this->db->limit(3);
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_paper_according_to_concept_id()
	{
		$data = $this->get_six_valid_concept();
		$info = array();
		foreach ($data as $key => $value) {
			$this->db->limit(4);
			$this->db->join('paper','paper.paper_id=paper_concepts.pc_paper_id');
			$q = $this->db->get_where('paper_concepts',array('paper_concepts.pc_concept_id'=>$value->concept_id));
			if ($q->num_rows()>0){
				$info[] = array('concept_id'=>$value->concept_id,'concept_name'=>$value->name,'prog_name'=>$value->prog_name,'paper'=>$q->result());
			}
		}
		return $info;
	}
	public function paper_added_recently()
	{
		$this->db->order_by('paper_creation_date','desc');
		$q = $this->db->get('paper');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_all_valid_concept()
	{
		
		$this->db->where(array('valid'=>'1'));
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function view_paper_accord_concept($concept_id)
	{
		$this->db->join('paper','paper_concepts.pc_paper_id =paper.paper_id');
		$this->db->join('users','users.id=paper.paper_added_by');
		$q = $this->db->get_where('paper_concepts',array('paper_concepts.pc_concept_id'=>$concept_id));
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	
	public function get_paper_details($paper_id)
	{
		$this->db->join('users','users.id=paper.paper_added_by');
		$q = $this->db->get_where('paper',array('paper.paper_id'=>$paper_id));
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_paper()
	{
		$this->db->join('users','paper.paper_added_by = users.id');
		$data = array('paper.paper_type'=>'PAPER','paper.paper_status'=>'6');
		$q = $this->db->get_where('paper',$data);
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function get_journal()
	{
		$this->db->join('users','paper.paper_added_by = users.id');
		$data = array('paper.paper_type'=>'JOURNAL','paper.paper_status'=>'6');
		$q = $this->db->get_where('paper',$data);
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
    public function four_concepts_for_footer($value='')
	{
		$this->db->where(array('valid'=>'1'));
		$this->db->limit(4);
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function put_concept_false($value='')
	
	{
		$data = $this->get_all_concept();
		foreach ($data as $key => $value) {
			$this->db->where('concept_id',$value->concept_id);
        	$this->db->update('concept', array('valid'=> '0'));
		}
		
	}
	public function put_concept_true($value='')
	
	{
		$data = $this->get_all_concept();
		foreach ($data as $key => $value) {
			$this->db->where('concept_id',$value->concept_id);
        	$this->db->update('concept', array('valid'=> '1'));
		}
		
	}
}
?>