<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Search_engin_model extends CI_Model {
	public function get_ready_to_publish($value='')
	{
		$this->db->select('st_id');
		$this->db->where('st_name','ready_to_publish');
		$q = $this->db->get('status');
		if ($q->num_rows()>0){
			$data =  array_shift($q->result_array());
			return $data['st_id'];
		}else {
			return FALSE;
		}
	}
	public function get_publish_paper($value='')
	{
		$id_publish = $this->get_ready_to_publish();
		$this->db->where('paper_status',$id_publish);
		$q = $this->db->get('paper');
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_num_publish_paper($value='')
	{
		$id_publish = $this->get_ready_to_publish();
		$this->db->where('paper_status',$id_publish);
		$q = $this->db->get('paper');
		if ($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_publish_paper_and_concepts($value='')
	{
		$id_publish = $this->get_ready_to_publish();
		$this->db->select('*');
		$this->db->from('paper_concepts');
		$this->db->join('paper', 'paper_concepts.pc_paper_id = paper.paper_id AND paper.paper_status = '.$id_publish);
		$q = $this->db->get();
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_publish_concepts($value='')
	{
		$id_publish = $this->get_ready_to_publish();
		$this->db->distinct();
		$this->db->select('pc_concept_id');
		$this->db->from('paper_concepts');
		$this->db->join('paper', 'paper_concepts.pc_paper_id = paper.paper_id AND paper.paper_status = '.$id_publish);
		$this->db->join('concept', 'paper_concepts.pc_concept_id = concept.concept_id ');
		$q = $this->db->get();
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_papers_num_according_to_specific_concept($pc_concept_id)
	{
		$this->db->where('pc_concept_id',$pc_concept_id);
		$q = $this->db->get('paper_concepts');
		if ($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function insert_paper_final_result($data)
	{
		$this->db->insert('paper_final_result',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}else {
			return FALSE;
		}		
	}
	public function delete_paper_final_result($id)
	{
		$this->db->where('pfr_id', $id);
		$this->db->delete('paper_final_result');	
	}
	public function get_paper_final_result()
	{
		$q = $this->db->get('paper_final_result');
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_search_by_concept($spilt)
	{
		for ($i=0; $i < sizeof($spilt); $i++) {
			$this->db->distinct();
			$this->db->where("`name` LIKE  '%".$this->db->escape_like_str($spilt[$i])."%'");
			$q = $this->db->get('concept');
			$data[] = $q->result(); 	
		}
		if($data > 0){
			return $data;
		}
		else{
			return FALSE;
		}
			
	}
	public function get_paper_in_field($concept_id='')
	{
		$this->db->select('paper_id');
		$this->db->where('concept_id',$concept_id);
		$this->db->order_by('result','desc');
		$q = $this->db->get('paper_final_result');
		if($q->num_rows > 0){
			$result = array();
			foreach ($q->result() as $key => $value) {
				$result[] = $value->paper_id;
				var_dump($result);
			}
			return $result;
		}
		return FALSE;
	}
	public function get_concept()
	{
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}
		else {
			return 0;
		}
	}
	public function insert_test($value='')
	{
		
		$this->db->insert('test',array('val'=>$value));
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}else {
			return FALSE;
		}	
	}
	public function get_concept_parent($paper_concept='')
	{
		$q = $this->db->get_where('concept',array('concept_id'=>$paper_concept));
		if($q->result() > 0){
			var_dump($q);echo "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr";
			$parent_id = $q->row()->parent_id;
			if($parent_id != "NULL"){
				$q = $this->db->get_where('concept',array('concept_id'=>$parent_id));
				return $q->row();
			}
			else {
				return FALSE;
			}
		}
		else {
			return FALSE;
		}
	}
}