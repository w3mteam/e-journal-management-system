<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Inference_model extends CI_Model {
	/*
	public function get_all_publication($profile_id)
	{
		$this->db->where('profile_id',$profile_id);
		$q = $this->db->get('publication');
		if ($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_all_project($profile_id)
	{
		$this->db->where('profile_id',$profile_id);
		$q = $this->db->get('project');
		if ($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_all_course($profile_id)
	{
		$this->db->where('profile_id',$profile_id);
		$q = $this->db->get('course');
		if ($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}*/
	public function get_all_valid_concept()
	{
		$this->db->where('valid',1);
		$q = $this->db->get('concept');
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_all_profile()
	{
		$q = $this->db->get('profile');
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}
	public function get_all_concept_within_publications_foreach_profile($profile_id,$concept_id)
	{
		$q = $this->db->get_where('publication_concepts',array('profile_id'=>$profile_id,'concept_id'=>$concept_id));
		if($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_all_concept_within_projects_foreach_profile($profile_id,$concept_id)
	{
		$q = $this->db->get_where('project_concepts',array('profile_id'=>$profile_id,'concept_id'=>$concept_id));
		if($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_all_concept_within_courses_foreach_profile($profile_id,$concept_id)
	{
		$q = $this->db->get_where('course_concepts',array('profile_id'=>$profile_id,'concept_id'=>$concept_id));
		if($q->num_rows()>0){
			return $q->num_rows();
		}else {
			return FALSE;
		}
	}
	public function get_profile_num()
	{
		$q = $this->db->get('profile');
		if($q->num_rows() >0){
			return $q->num_rows();
		}else{
			return FALSE;
		}
	}
	public function get_concept_presence_within_pub($concept_id,$profile_id)
	{
		$q = $this->db->get_where('publication_concepts',array('concept_id'=>$concept_id,'profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function get_concept_presence_within_pro($concept_id,$profile_id)
	{
		$q = $this->db->get_where('project_concepts',array('concept_id'=>$concept_id,'profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function get_concept_presence_within_course($concept_id,$profile_id)
	{
		$q = $this->db->get_where('course_concepts',array('concept_id'=>$concept_id,'profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function profile_has_pub($profile_id)
	{
		$q = $this->db->get_where('publication_concepts',array('profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function profile_has_pro($profile_id)
	{
		$q = $this->db->get_where('project_concepts',array('profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function profile_has_course($profile_id)
	{
		$q = $this->db->get_where('course_concepts',array('profile_id'=>$profile_id));
		if($q->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	public function insert_profile_final_result($data)
	{
		$this->db->insert('profile_final_result',$data);
		if ($this->db->affected_rows() > 0){
			return TRUE;
		}else {
			return FALSE;
		}		
	}
	public function delete_profile_final_result($id)
	{
		$this->db->where('pfr_id', $id);
		$this->db->delete('profile_final_result');	
	}
	public function get_profile_final_result()
	{
		$q = $this->db->get('profile_final_result');
		if ($q->num_rows()>0){
			return $q->result();
		}else {
			return FALSE;
		}
	}

}