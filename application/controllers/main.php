<?php

/**
 * 
 */
class Main extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('support_lib');
		$this->load->library('profile_lib');
		$this->load->library('ion_auth');
		$this->load->model('search_engin_model');
		$this->load->model('main_model');
		$this->load->model('profile_model');
	}
	
	public function index()
	{
		$concept = $this->search_engin_model->get_concept();
		$data['concept'] = json_encode($concept);
		$data['valid_concept'] = $this->main_model->get_valid_concept();
		$data['paper'] = $this->main_model->get_publish_paper();
		$data['six_concepts'] = $this->main_model->get_paper_according_to_concept_id();
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		$data['page'] = 'template/home';
		$this->support_lib->view_template($data);

	}
	public function cpanel()
	{
		if($this->ion_auth->logged_in()){
			$data['message'] = '';
			$data['page'] = 'cpanel/home';
			$this->support_lib->view_cpanel($data);
		}
		else {
			redirect('auth/login');
		}
		
	}
	public function display_all_concept($value='')
	{
		$data['valid_concept'] = $this->main_model->get_valid_concept();
		$data['all_valid_concept'] 	= $this->main_model->get_all_valid_concept();
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		$data['page'] 				= 'template/other_page/display_category';
	    $this->load->view('view_controller',$data);
	}
	public function view_judge_profile($prof_id)
	{
		if($this->profile_lib->check_profile_id($prof_id)){
			$data['profile'] = $this->profile_lib->get_profile_info($prof_id);
			$data['page'] 				= 'template/other_page/view_profile_info';
			$data['valid_concept'] = $this->main_model->get_valid_concept();
			$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		    $this->support_lib->view_template($data);
		}
		else {
			$data['profile'] 			  = FALSE;
			$data['page'] 	 			  = 'template/other_page/view_profile_info';
			$data['message']			  = 'Unkown judge id';
			$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		    $this->support_lib->view_template($data);
		}
	}
	public function view_paper_accordance_concept ($concept_id,$prog_name)
	{
		$data['valid_concept'] 	= $this->main_model->get_valid_concept();
		$data['paper'] 			= $this->main_model->view_paper_accord_concept($concept_id);
		$data['prog_name'] 		= $prog_name;
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		$data['page'] 			= 'template/other_page/view_paper_accordance_concept';
	    $this->load->view('view_controller',$data);
	}
	public function paper_view_details($paper_id)
	{
		$data['valid_concept'] = $this->main_model->get_valid_concept();
		//$data['paper'] = $this->main_model->view_paper_accord_concept($concept_id);
		$data['paper_details'] = $this->main_model->get_paper_details($paper_id);
		$data['paper_concept'] = $this->main_model->get_paper_concept($paper_id);
 		$data['page'] 				= 'template/other_page/paper_view_details';
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
	    $this->load->view('view_controller',$data);
	}
	public function get_paper()
	{
		$data['valid_concept'] = $this->main_model->get_valid_concept();
		$data['paper'] = $this->main_model->get_paper();
 		$data['page'] 				= 'template/other_page/view_paper';
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
	    $this->load->view('view_controller',$data);	
	}
	public function get_journal()
	{
		$data['valid_concept'] = $this->main_model->get_valid_concept();
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		$data['paper'] = $this->main_model->get_journal();
 		$data['page'] 				= 'template/other_page/view_journal';
	    $this->load->view('view_controller',$data);	
	}
	public function put_concept_true($value='')
	{
		$this->main_model->put_concept_true();
	}
	public function put_concept_false($value='')
	{
		$this->main_model->put_concept_false();
	}
	
	public function get_profile($value='')
	{
		$data['profiles'] = $this->profile_model->get_profile();
		$data['paper_added_recently'] = $this->main_model->paper_added_recently();
		$prof_id =  $this->profile_model->get_first_profile_id();
		$data['profile'] = $this->profile_lib->get_profile_info($prof_id->profile_id);
		$data['page'] 	= 'template/other_page/get_first_profile';
	    $this->load->view('view_controller',$data);	
	}
}
