<?php 
/**
 * 
 */
class Paper extends CI_Controller {
	private $user_id;
	private $profile_id;
	function __construct() {
		parent::__construct();
		$this->load->library('paper_lib');
		$this->load->library('judgment_lib');
		$this->load->library('ion_auth');
		$this->lang->load('general', 'english');
		$this->load->model('profile_model');
		$this->load->model('main_model');
		$this->user_id = $this->ion_auth->get_user_id();
		$this->profile_id = $this->ion_auth_model->get_profile_id($this->user_id);
	}
	public function index()
	{
		$this->show();
	}
	public function view_paper_assigned_judges($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'view_paper_assigned_judges')){
				if($id == ''){
					redirect('paper/show');
				}
				if($this->paper_lib->check_paper_id($id)){
					$data['judges'] = $this->paper_lib->get_assigned_judges($id);
					$data['page'] = 'paper/show_assigned_judges';
					$this->support_lib->view_cpanel($data);
				}
				else {
					echo $this->lang->line('invalid_id');
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/view_paper_assigned_judges');
		}
	}
	public function paper_judgment_result($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'paper_judgment_result')){
				if($this->paper_lib->check_paper_id($id)){
					if($this->judgment_lib->judgement_process_finished($id)){
						$data['paper_result'] = $this->paper_lib->get_paper_judgment_result($id);
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$data['page'] = 'paper/show_paper_result';
						$this->support_lib->view_template($data);
					}
					else
						echo $this->lang->line('judgment_proccess_not_finished');
					
				}
				else {
					echo $this->lang->line('invalid_id');
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/paper_judgment_result');
		}
	}
	/* this function For Adding paper process */
	public function add($state="add")
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'add_paper')){
				// add information of paper phase
				if($state == "add"){
					if($this->paper_lib->add_paper()){
						// $concept = $this->profile_model->get_concept();
						// $data['concept'] = json_encode($concept);
						$message = '<p style="color: #5CB85C">'.$this->lang->line('success_add').'</p>';
						//$data['message'] = '';
						// $data['page'] = 'paper/upload';
						// $data['valid_concept'] = $this->main_model->get_valid_concept();
						// $data['paper_added_recently'] = $this->main_model->paper_added_recently();
						//$page_string = $this->load->view('paper/upload',$data,TRUE); 
						echo json_encode(array('message'=>$message)); 
					}
					else {
						$concept = $this->profile_model->get_concept();
						$data['concept'] = json_encode($concept);
						$data['message'] = "";//$this->lang->line('fail_add');
						$data['page'] = 'paper/add';
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$this->load->view('view_controller',$data);
					}
				}
				// add paper file phase
				else {
					$id = $this->session->userdata('paper_id');
					$not_uploaded = $this->paper_lib->add_paper_file($id);
					if(!$not_uploaded){
						$concept = $this->profile_model->get_concept();
					 	$data['concept'] = json_encode($concept);
						$data['message'] =  '<p style="color: #5CB85C">'.$this->lang->line('success_add').'</p>';
						$data['page'] = 'paper/add';
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$this->load->view('view_controller',$data);
					}
					else {
						$concept = $this->profile_model->get_concept();
						$data['concept'] = json_encode($concept);
						$data['message'] ='<p style="color: #D9534F">'.$not_uploaded.'</p>' ;//$this->lang->line('fail_add');
						$data['page'] = 'paper/upload';
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$this->load->view('view_controller',$data);
					}
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/add');
		}
		
	}
	/* this function For update paper file  */
	public function upload_new_file($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'upload_paper_file')){
				$not_uploaded = $this->paper_lib->add_paper_file($id);
				if(!$not_uploaded){
					$data['id'] = $id;
					$data['message'] = $this->lang->line('success_add');
					$data['page'] = 'paper/edit_uploaded_file';
					$data['valid_concept'] = $this->main_model->get_valid_concept();
					$data['paper_added_recently'] = $this->main_model->paper_added_recently();
					$this->load->view('view_controller',$data);
				}
				else {
					$data['id'] = $id;
					$data['valid_concept'] = $this->main_model->get_valid_concept();
					$data['paper_added_recently'] = $this->main_model->paper_added_recently();
					$data['message'] = $not_uploaded;//$this->lang->line('fail_add');
					$data['page'] = 'paper/edit_uploaded_file';
					$this->load->view('view_controller',$data);
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/upload_new_file');
		}
	}
	/* this function For showing paper */
	public function show()
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'show_paper')){
				$this->load->model('paper_model');
				
				if($this->ion_auth->is_admin()){
					$data['page'] = 'paper/cpanel_papers_list';
					$papers = $this->paper_model->get_all_papers();
					$data['operations'] = array('delete','view_paper_assigned_judges');
					$data['papers'] = $papers;
					$this->load->view('cpanel_controller',$data);
				}
				else if($this->ion_auth->is_judge()){
					$this->assigned_papers();
				}
				else {
					$data['page'] = 'paper/user_papers_list';
					$papers = $this->paper_model->get_my_papers($this->user_id);
					$data['valid_concept'] = $this->main_model->get_valid_concept();
					$data['paper_added_recently'] = $this->main_model->paper_added_recently();
					$data['operations'] = array('edit');
					$data['papers'] = $papers;
					$this->load->view('view_controller',$data);
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/show');
		}
	}
	/* this function For Updateing paper process */
	public function edit($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'edit_paper')){
				if($this->paper_lib->check_paper_id($id)){
					if($this->paper_lib->edit_paper($id)){
						$data['message'] = $this->lang->line('success_edit');
						$data['page'] = 'paper/edit';
						$data['paper'] = $this->paper_model->get_paper($id);
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$this->load->view('view_controller',$data);
					}
					else {
						$data['paper'] = $this->paper_model->get_paper($id);
						//$data['current_concepts'] = json_encode($this->paper_model->get_current_paper_concepts($id));
						$data['message'] = validation_errors();//$this->lang->line('fail_add');
						$data['page'] = 'paper/edit';
						//$concept = $this->paper_model->get_concept();
						//$data['concept'] = json_encode($concept);
						$data['valid_concept'] = $this->main_model->get_valid_concept();
						$data['paper_added_recently'] = $this->main_model->paper_added_recently();
						$this->load->view('view_controller',$data);
					}
				}
				else {
					echo $this->lang->line('invalid_id');
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/edit');
		}
	}
	
	public function delete($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'delete_paper')){
				if($this->paper_lib->remove_paper($id)){
					redirect('paper/show');
				}
				else {
					$data['papers']  = $this->paper_model->get_all_papers();
					$data['can_edit'] = false;
					$data['can_delete'] = TRUE;
					$data['message'] = $this->lang->line('faild_delete');
					$data['page'] = 'paper/cpanel_papers_list';
					$this->support_lib->view_cpanel($data);
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/delete');
		}
	}
	
	
	public function assigned_papers()
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'assign_papers')){
				$data['pending_edit_papers'] = $this->paper_lib->get_pending_edit_papers();
				$data['papers'] = $this->paper_lib->get_assigned_papers();
				$data['page'] = 'paper/assinged_papers';
				$this->load->view('cpanel_controller',$data);
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/assigned_papers');
		}
	}
	
	public function judge($id='')
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'judge_paper')){
				$this->load->library('judgment_lib');
				if($id == ''){
					redirect('paper/assigned_papers');
				}
				if($this->judgment_lib->check_paper_id($id)){
					if($this->judgment_lib->can_judge($this->profile_id,$id)){
						if($this->judgment_lib->judge_paper($id)){
							$data['papers'] = $this->paper_lib->get_assigned_papers();
							$data['page'] = 'paper/assinged_papers';
							//$this->load->view('view_controller',$data);
							$this->support_lib->view_cpanel($data);
						}
						else {
							$res = $this->judgment_lib->get_measures_and_levels();
							$data['measures'] = $res['measures'];
							//$data['levels'] = $res['levels'];
							$data['final_res'] = $res['final_res'];
							$data['paper'] = $this->paper_model->get_paper($id);
							$data['page'] = 'paper/judgment';
							//$this->load->view('view_controller',$data);
							$this->support_lib->view_cpanel($data);
						}
					}
					else {
						$data['message'] = $this->lang->line('cant_judge');
						$data['page'] = 'paper/judgment';
						$this->load->view('view_controller',$data);
					}
				}
				else {
					$data['message'] = $this->lang->line('no_privilages');
					$data['page'] = 'paper/judgment';
					$this->load->view('view_controller',$data);
				}
				
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/paper/assigned_papers');
		}
	}
}
