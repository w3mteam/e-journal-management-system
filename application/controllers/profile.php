<?php 
/**
 * 
 */
class Profile extends CI_Controller {
	public $user_id;
	public $profile_id;
	function __construct() {
		parent::__construct();
		$this->load->library('profile_lib');
		$this->load->library('support_lib');
		$this->load->model('profile_model');
		$this->lang->load('general', 'english');
		$this->load->library('ion_auth');
		$this->user_id = $this->ion_auth->get_user_id();
		$this->load->model('ion_auth_model');
		$this->profile_id =$this->ion_auth_model->get_profile_id($this->user_id);
	}
	public function index()
	{
		$this->edit_profile();
	}
	public function edit_profile()
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'edit_profile')){
				
				$profile_id = $this->profile_lib->has_profile();
				
				if(!$profile_id){
					
					$this->add_info();
					
				}
				else {
					// edit profile ..
					$data['profile_info'] = $this->profile_model->get_profile_info($this->user_id);
					$data['specialization']=$this->profile_model->get_profile_spec($this->profile_id);
					//concept
					$concept = $this->profile_model->get_concept();
					$data['concept'] = json_encode($concept);
					
					$data['page']='profile/edit_info';
					$this->support_lib->view_cpanel($data);
				}
			}
			else {
				
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			
			redirect('account/login/profile/edit_profile');
		}
	}
	private function add_info()
	{
		$concept = $this->profile_model->get_concept();
		$data['concept'] = json_encode($concept);
		$data['page'] = 'profile/add_info';
		$this->support_lib->view_cpanel($data);
		//$this->load->view('profile/add_info',$data);
	}
	public function add_information()
	{
		if($this->profile_lib->add_information()){
			$concept 			= $this->profile_model->get_concept();
			$data['id'] 		=  $this->session->userdata('profile_id');
			$data['concept'] 	= json_encode($concept);
			$data['page'] = 'profile/add_jobs';
			//$this->load->view('profile/add_jobs',$data);
			$this->support_lib->view_cpanel($data);
		}else{
			$this->add_info();
		}
	}
    public function add_jobs()
	{
		if($this->ion_auth->logged_in()){
			if($this->ion_auth->has_privilage($this->user_id,'add_jobs')){
				$pub_resutl_obj = json_decode($_POST['pub_json']);
				foreach ($pub_resutl_obj as $key => $item) {
					$pub_dmains_obj = json_decode($item->pub_domain);
					$data = array('profile_id'=>$this->session->userdata('profile_id'),'name'=>$item->pub_name,'details'=>$item->pub_details,'date_time'=>$item->pub_date);
					$publication_id = $this->profile_model->insert_publication($data);
					foreach ($pub_dmains_obj as $key => $domain) {
						echo $key." : ".$domain."<br>";
						$info = array('profile_id'=>$this->session->userdata('profile_id'),'publication_id'=>$publication_id,'concept_id'=>$key);
						$this->profile_model->insert_publication_concepts($info);
						$this->profile_model->update_concept_valid($key);
					}
				}
				$pro_resutl_obj = json_decode($_POST['pro_json']);
				foreach ($pro_resutl_obj as $key => $item) {
					$pro_dmains_obj = json_decode($item->pro_domain);
					$data = array('profile_id'=>$this->session->userdata('profile_id'),'name'=>$item->pro_name,'details'=>$item->pro_details,'date_time'=>$item->pro_date);
					$project_id = $this->profile_model->insert_project($data);
					foreach ($pro_dmains_obj as $key => $domain) {
						echo $key." : ".$domain."<br>";
						$info = array('profile_id'=>$this->session->userdata('profile_id'),'project_id'=>$project_id,'concept_id'=>$key);
						$this->profile_model->insert_project_concepts($info);
						$this->profile_model->update_concept_valid($key);
					}
				}
				$course_resutl_obj = json_decode($_POST['course_json']);
				foreach ($course_resutl_obj as $key => $item) {
					$course_dmains_obj = json_decode($item->course_domain);
					$data = array('profile_id'=>$this->session->userdata('profile_id'),'name'=>$item->course_name,'details'=>$item->course_details,'date_time'=>$item->course_date);
					$course_id = $this->profile_model->insert_course($data);
					foreach ($course_dmains_obj as $key => $domain) {
						echo $key." : ".$domain."<br>";
						$info = array('profile_id'=>$this->session->userdata('profile_id'),'course_id'=>$course_id,'concept_id'=>$key);
						$this->profile_model->insert_course_concepts($info);
						$this->profile_model->update_concept_valid($key);
					}
				}
			}
			else {
				echo $this->lang->line('no_privilages');
			}
		}
		else {
			redirect('account/login/profile/add_jobs');
		}
	}
	
}