<?php
/**
 * 
 */
class Notification extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('support_lib');
		$this->load->model('notification_model');
		$this->load->library('session');
	}
	
	public function index()
	{		
		redirect('account');
	}
	
	public function see_all()
	{
		$user_id=$this->session->userdata('user_id');
		$notifications=$this->notification_model->get_notifications($user_id);
		$data['page']='see_all_notifications';
		$data['notifications']=$notifications;
		$this->support_lib->view_cpanel($data);;
		//$this->load->view('view_controller',$data);
	}
	public function seen()
	{
		$user_id=$this->session->userdata('user_id');
		if ($this->notification_model->seen($user_id))
			echo  "true";
		else
			echo  "false";
	}

	
	// public function get_unseen_notifications()
	// {
		// $user_id=$this->session->userdata('user_id');
		// $data=$this->notification_model->get_unseen_notifications($user_id);
		// var_dump($data);
	// }
	
	// public function add($value='')
	// {
		// $user_id=$this->session->userdata('user_id');
		// $data=array('not_user_id' => $user_id,
					// 'not_paper_id' => 8,
					// 'not_text' => 'test 2 ',
					// 'not_is_read' => 0,
					// 'not_pushing_date' =>  $this->support_lib->get_current_date()
				// );
		// $this->notification_model->add($data);		
	// }
}
