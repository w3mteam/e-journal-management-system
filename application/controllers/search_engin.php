<?php 
/**
 * 
 */
class Search_engin extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('search_engin_lib');
		$this->load->library('support_lib');
		$this->load->model('search_engin_model');
		$this->lang->load('general', 'english');
		
	}
	public function index($value='')
	{
		$this->search_engin_lib->link_matrix();
	}
	public function manipulate_search_request()
	{
		$keyword = array();
		$data['keywords'] = $this->input->post('keywords');
		foreach (json_decode($data['keywords']) as $key => $value) {
			$keyword[] = $value;
		}
		
		$result = $this->search_engin_lib->get_search_result($keyword);
		echo json_encode($result);
		
	}
}