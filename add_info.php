<html>
	<head>
		<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/bootstrap.css">
		<script src="<?php echo base_url();?>files/public/js/jquery-1.8.3.min.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap.js"></script>
		<script src="<?php echo base_url();?>files/public/js/bootstrap-typeahead.js"></script>
		
		
		<script type="text/javascript">
			function displayRes(item, val, text) {
			    $('#selecteditems').append('<tr><td><a style="color:red;" onclick="removeparent(this.parentNode);" href="javascript:void(0)">X</a></td><td><p>' + text + '</p><input name="'+val+'" value="'+text+'" type="hidden" /></td></tr>');
			    $('#demo1').val('');
			}
			function removeparent(e){
				e.parentNode.remove();
				var p = parseInt(e.parentNode.lastChild.textContent);
			}
			$(document).ready(function () {
				var jsonData = <?php echo $concept;?>;
				src = []; 
				for (var i = 0; i < jsonData.length; i++) {
					src.push({id: jsonData[i].concept_id , name: jsonData[i].name});
				}
			    $('#demo1').typeahead({
			        source:src,
			        itemSelected: displayRes
			    });
				
			});
		</script>
	</head>
	<body>
		<?php echo form_open('profile/add_information'); ?>
		<label>First Name:</label>
		<?php echo form_input('first_name',set_value('first_name'),''); ?><br />
		<label>Last Name:</label>
		<?php echo form_input('last_name',set_value('last_name'),''); ?><br />
		<label>Specialization In</label>
		<div id="scrollable-dropdown-menu">
			<?php echo form_input('specialization',set_value('specialization'),'id="demo1"'); ?><br />
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th><center><span class="span1">Remove Item</span></center></th>
							<th><center><span class="span1">Speciality name</span></center></th>
						</tr>
					</thead>
					<tbody id="selecteditems">
					
					</tbody>
				</table>
			</div>
		</div>
		<?php echo form_submit('add','Add'); ?>
		<?php echo form_close(); ?>
		<?php echo validation_errors(); ?>
	</body> 
</html>
